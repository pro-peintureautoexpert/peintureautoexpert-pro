<?php

namespace App\Form;

use App\Entity\Carrier;
use App\Entity\TemporaryOrder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TemporaryOrderFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('brands', BrandAutocompleteField::class)
            ->add('name', TextType::class, [
                'label' => 'Nom du produit',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Mipa P 99 Multi Star',
                ],
            ])

            ->add('codeArticle', TextType::class, [
                'label' => 'Référénce produit',
                'required' => true,
                'attr' => [
                    'placeholder' => '28825 0000',
                ],
            ])
            ->add('qte', NumberType::class, [
                'label' => 'Quantité',
                'required' => true,
                'attr' => [
                    'placeholder' => '10',
                ],
            ])

            ->add('carrierName', EntityType::class, [
                'label' => 'Choisissez votre transporteur',
                'required' => true,
                'class' => Carrier::class,
                'expanded' => true,
                'label_html' => true,
            ])

        ->add('submit', SubmitType::class, [
            'label' => 'Valider',
            'attr' => [
                'class' => 'w-100 btn btn-success',
            ],
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TemporaryOrder::class,
            'brands' => null,
            'carriers' => null,
        ]);
    }
}
