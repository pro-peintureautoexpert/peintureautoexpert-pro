<?php

namespace App\Form;

use App\Entity\Annexe;
use App\Entity\Bd;
use App\Entity\Sol;
use App\Entity\Water;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfonycasts\DynamicForms\DependentField;
use Symfonycasts\DynamicForms\DynamicFormBuilder;

class PaintType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder = new DynamicFormBuilder($builder);
        $builder
        ->add('category', ChoiceType::class, [
            'choices' => [
                'Solventé' => 'solvente',
                'A l\'eau' => 'eau',
                'Brillant direct' => 'brillant direct',
                'Annexe' => 'annexe',
            ],
            'label' => 'Type de peinture',
            'expanded' => false,
            'multiple' => false,
            'autocomplete' => true,
            'mapped' => false,
            'required' => false,  // Allows the field to be empty
            'placeholder' => 'Sélectionnez une option',  // Displays as the default "null" value
        ])

            ->addDependent('name', 'category', function (DependentField $field, ?string $category) {
                if ('solvente' == $category) {
                    $field
                    ->add(EntityType::class, [
                        'label' => 'Base peinture Sol (ex: T014)',
                        'class' => Sol::class,
                        'required' => true,
                        'expanded' => false,
                        'multiple' => true,
                        'placeholder' => 'Choisir une base de peinture',
                        'autocomplete' => true,
                    ]);
                } elseif ('eau' == $category) {
                    $field
                    ->add(EntityType::class, [
                        'label' => 'Base peinture à l\'eau (ex: T500)',
                        'class' => Water::class,
                        'required' => true,
                        'expanded' => false,
                        'multiple' => true,
                        'placeholder' => 'Choisir une base de peinture à l\'eau',
                        'autocomplete' => true,
                    ]);
                } elseif ('brillant direct' == $category) {
                    $field
                    ->add(EntityType::class, [
                        'label' => 'Base peinture BD (ex: T98)',
                        'class' => Bd::class,
                        'required' => true,
                        'expanded' => false,
                        'multiple' => true,
                        'placeholder' => 'Choisir une base de peinture BD',
                        'autocomplete' => true,
                    ]);
                } elseif ('annexe' == $category) {
                    $field
                    ->add(EntityType::class, [
                        'label' => 'Base peinture Annexe (ex: X)',
                        'class' => Annexe::class,
                        'required' => true,
                        'expanded' => false,
                        'multiple' => true,
                        'placeholder' => 'Choisir une base de peinture Annexe',
                        'autocomplete' => true,
                    ]);
                }
            })

            ->add('submit', SubmitType::class, [
                'label' => 'Commander',
                // 'attr' => [
                //     'class' => 'btn btn-success',
                // ],
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
        ]);
    }
}
