<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class RegisterUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('password', TextType::class, [
                'label' => 'Mot de passe',
                'attr' => [
                    'placeholder' => 'Indiquez le mot de passe',
                ],
            ])
            ->add('Uuid', TextType::class, [
                'label' => 'Identifiant Unique',
                'attr' => [
                    'placeholder' => 'Indiquez l\'identifiant du magasin',
                ],
            ])
            ->add('email', EmailType::class, [
                'label' => 'Adresse email',
                'attr' => [
                    'placeholder' => 'Indiquez l\'email du magasin',
                ],
            ])
            ->add('firstName', TextType::class, [
                'label' => 'Prénom',
                'attr' => [
                    'placeholder' => 'Indiquez le prénom du responsable du magasin',
                ],
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Nom',
                'attr' => [
                    'placeholder' => 'Indiquez le nom du responsable du magasin',
                ],
            ])
            ->add('magasinName', TextType::class, [
                'label' => 'Nom du magasin',
                'attr' => [
                    'placeholder' => 'Indiquez le nom du magasin',
                    'webkitdirectory' => true, // Autorise la sélection de dossiers
                ],
            ])
            ->add('identite', FileType::class, [
                'label' => 'Piéce d\'identité',
                'attr' => [
                    'placeholder' => 'Glissez-déposez un fichier ou cliquez pour parcourir',
                ],
                'constraints' => [
                    new File(),
                ],
            ])

            ->add('kbis', FileType::class, [
                'label' => 'Kbis',
                'attr' => [
                    'placeholder' => 'Glissez-déposez un fichier ou cliquez pour parcourir',
                ],
                'constraints' => [
                    new File(),
                ],
            ])
            
            ->add('plafond', TextType::class, [
                'label' => 'Plafond du magasin',
                'attr' => [
                    'placeholder' => 'Indiquez le plafond du magasin (Ex: 15000)',
                ],
            ])

            ->add('submite', SubmitType::class, [
                'label' => 'Crée le compte',
                'attr' => [
                    'class' => 'btn btn-primary',
                ],
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
