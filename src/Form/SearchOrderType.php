<?php

namespace App\Form;

use App\Entity\Order;
use App\Entity\OrderDetail;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchOrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder

        ->add('order', EntityType::class, [
            'class' => Order::class, // The entity to use
            'label' => 'Recherche par commande',
            'required' => true,
            'expanded' => false, // Use a dropdown list
            'autocomplete' => true, // Enable autocomplete (requires JavaScript support)
            'placeholder' => 'Sélectionnez une commande',
            'mapped' => false, // This field is not mapped to an entity property
        ])
        ->add('submit', SubmitType::class, [
            'label' => 'Validation',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
        ]);
    }
}
