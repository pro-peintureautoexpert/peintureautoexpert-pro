<?php

namespace App\Form;

use App\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddproductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('product', EntityType::class,[
                'class' => Product::class,
                'choice_label' => 'articleCode',
                'multiple' => false,
                'expanded' => false,
                'placeholder' => 'Sélectionnez un produit',
                'autocomplete'=> true,
                'mapped' => false,
                'label'=> 'Produit',
            ])

            ->add('qte', IntegerType::class, [
                'label'    => 'Quantité',
                'attr'     => [
                    'min'        => 1,
                    'max'        => 1000,
                    'step'       => 1,
                    'placeholder' => 'Saisissez une quantité', 
                ],
                'required' => true,
            ])
            
            ->add('submit', SubmitType::class, [
                'label' => 'Validation',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
        ]);
    }
}
