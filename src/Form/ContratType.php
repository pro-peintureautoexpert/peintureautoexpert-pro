<?php

namespace App\Form;

use App\Entity\Contrat;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ContratType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('user', EntityType::class, [
            'class' => User::class,
            'choice_label' => 'magasinName',
        ])
    
        ->add('name', FileType::class, [
            'label' => 'Contrat',
            'attr' => [
                'placeholder' => 'Glissez-déposez un fichier ou cliquez pour parcourir',
            ],
            'constraints' => [
                new File(),
            ],
        ])

        ->add('submit', SubmitType::class, [
            'label' => 'Ajouter',
            'attr' => [
                'class' => 'w-100 btn btn-success',
            ],
        ]);
            
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contrat::class,
        ]);
    }
}
