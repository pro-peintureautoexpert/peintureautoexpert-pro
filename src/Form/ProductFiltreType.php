<?php

namespace App\Form;

use App\Entity\Accessoire;
use App\Entity\AccessoireAspirateur;
use App\Entity\AcierAlu;
use App\Entity\Adhesif;
use App\Entity\Aspirateur;
use App\Entity\BarreDebosslage;
use App\Entity\Buse;
use App\Entity\CapaciteAspirateur;
use App\Entity\ChaineRaccord;
use App\Entity\ColleJoint;
use App\Entity\ConAntigravillon;
use App\Entity\ConAppret;
use App\Entity\ConBidonPlastique;
use App\Entity\ConDegraissant;
use App\Entity\ConDiluant;
use App\Entity\ConDurcisseur;
use App\Entity\ConGobelet;
use App\Entity\ConMastic;
use App\Entity\ConPolish;
use App\Entity\Brand;
use App\Entity\ConSavon;
use App\Entity\ConVernis;
use App\Entity\CouleurAll;
use App\Entity\DebosseleurMarteau;
use App\Entity\DiametreAgrafes;
use App\Entity\DiametreZikzak;
use App\Entity\DimansionFiltreCabine;
use App\Entity\DimensionAll;
use App\Entity\DimensionChariotKraft;
use App\Entity\DimensionDoubleFace;
use App\Entity\DimensionKraft;
use App\Entity\DimensionMicroBruch;
use App\Entity\DimensionPapierCale;
use App\Entity\DimensionTireau;
use App\Entity\Durcisseur;
use App\Entity\Effet;
use App\Entity\EquerreTirageGriffe;
use App\Entity\FilterDimensionCale;
use App\Entity\FiltreCone;
use App\Entity\Grain;
use App\Entity\Malette;
use App\Entity\Masquage;
use App\Entity\MasseMagnetiqueChario;
use App\Entity\Materiel;
use App\Entity\Matiere;
use App\Entity\Nettoyage;
use App\Entity\PinceauDimension;
use App\Entity\Ponceuse;
use App\Entity\Rapidite;
use App\Entity\RegleDosage;
use App\Entity\SolPlafond;
use App\Entity\Taille;
use App\Entity\TailleFineTape;
use App\Entity\TypeDiluant;
use App\Entity\TypeMastic;
use App\Entity\TypeMateriel;
use App\Entity\TypePistole;
use App\Entity\TypePonceuse;
use App\Entity\TypeRaccord;
use App\Entity\Vitrage;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;


class ProductFiltreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $category = $options['current_category'];

        $builder
        ->add('category', HiddenType::class, [
            'data' => $options['category'],
        ]);

        foreach ($options['filters'] as $filter) {
              if ('brand' === $filter->getName()) {
                  $builder
                      ->add('brand', EntityType::class, [
                          'label' => 'Marque',
                          'required' => false,
                          'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                          'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                          'class' => Brand::class,
                          'expanded' => true,
                          'label_html' => true,
                          'query_builder' => function (EntityRepository $er) use ($category) {
                              return $er->createQueryBuilder('b')
                                  ->join('b.productParents', 'p') // Relier Brand à ProductParent
                                  ->join('p.category', 'c')      // Relier ProductParent à Category (corrigé)
                                  ->where('c.slug = :category')  // Filtrer par slug de la catégorie
                                  ->setParameter('category', $category->getSlug())
                                  ->groupBy('b.id');             // Éviter les doublons
                          },
                      ]);
              }


            if ('grain' === $filter->getName()) {
                $builder
                ->add('grain', EntityType::class, [
                    'label' => 'GRAIN',
                    'required' => false,
                    'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                    'class' => Grain::class,
                    'expanded' => true,
                    'label_html' => true,
               		'query_builder' => function (EntityRepository $er) use ($category) {
                    return $er->createQueryBuilder('g')
                        ->join('g.products', 'p')  // Joint Grain et Product
                        ->join('p.Category', 'c') // Joint Product et Category
                        ->where('c.slug = :category') // Filtrer par catégorie courante
                        ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                        ->setParameter('category', $category->getSlug())
                        ->groupBy('g.id');       // Éviter les doublons
                },
                ]);
            }
          
          if ('conMastic' === $filter->getName()) {
            $builder
                ->add('conMastic', EntityType::class, [
                    'label' => 'CONDITIONNEMENT',
                    'required' => false,
                    'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                    'class' => ConMastic::class,
                    'expanded' => true,
                    'label_html' => true,
                    'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('cm')
                            ->join('cm.products', 'p')  // Joint ConMastic et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('cm.id');         // Éviter les doublons
                    },
                ]);
        }

        if ('conAppret' === $filter->getName()) {
            $builder
                ->add('conAppret', EntityType::class, [
                    'label' => 'CONDITIONNEMENT',
                    'required' => false,
                    'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                    'class' => ConAppret::class,
                    'expanded' => true,
                    'label_html' => true,
                    'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('ca')
                            ->join('ca.product', 'p')  // Joint ConAppret et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('ca.id');         // Éviter les doublons
                    },
                ]);
        }

        if ('conVernis' === $filter->getName()) {
            $builder
                ->add('conVernis', EntityType::class, [
                    'label' => 'CONDITIONNEMENT',
                    'required' => false,
                    'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                    'class' => ConVernis::class,
                    'expanded' => true,
                    'label_html' => true,
                    'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('cv')
                            ->join('cv.product', 'p')  // Joint ConVernis et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('cv.id');         // Éviter les doublons
                    },
                ]);
        }

        if ('rapidite' === $filter->getName()) {
            $builder
                ->add('rapidite', EntityType::class, [
                    'label' => 'Type de durcisseur',
                    'required' => false,
                    'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                    'class' => Rapidite::class,
                    'expanded' => true,
                    'label_html' => true,
                    'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('r')
                            ->join('r.product', 'p')  // Joint Rapidite et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('r.id');         // Éviter les doublons
                    },
                ]);
        }

        if ('malette' === $filter->getName()) {
            $builder
                ->add('malette', EntityType::class, [
                    'label' => 'MALETTE',
                    'required' => false,
                    'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                    'class' => Malette::class,
                    'expanded' => true,
                    'label_html' => true,
                    'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('m')
                            ->join('m.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('m.id');         // Éviter les doublons
                    },
                ]);
        }

            if ('couleurAll' === $filter->getName()) {
                $builder
                ->add('couleurAll', EntityType::class, [
                    'label' => 'Couleur',
                    'required' => false,
                    'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                    'class' => CouleurAll::class,
                    'expanded' => true,
                    'label_html' => true,
                   'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('cou')
                            ->join('cou.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('cou.id');         // Éviter les doublons
                    },
                ]);
            }

            if ('dimensionCale' === $filter->getName()) {
                $builder
                ->add('dimensionCale', EntityType::class, [
                    'label' => 'Dimension Cale',
                    'required' => false,
                    'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                    'class' => FilterDimensionCale::class,
                    'expanded' => true,
                    'label_html' => true,
                  'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('cal')
                            ->join('cal.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('cal.id');         // Éviter les doublons
                    },
                ]);
            }
            if ('dimensionDoubleFace' === $filter->getName()) {
                $builder
                ->add('dimensionDoubleFace', EntityType::class, [
                    'label' => 'Dimension Double Face',
                    'required' => false,
                    'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                    'class' => DimensionDoubleFace::class,
                    'expanded' => true,
                    'label_html' => true,
                  'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('face')
                            ->join('face.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('face.id');         // Éviter les doublons
                    },
                ]);
            }

            if ('buse' === $filter->getName()) {
                $builder
                ->add('buse', EntityType::class, [
                    'label' => 'buse',
                    'required' => false,
                    'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                    'class' => Buse::class,
                    'expanded' => true,
                    'label_html' => true,
                  'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('bu')
                            ->join('bu.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('bu.id');         // Éviter les doublons
                    },
                ]);
            }
            if ('conAntigravillon' === $filter->getName()) {
                $builder
                    ->add('conAntigravillon', EntityType::class, [
                        'label' => 'Conditionnement Antigravillon', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => ConAntigravillon::class,
                        'expanded' => true,
                        'label_html' => true,
                      'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('anti')
                            ->join('anti.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('anti.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('conDegraissant' === $filter->getName()) {
                $builder
                    ->add('conDegraissant', EntityType::class, [
                        'label' => 'Conditionnement Dégraissant', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => ConDegraissant::class,
                        'expanded' => true,
                        'label_html' => true,
                      	'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('deg')
                            ->join('deg.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('deg.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('capaciteAspirateur' === $filter->getName()) {
                $builder
                    ->add('capaciteAspirateur', EntityType::class, [
                        'label' => 'Capacité de l\'Aspirateur', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => CapaciteAspirateur::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('capa')
                            ->join('capa.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('capa.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('conPolish' === $filter->getName()) {
                $builder
                    ->add('conPolish', EntityType::class, [
                        'label' => 'Conditionnement Polish', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => ConPolish::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('poli')
                            ->join('poli.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('poli.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('conSavon' === $filter->getName()) {
                $builder
                    ->add('conSavon', EntityType::class, [
                        'label' => 'Conditionnement Savon', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => ConSavon::class,
                        'expanded' => true,
                        'label_html' => true,
                      'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('savon')
                            ->join('savon.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('savon.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('conDiluant' === $filter->getName()) {
                $builder
                    ->add('conDiluant', EntityType::class, [
                        'label' => 'Conditionnement Diluant', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => ConDiluant::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('dilu')
                            ->join('dilu.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('dilu.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('conGobelet' === $filter->getName()) {
                $builder
                    ->add('conGobelet', EntityType::class, [
                        'label' => 'Conditionnement Gobelet', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => ConGobelet::class,
                        'expanded' => true,
                        'label_html' => true,
                      	'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('conBidonPlastique' === $filter->getName()) {
                $builder
                    ->add('conBidonPlastique', EntityType::class, [
                        'label' => 'Conditionnement Bidon Plastique', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => ConBidonPlastique::class,
                        'expanded' => true,
                        'label_html' => true,
                      	'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('conDurcisseur' === $filter->getName()) {
                $builder
                    ->add('conDurcisseur', EntityType::class, [
                        'label' => 'Conditionnement Durcisseur', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => ConDurcisseur::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('dimansionFiltreCabine' === $filter->getName()) {
                $builder
                    ->add('dimansionFiltreCabine', EntityType::class, [
                        'label' => 'Dimension du Filtre de Cabine', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => DimansionFiltreCabine::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('diametreZikzak' === $filter->getName()) {
                $builder
                    ->add('diametreZikzak', EntityType::class, [
                        'label' => 'Diamètre Zikzak', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => DiametreZikzak::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('diametreAgrafes' === $filter->getName()) {
                $builder
                    ->add('diametreAgrafes', EntityType::class, [
                        'label' => 'Diamètre des Agrafes', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => DiametreAgrafes::class,
                        'expanded' => true,
                        'label_html' => true,
                         'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('dimensionChariotKraft' === $filter->getName()) {
                $builder
                    ->add('dimensionChariotKraft', EntityType::class, [
                        'label' => 'Dimension du Chariot Kraft', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => DimensionChariotKraft::class,
                        'expanded' => true,
                        'label_html' => true,
                          'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('dimensionPapierCale' === $filter->getName()) {
                $builder
                    ->add('dimensionPapierCale', EntityType::class, [
                        'label' => 'Dimension du Papier Cale', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => DimensionPapierCale::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('filtreCone' === $filter->getName()) {
                $builder
                    ->add('filtreCone', EntityType::class, [
                        'label' => 'Filtre Cone', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => FiltreCone::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('dimensionMicroBruch' === $filter->getName()) {
                $builder
                    ->add('dimensionMicroBruch', EntityType::class, [
                        'label' => 'Dimension Micro Bruch', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => DimensionMicroBruch::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('pinceauDimension' === $filter->getName()) {
                $builder
                    ->add('pinceauDimension', EntityType::class, [
                        'label' => 'Dimension du Pinceau', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => PinceauDimension::class,
                        'expanded' => true,
                        'label_html' => true,
                         'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('regleDosage' === $filter->getName()) {
                $builder
                    ->add('regleDosage', EntityType::class, [
                        'label' => 'Règle de Dosage', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => RegleDosage::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('dimensionKraft' === $filter->getName()) {
                $builder
                    ->add('dimensionKraft', EntityType::class, [
                        'label' => 'Dimension Kraft', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => DimensionKraft::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('dimensionTireau' === $filter->getName()) {
                $builder
                    ->add('dimensionTireau', EntityType::class, [
                        'label' => 'Dimension Tireau', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => DimensionTireau::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('tailleFineTape' === $filter->getName()) {
                $builder
                    ->add('tailleFineTape', EntityType::class, [
                        'label' => 'Taille Fine du Tape', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => TailleFineTape::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.product', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('effet' === $filter->getName()) {
                $builder
                    ->add('effet', EntityType::class, [
                        'label' => 'Effet', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => Effet::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('aspirateur' === $filter->getName()) {
                $builder
                    ->add('aspirateur', EntityType::class, [
                        'label' => 'Aspirateur', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => Aspirateur::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('ponceuse' === $filter->getName()) {
                $builder
                    ->add('ponceuse', EntityType::class, [
                        'label' => 'Ponceuse', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => Ponceuse::class,
                        'expanded' => true,
                        'label_html' => true,
                         'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('typePistole' === $filter->getName()) {
                $builder
                    ->add('typePistole', EntityType::class, [
                        'label' => 'Type de Pistolet', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => TypePistole::class,
                        'expanded' => true,
                        'label_html' => true,
                         'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('solPlafond' === $filter->getName()) {
                $builder
                    ->add('solPlafond', EntityType::class, [
                        'label' => 'Sol et Plafond', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => SolPlafond::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('acierAlu' === $filter->getName()) {
                $builder
                    ->add('acierAlu', EntityType::class, [
                        'label' => 'Acier et Alu', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => AcierAlu::class,
                        'expanded' => true,
                        'label_html' => true,
                         'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('dimensionAll' === $filter->getName()) {
                $builder
                    ->add('dimensionAll', EntityType::class, [
                        'label' => 'Dimensions All', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => DimensionAll::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('taille' === $filter->getName()) {
                $builder
                    ->add('taille', EntityType::class, [
                        'label' => 'Taille', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => Taille::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('matiere' === $filter->getName()) {
                $builder
                    ->add('matiere', EntityType::class, [
                        'label' => 'Matière', // Label clarifié
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => Matiere::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }
            if ('masquage' === $filter->getName()) {
                $builder
                    ->add('masquage', EntityType::class, [
                        'label' => 'Masquage',
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => Masquage::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('nettoyage' === $filter->getName()) {
                $builder
                    ->add('nettoyage', EntityType::class, [
                        'label' => 'Nettoyage',
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => Nettoyage::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('durcisseur' === $filter->getName()) {
                $builder
                    ->add('durcisseur', EntityType::class, [
                        'label' => 'Durcisseur',
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => Durcisseur::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('accessoireAspirateur' === $filter->getName()) {
                $builder
                    ->add('accessoireAspirateur', EntityType::class, [
                        'label' => 'Accessoire Aspirateur',
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => AccessoireAspirateur::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('vitrage' === $filter->getName()) {
                $builder
                    ->add('vitrage', EntityType::class, [
                        'label' => 'Vitrage',
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => Vitrage::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('typeRaccord' === $filter->getName()) {
                $builder
                    ->add('typeRaccord', EntityType::class, [
                        'label' => 'Type Raccord',
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => TypeRaccord::class,
                        'expanded' => true,
                        'label_html' => true,
                          'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('typeMateriel' === $filter->getName()) {
                $builder
                    ->add('typeMateriel', EntityType::class, [
                        'label' => 'Type Matériel',
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => TypeMateriel::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('materiel' === $filter->getName()) {
                $builder
                    ->add('materiel', EntityType::class, [
                        'label' => 'Matériel',
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => Materiel::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('colleJoint' === $filter->getName()) {
                $builder
                    ->add('colleJoint', EntityType::class, [
                        'label' => 'Colle Joint',
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => ColleJoint::class,
                        'expanded' => true,
                        'label_html' => true,
                         'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('adhesif' === $filter->getName()) {
                $builder
                    ->add('adhesif', EntityType::class, [
                        'label' => 'Adhésif',
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => Adhesif::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('typePonceuse' === $filter->getName()) {
                $builder
                    ->add('typePonceuse', EntityType::class, [
                        'label' => 'Type Ponceuse',
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => TypePonceuse::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('typeMastic' === $filter->getName()) {
                $builder
                    ->add('typeMastic', EntityType::class, [
                        'label' => 'Type Mastic',
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => TypeMastic::class,
                        'expanded' => true,
                        'label_html' => true,
                         'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('typeDiluant' === $filter->getName()) {
                $builder
                    ->add('typeDiluant', EntityType::class, [
                        'label' => 'Type Diluant',
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => TypeDiluant::class,
                        'expanded' => true,
                        'label_html' => true,
                      	'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }

            if ('accessoire' === $filter->getName()) {
                $builder
                    ->add('accessoire', EntityType::class, [
                        'label' => 'Accessoire',
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => Accessoire::class,
                        'expanded' => true,
                        'label_html' => true,
                          'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }
            if ('accessoirePolisseuse' === $filter->getName()) {
                $builder
                    ->add('accessoirePolisseuse', EntityType::class, [
                        'label' => 'Accessoire Polisseuse',
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => AccessoirePolisseuse::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }
            if ('debosseleurMarteau' === $filter->getName()) {
                $builder
                    ->add('debosseleurMarteau', EntityType::class, [
                        'label' => 'Débosseleur Marteau',
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => DebosseleurMarteau::class,
                        'expanded' => true,
                        'label_html' => true,
                       'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }
            if ('equerreTirageGriffe' === $filter->getName()) {
                $builder
                    ->add('equerreTirageGriffe', EntityType::class, [
                        'label' => 'Équerre Tirage Griffe',
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => EquerreTirageGriffe::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }
            if ('chaineRaccord' === $filter->getName()) {
                $builder
                    ->add('chaineRaccord', EntityType::class, [
                        'label' => 'Chaîne Raccord',
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => ChaineRaccord::class,
                        'expanded' => true,
                        'label_html' => true,
                         'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }
            if ('barreDebosslage' === $filter->getName()) {
                $builder
                    ->add('barreDebosslage', EntityType::class, [
                        'label' => 'Barre de Débosselage',
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => BarreDebosslage::class,
                        'expanded' => true,
                        'label_html' => true,
                        'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }
            if ('masseMagnetiqueChario' === $filter->getName()) {
                $builder
                    ->add('masseMagnetiqueChario', EntityType::class, [
                        'label' => 'Masse Magnétique Chariot',
                        'required' => false,
                        'placeholder' => 'Aucun filtre', // Texte affiché pour l'option vide
                    'empty_data' => null, // La valeur par défaut si rien n'est sélectionné
                        'class' => MasseMagnetiqueChario::class,
                        'expanded' => true,
                        'label_html' => true,
                          'query_builder' => function (EntityRepository $er) use ($category) {
                        return $er->createQueryBuilder('test')
                            ->join('test.produit', 'p')  // Joint Malette et Product
                            ->join('p.Category', 'c')  // Joint Product et Category
                            ->where('c.id = :category') // Filtrer par catégorie courante
                            ->andWhere('p.stock > 0')    // Filtrer les produits avec du stock
                            ->setParameter('category', $category->getId())
                            ->groupBy('test.id');         // Éviter les doublons
                    },
                    ]);
            }
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'category' => null,
            'filters' => [],
            'current_category' => null, // Option pour passer la catégorie courante

          
        ]);
    }
}
