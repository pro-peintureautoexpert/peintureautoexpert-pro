<?php

namespace App\Repository;

use App\Entity\ConMastic;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ConMastic>
 *
 * @method ConMastic|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConMastic|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConMastic[]    findAll()
 * @method ConMastic[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConMasticRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConMastic::class);
    }

    //    /**
    //     * @return ConMastic[] Returns an array of ConMastic objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ConMastic
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
