<?php

namespace App\Repository;

use App\Entity\Malette;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Malette>
 *
 * @method Malette|null find($id, $lockMode = null, $lockVersion = null)
 * @method Malette|null findOneBy(array $criteria, array $orderBy = null)
 * @method Malette[]    findAll()
 * @method Malette[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MaletteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Malette::class);
    }

    //    /**
    //     * @return Malette[] Returns an array of Malette objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('m')
    //            ->andWhere('m.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('m.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Malette
    //    {
    //        return $this->createQueryBuilder('m')
    //            ->andWhere('m.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
