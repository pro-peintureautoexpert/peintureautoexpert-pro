<?php

namespace App\Repository;

use App\Entity\CouleurAll;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CouleurAll>
 *
 * @method CouleurAll|null find($id, $lockMode = null, $lockVersion = null)
 * @method CouleurAll|null findOneBy(array $criteria, array $orderBy = null)
 * @method CouleurAll[]    findAll()
 * @method CouleurAll[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CouleurAllRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CouleurAll::class);
    }

    //    /**
    //     * @return CouleurAll[] Returns an array of CouleurAll objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?CouleurAll
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
