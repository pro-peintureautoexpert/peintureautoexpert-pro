<?php

namespace App\Repository;

use App\Entity\AcierAlu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AcierAlu>
 *
 * @method AcierAlu|null find($id, $lockMode = null, $lockVersion = null)
 * @method AcierAlu|null findOneBy(array $criteria, array $orderBy = null)
 * @method AcierAlu[]    findAll()
 * @method AcierAlu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AcierAluRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AcierAlu::class);
    }

    //    /**
    //     * @return AcierAlu[] Returns an array of AcierAlu objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('a.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?AcierAlu
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
