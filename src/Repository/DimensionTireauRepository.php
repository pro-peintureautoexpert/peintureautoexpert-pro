<?php

namespace App\Repository;

use App\Entity\DimensionTireau;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DimensionTireau>
 *
 * @method DimensionTireau|null find($id, $lockMode = null, $lockVersion = null)
 * @method DimensionTireau|null findOneBy(array $criteria, array $orderBy = null)
 * @method DimensionTireau[]    findAll()
 * @method DimensionTireau[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DimensionTireauRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DimensionTireau::class);
    }

    //    /**
    //     * @return DimensionTireau[] Returns an array of DimensionTireau objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('d.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?DimensionTireau
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
