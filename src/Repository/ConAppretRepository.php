<?php

namespace App\Repository;

use App\Entity\ConAppret;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ConAppret>
 *
 * @method ConAppret|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConAppret|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConAppret[]    findAll()
 * @method ConAppret[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConAppretRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConAppret::class);
    }

    //    /**
    //     * @return ConAppret[] Returns an array of ConAppret objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ConAppret
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
