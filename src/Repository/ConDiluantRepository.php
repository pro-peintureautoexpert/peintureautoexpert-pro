<?php

namespace App\Repository;

use App\Entity\ConDiluant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ConDiluant>
 *
 * @method ConDiluant|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConDiluant|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConDiluant[]    findAll()
 * @method ConDiluant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConDiluantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConDiluant::class);
    }

    //    /**
    //     * @return ConDiluant[] Returns an array of ConDiluant objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ConDiluant
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
