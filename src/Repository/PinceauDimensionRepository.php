<?php

namespace App\Repository;

use App\Entity\PinceauDimension;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<PinceauDimension>
 *
 * @method PinceauDimension|null find($id, $lockMode = null, $lockVersion = null)
 * @method PinceauDimension|null findOneBy(array $criteria, array $orderBy = null)
 * @method PinceauDimension[]    findAll()
 * @method PinceauDimension[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PinceauDimensionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PinceauDimension::class);
    }

    //    /**
    //     * @return PinceauDimension[] Returns an array of PinceauDimension objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('p.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?PinceauDimension
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
