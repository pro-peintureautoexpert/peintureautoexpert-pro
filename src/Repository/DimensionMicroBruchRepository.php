<?php

namespace App\Repository;

use App\Entity\DimensionMicroBruch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DimensionMicroBruch>
 *
 * @method DimensionMicroBruch|null find($id, $lockMode = null, $lockVersion = null)
 * @method DimensionMicroBruch|null findOneBy(array $criteria, array $orderBy = null)
 * @method DimensionMicroBruch[]    findAll()
 * @method DimensionMicroBruch[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DimensionMicroBruchRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DimensionMicroBruch::class);
    }

    //    /**
    //     * @return DimensionMicroBruch[] Returns an array of DimensionMicroBruch objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('d.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?DimensionMicroBruch
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
