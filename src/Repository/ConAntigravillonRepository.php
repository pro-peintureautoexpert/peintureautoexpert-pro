<?php

namespace App\Repository;

use App\Entity\ConAntigravillon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ConAntigravillon>
 *
 * @method ConAntigravillon|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConAntigravillon|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConAntigravillon[]    findAll()
 * @method ConAntigravillon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConAntigravillonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConAntigravillon::class);
    }

    //    /**
    //     * @return ConAntigravillon[] Returns an array of ConAntigravillon objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ConAntigravillon
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
