<?php

namespace App\Repository;

use App\Entity\TemporaryOrder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TemporaryOrder>
 *
 * @method TemporaryOrder|null find($id, $lockMode = null, $lockVersion = null)
 * @method TemporaryOrder|null findOneBy(array $criteria, array $orderBy = null)
 * @method TemporaryOrder[]    findAll()
 * @method TemporaryOrder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TemporaryOrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TemporaryOrder::class);
    }

    //    /**
    //     * @return TemporaryOrder[] Returns an array of TemporaryOrder objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('t')
    //            ->andWhere('t.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('t.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?TemporaryOrder
    //    {
    //        return $this->createQueryBuilder('t')
    //            ->andWhere('t.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
