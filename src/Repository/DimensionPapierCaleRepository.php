<?php

namespace App\Repository;

use App\Entity\DimensionPapierCale;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DimensionPapierCale>
 *
 * @method DimensionPapierCale|null find($id, $lockMode = null, $lockVersion = null)
 * @method DimensionPapierCale|null findOneBy(array $criteria, array $orderBy = null)
 * @method DimensionPapierCale[]    findAll()
 * @method DimensionPapierCale[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DimensionPapierCaleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DimensionPapierCale::class);
    }

    //    /**
    //     * @return DimensionPapierCale[] Returns an array of DimensionPapierCale objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('d.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?DimensionPapierCale
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
