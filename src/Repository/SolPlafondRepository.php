<?php

namespace App\Repository;

use App\Entity\SolPlafond;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SolPlafond>
 *
 * @method SolPlafond|null find($id, $lockMode = null, $lockVersion = null)
 * @method SolPlafond|null findOneBy(array $criteria, array $orderBy = null)
 * @method SolPlafond[]    findAll()
 * @method SolPlafond[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SolPlafondRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SolPlafond::class);
    }

    //    /**
    //     * @return SolPlafond[] Returns an array of SolPlafond objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('s.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?SolPlafond
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
