<?php

namespace App\Repository;

use App\Entity\Ponceuse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Ponceuse>
 *
 * @method Ponceuse|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ponceuse|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ponceuse[]    findAll()
 * @method Ponceuse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PonceuseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ponceuse::class);
    }

    //    /**
    //     * @return Ponceuse[] Returns an array of Ponceuse objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('p.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Ponceuse
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
