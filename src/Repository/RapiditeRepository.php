<?php

namespace App\Repository;

use App\Entity\Rapidite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Rapidite>
 *
 * @method Rapidite|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rapidite|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rapidite[]    findAll()
 * @method Rapidite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RapiditeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rapidite::class);
    }

    //    /**
    //     * @return Rapidite[] Returns an array of Rapidite objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('r.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Rapidite
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
