<?php

namespace App\Repository;

use App\Entity\DimensionDoubleFace;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DimensionDoubleFace>
 *
 * @method DimensionDoubleFace|null find($id, $lockMode = null, $lockVersion = null)
 * @method DimensionDoubleFace|null findOneBy(array $criteria, array $orderBy = null)
 * @method DimensionDoubleFace[]    findAll()
 * @method DimensionDoubleFace[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DimensionDoubleFaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DimensionDoubleFace::class);
    }

    //    /**
    //     * @return DimensionDoubleFace[] Returns an array of DimensionDoubleFace objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('d.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?DimensionDoubleFace
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
