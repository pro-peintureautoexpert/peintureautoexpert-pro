<?php

namespace App\Repository;

use App\Entity\DimensionKraft;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DimensionKraft>
 *
 * @method DimensionKraft|null find($id, $lockMode = null, $lockVersion = null)
 * @method DimensionKraft|null findOneBy(array $criteria, array $orderBy = null)
 * @method DimensionKraft[]    findAll()
 * @method DimensionKraft[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DimensionKraftRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DimensionKraft::class);
    }

    //    /**
    //     * @return DimensionKraft[] Returns an array of DimensionKraft objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('d.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?DimensionKraft
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
