<?php

namespace App\Repository;

use App\Entity\DimensionChariotKraft;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DimensionChariotKraft>
 *
 * @method DimensionChariotKraft|null find($id, $lockMode = null, $lockVersion = null)
 * @method DimensionChariotKraft|null findOneBy(array $criteria, array $orderBy = null)
 * @method DimensionChariotKraft[]    findAll()
 * @method DimensionChariotKraft[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DimensionChariotKraftRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DimensionChariotKraft::class);
    }

    //    /**
    //     * @return DimensionChariotKraft[] Returns an array of DimensionChariotKraft objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('d.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?DimensionChariotKraft
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
