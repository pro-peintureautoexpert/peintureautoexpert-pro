<?php

namespace App\Repository;

use App\Entity\Order;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Order>
 *
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    //    /**
    //     * @return Order[] Returns an array of Order objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('o')
    //            ->andWhere('o.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('o.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Order
    //    {
    //        return $this->createQueryBuilder('o')
    //            ->andWhere('o.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
    //  Calculs les chiffres d'affaire par mois pendant 6 mois:

    public function getChiffreAffaireMensuelSurSixMois(User $user)
    {
        $dateSixMois = (new \DateTime())->modify('-6 months');

        $qb = $this->createQueryBuilder('o')
            ->join('o.orderDetails', 'child')  // Joindre la relation 'orderDetails' de la commande
            ->select('MONTH(o.createdAt) AS mois, YEAR(o.createdAt) AS annee, SUM(child.ProductQuantity * child.productPrice) AS chiffreAffaire')  // Correction de la faute de frappe sur 'productQuantity'
            ->where('o.createdAt >= :dateSixMois')  // Filtrage des commandes des six derniers mois
            ->andWhere('o.user = :user' )
            ->andwhere('o.statePay = 2')
            ->setParameter('dateSixMois', $dateSixMois, Types::DATE_MUTABLE)  // Passer la date des six derniers mois
            ->setParameter('user', $user)
            ->groupBy('annee, mois')  // Regrouper par année et mois
            ->orderBy('annee', 'DESC')  // Trier d'abord par année, puis par mois
            ->addOrderBy('mois');  // Trier par mois

        return $qb->getQuery()->getResult();
    }


    public function getOrderSurUnAn( User $user)
    {
        // Date du début de l'année
        $debutAnnee = (new \DateTime())->modify('-6 Months');
        
        // Date actuelle (aujourd'hui)
        $aujourdhui = new \DateTime('now');
    
        $qb = $this->createQueryBuilder('o')
            ->join('o.orderDetails', 'od')  // Joindre la relation 'orderDetails' de la commande
            ->select('od.category AS categoryName, 
                      SUM(od.ProductQuantity * od.productPrice) AS chiffreAffaire')
            ->where('o.createdAt >= :debutAnnee')  // Filtrage des commandes 
            ->andWhere('o.user = :user' )
            ->andWhere('o.statePay = 2')           // Assurez-vous que `statePay` est bien une colonne valide
            ->setParameter('user', $user)
            ->setParameter('debutAnnee', $debutAnnee, Types::DATE_MUTABLE)
            ->groupBy('categoryName')              // Regrouper par année et catégorie
            ->addOrderBy('chiffreAffaire','DESC' ); // Trier par chiffre d'affaires en plus, si nécessaire
    
        return $qb->getQuery()->getResult();
    }
    



}
