<?php

namespace App\Repository;

use App\Entity\ConSavon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ConSavon>
 *
 * @method ConSavon|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConSavon|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConSavon[]    findAll()
 * @method ConSavon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConSavonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConSavon::class);
    }

    //    /**
    //     * @return ConSavon[] Returns an array of ConSavon objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ConSavon
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
