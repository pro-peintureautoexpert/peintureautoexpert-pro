<?php

namespace App\Repository;

use App\Entity\FilterDimensionCale;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<FilterDimensionCale>
 *
 * @method FilterDimensionCale|null find($id, $lockMode = null, $lockVersion = null)
 * @method FilterDimensionCale|null findOneBy(array $criteria, array $orderBy = null)
 * @method FilterDimensionCale[]    findAll()
 * @method FilterDimensionCale[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilterDimensionCaleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FilterDimensionCale::class);
    }

    //    /**
    //     * @return FilterDimensionCale[] Returns an array of FilterDimensionCale objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('f')
    //            ->andWhere('f.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('f.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?FilterDimensionCale
    //    {
    //        return $this->createQueryBuilder('f')
    //            ->andWhere('f.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
