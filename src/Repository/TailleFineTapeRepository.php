<?php

namespace App\Repository;

use App\Entity\TailleFineTape;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TailleFineTape>
 *
 * @method TailleFineTape|null find($id, $lockMode = null, $lockVersion = null)
 * @method TailleFineTape|null findOneBy(array $criteria, array $orderBy = null)
 * @method TailleFineTape[]    findAll()
 * @method TailleFineTape[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TailleFineTapeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TailleFineTape::class);
    }

    //    /**
    //     * @return TailleFineTape[] Returns an array of TailleFineTape objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('t')
    //            ->andWhere('t.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('t.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?TailleFineTape
    //    {
    //        return $this->createQueryBuilder('t')
    //            ->andWhere('t.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
