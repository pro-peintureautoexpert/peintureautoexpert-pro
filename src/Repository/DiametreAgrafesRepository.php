<?php

namespace App\Repository;

use App\Entity\DiametreAgrafes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DiametreAgrafes>
 *
 * @method DiametreAgrafes|null find($id, $lockMode = null, $lockVersion = null)
 * @method DiametreAgrafes|null findOneBy(array $criteria, array $orderBy = null)
 * @method DiametreAgrafes[]    findAll()
 * @method DiametreAgrafes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DiametreAgrafesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DiametreAgrafes::class);
    }

    //    /**
    //     * @return DiametreAgrafes[] Returns an array of DiametreAgrafes objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('d.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?DiametreAgrafes
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
