<?php

namespace App\Repository;

use App\Entity\FiltreCone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<FiltreCone>
 *
 * @method FiltreCone|null find($id, $lockMode = null, $lockVersion = null)
 * @method FiltreCone|null findOneBy(array $criteria, array $orderBy = null)
 * @method FiltreCone[]    findAll()
 * @method FiltreCone[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FiltreConeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FiltreCone::class);
    }

    //    /**
    //     * @return FiltreCone[] Returns an array of FiltreCone objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('f')
    //            ->andWhere('f.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('f.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?FiltreCone
    //    {
    //        return $this->createQueryBuilder('f')
    //            ->andWhere('f.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
