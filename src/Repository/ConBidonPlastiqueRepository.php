<?php

namespace App\Repository;

use App\Entity\ConBidonPlastique;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ConBidonPlastique>
 *
 * @method ConBidonPlastique|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConBidonPlastique|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConBidonPlastique[]    findAll()
 * @method ConBidonPlastique[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConBidonPlastiqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConBidonPlastique::class);
    }

    //    /**
    //     * @return ConBidonPlastique[] Returns an array of ConBidonPlastique objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ConBidonPlastique
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
