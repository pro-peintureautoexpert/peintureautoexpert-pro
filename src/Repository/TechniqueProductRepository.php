<?php

namespace App\Repository;

use App\Entity\TechniqueProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TechniqueProduct>
 *
 * @method TechniqueProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method TechniqueProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method TechniqueProduct[]    findAll()
 * @method TechniqueProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TechniqueProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TechniqueProduct::class);
    }

    //    /**
    //     * @return TechniqueProduct[] Returns an array of TechniqueProduct objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('t')
    //            ->andWhere('t.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('t.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?TechniqueProduct
    //    {
    //        return $this->createQueryBuilder('t')
    //            ->andWhere('t.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
