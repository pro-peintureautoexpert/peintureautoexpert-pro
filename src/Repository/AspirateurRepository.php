<?php

namespace App\Repository;

use App\Entity\Aspirateur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Aspirateur>
 *
 * @method Aspirateur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Aspirateur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Aspirateur[]    findAll()
 * @method Aspirateur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AspirateurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Aspirateur::class);
    }

    //    /**
    //     * @return Aspirateur[] Returns an array of Aspirateur objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('a.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Aspirateur
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
