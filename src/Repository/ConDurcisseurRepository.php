<?php

namespace App\Repository;

use App\Entity\ConDurcisseur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ConDurcisseur>
 *
 * @method ConDurcisseur|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConDurcisseur|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConDurcisseur[]    findAll()
 * @method ConDurcisseur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConDurcisseurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConDurcisseur::class);
    }

    //    /**
    //     * @return ConDurcisseur[] Returns an array of ConDurcisseur objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ConDurcisseur
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
