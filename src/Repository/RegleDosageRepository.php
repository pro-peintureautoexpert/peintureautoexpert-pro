<?php

namespace App\Repository;

use App\Entity\RegleDosage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RegleDosage>
 *
 * @method RegleDosage|null find($id, $lockMode = null, $lockVersion = null)
 * @method RegleDosage|null findOneBy(array $criteria, array $orderBy = null)
 * @method RegleDosage[]    findAll()
 * @method RegleDosage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegleDosageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RegleDosage::class);
    }

    //    /**
    //     * @return RegleDosage[] Returns an array of RegleDosage objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('r.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?RegleDosage
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
