<?php

namespace App\Repository;

use App\Entity\ConVernis;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ConVernis>
 *
 * @method ConVernis|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConVernis|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConVernis[]    findAll()
 * @method ConVernis[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConVernisRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConVernis::class);
    }

    //    /**
    //     * @return ConVernis[] Returns an array of ConVernis objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ConVernis
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
