<?php

namespace App\Repository;

use App\Entity\ConPolish;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ConPolish>
 *
 * @method ConPolish|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConPolish|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConPolish[]    findAll()
 * @method ConPolish[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConPolishRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConPolish::class);
    }

    //    /**
    //     * @return ConPolish[] Returns an array of ConPolish objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ConPolish
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
