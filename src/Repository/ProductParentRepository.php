<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\ProductParent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ProductParent>
 *
 * @method ProductParent|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductParent|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductParent[]    findAll()
 * @method ProductParent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductParentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductParent::class);
    }

    //    /**
    //     * @return ProductParent[] Returns an array of ProductParent objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('p.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ProductParent
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
    /**
     * @return ProductParent[] Returns an array of ProductParent objects
     */
    public function findByString($value): array
    {
        return $this->createQueryBuilder('p')
            ->leftJoin('p.products', 'child') // Utilisation d'un join approprié
            ->where('p.name LIKE :val') // Condition sur le nom du parent
            ->orWhere('child.articleCode LIKE :val') // Condition sur le code article de l'enfant
            ->setParameter('val', '%'.$value.'%') // Assignation du paramètre
            ->getQuery()
            ->getResult(); // Récupération des résultats
    }

    public function findByFilters(Category $category, $filters)
    {
        $qb = $this->createQueryBuilder('p')
        ->where('p.category = :category')
        ->setParameter('category', $category);

        // Une seule jointure pour tous les filtres sur `p.products`
        $qb->join('p.products', 'child');

        // Ajout des filtres avec l'alias `child`
        if (!empty($filters['brand'])) {
            $qb->andWhere('p.brand = :brand')
            ->setParameter('brand', $filters['brand']);
        }

        if (!empty($filters['grain'])) {
            $qb->andWhere('child.grain = :grain')
            ->setParameter('grain', $filters['grain']);
        }

        if (!empty($filters['conMastic'])) {
            $qb->andWhere('child.conMastic = :conMastic')
            ->setParameter('conMastic', $filters['conMastic']);
        }

        if (!empty($filters['conAppret'])) {
            $qb->andWhere('child.conAppret = :conAppret')
            ->setParameter('conAppret', $filters['conAppret']);
        }

        if (!empty($filters['conVernis'])) {
            $qb->andWhere('child.conVernis = :conVernis')
            ->setParameter('conVernis', $filters['conVernis']);
        }

        if (!empty($filters['rapidite'])) {
            $qb->andWhere('child.rapidite = :rapidite')
            ->setParameter('rapidite', $filters['rapidite']);
        }

        if (!empty($filters['malette'])) {
            $qb->andWhere('child.malette = :malette')
            ->setParameter('malette', $filters['malette']);
        }

        if (!empty($filters['couleurAll'])) {
            $qb->andWhere('child.couleurAll = :couleurAll')
            ->setParameter('couleurAll', $filters['couleurAll']);
        }

        if (!empty($filters['filterDimensionCale'])) {
            $qb->andWhere('child.filterDimensionCale = :filterDimensionCale')
            ->setParameter('filterDimensionCale', $filters['filterDimensionCale']);
        }

        if (!empty($filters['dimensionDoubleFace'])) {
            $qb->andWhere('child.dimensionDoubleFace = :dimensionDoubleFace')
            ->setParameter('dimensionDoubleFace', $filters['dimensionDoubleFace']);
        }

        if (!empty($filters['buse'])) {
            $qb->andWhere('child.buse = :buse')
            ->setParameter('buse', $filters['buse']);
        }
        if (!empty($filters['conAntigravillon'])) {
            $qb->andWhere('child.conAntigravillon = :conAntigravillon')
                ->setParameter('conAntigravillon', $filters['conAntigravillon']);
        }

        if (!empty($filters['conDegraissant'])) {
            $qb->andWhere('child.conDegraissant = :conDegraissant')
                ->setParameter('conDegraissant', $filters['conDegraissant']);
        }

        if (!empty($filters['capaciteAspirateur'])) {
            $qb->andWhere('child.capaciteAspirateur = :capaciteAspirateur')
                ->setParameter('capaciteAspirateur', $filters['capaciteAspirateur']);
        }

        if (!empty($filters['conPolish'])) {
            $qb->andWhere('child.conPolish = :conPolish')
                ->setParameter('conPolish', $filters['conPolish']);
        }

        if (!empty($filters['conSavon'])) {
            $qb->andWhere('child.conSavon = :conSavon')
                ->setParameter('conSavon', $filters['conSavon']);
        }

        if (!empty($filters['conDiluant'])) {
            $qb->andWhere('child.conDiluant = :conDiluant')
                ->setParameter('conDiluant', $filters['conDiluant']);
        }

        if (!empty($filters['conGobelet'])) {
            $qb->andWhere('child.conGobelet = :conGobelet')
                ->setParameter('conGobelet', $filters['conGobelet']);
        }

        if (!empty($filters['conBidonPlastique'])) {
            $qb->andWhere('child.conBidonPlastique = :conBidonPlastique')
                ->setParameter('conBidonPlastique', $filters['conBidonPlastique']);
        }

        if (!empty($filters['conDurcisseur'])) {
            $qb->andWhere('child.conDurcisseur = :conDurcisseur')
                ->setParameter('conDurcisseur', $filters['conDurcisseur']);
        }

        if (!empty($filters['dimansionFiltreCabine'])) {
            $qb->andWhere('child.dimansionFiltreCabine = :dimansionFiltreCabine')
                ->setParameter('dimansionFiltreCabine', $filters['dimansionFiltreCabine']);
        }

        if (!empty($filters['diametreZikzak'])) {
            $qb->andWhere('child.diametreZikzak = :diametreZikzak')
                ->setParameter('diametreZikzak', $filters['diametreZikzak']);
        }

        if (!empty($filters['diametreAgrafes'])) {
            $qb->andWhere('child.diametreAgrafes = :diametreAgrafes')
                ->setParameter('diametreAgrafes', $filters['diametreAgrafes']);
        }

        if (!empty($filters['dimensionChariotKraft'])) {
            $qb->andWhere('child.dimensionChariotKraft = :dimensionChariotKraft')
                ->setParameter('dimensionChariotKraft', $filters['dimensionChariotKraft']);
        }

        if (!empty($filters['dimensionPapierCale'])) {
            $qb->andWhere('child.dimensionPapierCale = :dimensionPapierCale')
                ->setParameter('dimensionPapierCale', $filters['dimensionPapierCale']);
        }

        if (!empty($filters['filtreCone'])) {
            $qb->andWhere('child.filtreCone = :filtreCone')
                ->setParameter('filtreCone', $filters['filtreCone']);
        }

        if (!empty($filters['dimensionMicroBruch'])) {
            $qb->andWhere('child.dimensionMicroBruch = :dimensionMicroBruch')
                ->setParameter('dimensionMicroBruch', $filters['dimensionMicroBruch']);
        }

        if (!empty($filters['pinceauDimension'])) {
            $qb->andWhere('child.pinceauDimension = :pinceauDimension')
                ->setParameter('pinceauDimension', $filters['pinceauDimension']);
        }

        if (!empty($filters['regleDosage'])) {
            $qb->andWhere('child.regleDosage = :regleDosage')
                ->setParameter('regleDosage', $filters['regleDosage']);
        }

        if (!empty($filters['dimensionKraft'])) {
            $qb->andWhere('child.dimensionKraft = :dimensionKraft')
                ->setParameter('dimensionKraft', $filters['dimensionKraft']);
        }

        if (!empty($filters['dimensionTireau'])) {
            $qb->andWhere('child.dimensionTireau = :dimensionTireau')
                ->setParameter('dimensionTireau', $filters['dimensionTireau']);
        }

        if (!empty($filters['tailleFineTape'])) {
            $qb->andWhere('child.tailleFineTape = :tailleFineTape')
                ->setParameter('tailleFineTape', $filters['tailleFineTape']);
        }

        if (!empty($filters['effet'])) {
            $qb->andWhere('child.effet = :effet')
                ->setParameter('effet', $filters['effet']);
        }

        if (!empty($filters['aspirateur'])) {
            $qb->andWhere('child.aspirateur = :aspirateur')
                ->setParameter('aspirateur', $filters['aspirateur']);
        }

        if (!empty($filters['ponceuse'])) {
            $qb->andWhere('child.ponceuse = :ponceuse')
                ->setParameter('ponceuse', $filters['ponceuse']);
        }

        if (!empty($filters['typePistole'])) {
            $qb->andWhere('child.typePistole = :typePistole')
                ->setParameter('typePistole', $filters['typePistole']);
        }

        if (!empty($filters['solPlafond'])) {
            $qb->andWhere('child.solPlafond = :solPlafond')
                ->setParameter('solPlafond', $filters['solPlafond']);
        }

        if (!empty($filters['acierAlu'])) {
            $qb->andWhere('child.acierAlu = :acierAlu')
                ->setParameter('acierAlu', $filters['acierAlu']);
        }

        if (!empty($filters['dimensionAll'])) {
            $qb->andWhere('child.dimensionAll = :dimensionAll')
                ->setParameter('dimensionAll', $filters['dimensionAll']);
        }

        if (!empty($filters['taille'])) {
            $qb->andWhere('child.taille = :taille')
                ->setParameter('taille', $filters['taille']);
        }

        if (!empty($filters['matiere'])) {
            $qb->andWhere('child.matiere = :matiere')
                ->setParameter('matiere', $filters['matiere']);
        }
        if (!empty($filters['masquage'])) {
            $qb->andWhere('child.masquage = :masquage')
                ->setParameter('masquage', $filters['masquage']);
        }

        if (!empty($filters['nettoyage'])) {
            $qb->andWhere('child.nettoyage = :nettoyage')
                ->setParameter('nettoyage', $filters['nettoyage']);
        }

        if (!empty($filters['durcisseur'])) {
            $qb->andWhere('child.durcisseur = :durcisseur')
                ->setParameter('durcisseur', $filters['durcisseur']);
        }

        if (!empty($filters['accessoireAspirateur'])) {
            $qb->andWhere('child.accessoireAspirateur = :accessoireAspirateur')
                ->setParameter('accessoireAspirateur', $filters['accessoireAspirateur']);
        }

        if (!empty($filters['vitrage'])) {
            $qb->andWhere('child.vitrage = :vitrage')
                ->setParameter('vitrage', $filters['vitrage']);
        }

        if (!empty($filters['typeRaccord'])) {
            $qb->andWhere('child.typeRaccord = :typeRaccord')
                ->setParameter('typeRaccord', $filters['typeRaccord']);
        }

        if (!empty($filters['typeMateriel'])) {
            $qb->andWhere('child.typeMateriel = :typeMateriel')
                ->setParameter('typeMateriel', $filters['typeMateriel']);
        }

        if (!empty($filters['materiel'])) {
            $qb->andWhere('child.materiel = :materiel')
                ->setParameter('materiel', $filters['materiel']);
        }

        if (!empty($filters['colleJoint'])) {
            $qb->andWhere('child.colleJoint = :colleJoint')
                ->setParameter('colleJoint', $filters['colleJoint']);
        }

        if (!empty($filters['adhesif'])) {
            $qb->andWhere('child.adhesif = :adhesif')
                ->setParameter('adhesif', $filters['adhesif']);
        }

        if (!empty($filters['typePonceuse'])) {
            $qb->andWhere('child.typePonceuse = :typePonceuse')
                ->setParameter('typePonceuse', $filters['typePonceuse']);
        }

        if (!empty($filters['typeMastic'])) {
            $qb->andWhere('child.typeMastic = :typeMastic')
                ->setParameter('typeMastic', $filters['typeMastic']);
        }

        if (!empty($filters['typeDiluant'])) {
            $qb->andWhere('child.typeDiluant = :typeDiluant')
                ->setParameter('typeDiluant', $filters['typeDiluant']);
        }

        if (!empty($filters['accessoire'])) {
            $qb->andWhere('child.accessoire = :accessoire')
                ->setParameter('accessoire', $filters['accessoire']);
        }
        if (!empty($filters['accessoirePolisseuse'])) {
            $qb->andWhere('child.accessoirePolisseuse = :accessoirePolisseuse')
                ->setParameter('accessoirePolisseuse', $filters['accessoirePolisseuse']);
        }
        if (!empty($filters['debosseleurMarteau'])) {
            $qb->andWhere('child.debosseleurMarteau = :debosseleurMarteau')
                ->setParameter('debosseleurMarteau', $filters['debosseleurMarteau']);
        }
        if (!empty($filters['equerreTirageGriffe'])) {
            $qb->andWhere('child.equerreTirageGriffe = :equerreTirageGriffe')
                ->setParameter('equerreTirageGriffe', $filters['equerreTirageGriffe']);
        }
        if (!empty($filters['chaineRaccord'])) {
            $qb->andWhere('child.chaineRaccord = :chaineRaccord')
                ->setParameter('chaineRaccord', $filters['chaineRaccord']);
        }
        if (!empty($filters['barreDebosslage'])) {
            $qb->andWhere('child.barreDebosslage = :barreDebosslage')
                ->setParameter('barreDebosslage', $filters['barreDebosslage']);
        }
        if (!empty($filters['masseMagnetiqueChario'])) {
            $qb->andWhere('child.masseMagnetiqueChario = :masseMagnetiqueChario')
                ->setParameter('masseMagnetiqueChario', $filters['masseMagnetiqueChario']);
        }

        return $qb->getQuery()->getResult();
    }
}
