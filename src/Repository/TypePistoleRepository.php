<?php

namespace App\Repository;

use App\Entity\TypePistole;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TypePistole>
 *
 * @method TypePistole|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypePistole|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypePistole[]    findAll()
 * @method TypePistole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypePistoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypePistole::class);
    }

    //    /**
    //     * @return TypePistole[] Returns an array of TypePistole objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('t')
    //            ->andWhere('t.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('t.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?TypePistole
    //    {
    //        return $this->createQueryBuilder('t')
    //            ->andWhere('t.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
