<?php

namespace App\Repository;

use App\Entity\CapaciteAspirateur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CapaciteAspirateur>
 *
 * @method CapaciteAspirateur|null find($id, $lockMode = null, $lockVersion = null)
 * @method CapaciteAspirateur|null findOneBy(array $criteria, array $orderBy = null)
 * @method CapaciteAspirateur[]    findAll()
 * @method CapaciteAspirateur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CapaciteAspirateurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CapaciteAspirateur::class);
    }

    //    /**
    //     * @return CapaciteAspirateur[] Returns an array of CapaciteAspirateur objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?CapaciteAspirateur
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
