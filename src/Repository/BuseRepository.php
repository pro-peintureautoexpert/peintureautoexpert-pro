<?php

namespace App\Repository;

use App\Entity\Buse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Buse>
 *
 * @method Buse|null find($id, $lockMode = null, $lockVersion = null)
 * @method Buse|null findOneBy(array $criteria, array $orderBy = null)
 * @method Buse[]    findAll()
 * @method Buse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BuseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Buse::class);
    }

    //    /**
    //     * @return Buse[] Returns an array of Buse objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('b')
    //            ->andWhere('b.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('b.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Buse
    //    {
    //        return $this->createQueryBuilder('b')
    //            ->andWhere('b.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
