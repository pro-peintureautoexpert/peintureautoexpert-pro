<?php

namespace App\Repository;

use App\Entity\QtyCarton;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<QtyCarton>
 */
class QtyCartonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, QtyCarton::class);
    }

        /**
         * Finds QtyCarton objects where the 'active' field is true and sorts them by 'orderQty' in ascending order.
         *
         * @return QtyCarton[] Returns an array of QtyCarton objects
         */
        public function findByOrderQty(bool $value = true): array
        {
            return $this->createQueryBuilder('q')
                ->andWhere('q.active = :val')
                ->setParameter('val', $value)
                ->orderBy('q.OrderQte', 'ASC')
                ->getQuery()
                ->getResult();
        }

        /**
         * Finds QtyCarton objects where the 'active' field is true and sorts them by 'orderQty' in ascending order.
         *
         * @return QtyCarton[] Returns an array of QtyCarton objects
         */
        public function findByOrderPalette(bool $value = true): array
        {
            return $this->createQueryBuilder('q')
                ->andWhere('q.inPalette = :val')
                ->setParameter('val', $value)
                ->orderBy('q.OrderPalette', 'ASC')
                ->getQuery()
                ->getResult();
        }

    //    public function findOneBySomeField($value): ?QtyCarton
    //    {
    //        return $this->createQueryBuilder('q')
    //            ->andWhere('q.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
