<?php

namespace App\Repository;

use App\Entity\DimansionFiltreCabine;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DimansionFiltreCabine>
 *
 * @method DimansionFiltreCabine|null find($id, $lockMode = null, $lockVersion = null)
 * @method DimansionFiltreCabine|null findOneBy(array $criteria, array $orderBy = null)
 * @method DimansionFiltreCabine[]    findAll()
 * @method DimansionFiltreCabine[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DimansionFiltreCabineRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DimansionFiltreCabine::class);
    }

    //    /**
    //     * @return DimansionFiltreCabine[] Returns an array of DimansionFiltreCabine objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('d.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?DimansionFiltreCabine
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
