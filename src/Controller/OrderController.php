<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Classe\CartData;
use App\Entity\AddressItem;
use App\Entity\Order;
use App\Entity\OrderDetail;
use App\Form\OrderType;
use App\Repository\CategoryRepository;
use App\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    /*
     * 1ère étape du tunnel d'achat
     * Choix de l'adresse de livraison et du transporteur
     */
    #[Route('/commande/livraison', name: 'app_order')]
    public function index(): Response
    {
        $addresses = $this->getUser()->getAddresses();

        if (0 == count($addresses)) {
            return $this->redirectToRoute('app_account_address_form');
        }

        $form = $this->createForm(OrderType::class, null, [
            'addresses' => $addresses,
            'action' => $this->generateUrl('app_order_summary'),
        ]);

        return $this->renderForm('order/index.html.twig', [
            'deliverForm' => $form,
        ]);
    }

    /*
     * 2ème étape du tunnel d'achat
     * Récap de la commande de l'utilisateur
     * Insertion en base de donnée
     * Préparation du paiement vers Stripe
     */
    #[Route('/commande/recapitulatif', name: 'app_order_summary')]
    public function add(Request $request, CartData $cart,OrderRepository $commandeRepo, EntityManagerInterface $entityManager, CategoryRepository $categoryRepository): Response
    {
        if ('POST' != $request->getMethod()) {
            return $this->redirectToRoute('app_cart');
        }
        $products = $cart->afficher();

        $form = $this->createForm(OrderType::class, null, [
            'addresses' => $this->getUser()->getAddresses(),
        ]);

        $form->handleRequest($request);
        $order = null;
        if ($form->isSubmitted() && $form->isValid()) {
            // Stocker les informations en BDD

            // Création de la chaîne adresse
            $addressObj = $form->get('addresses')->getData();

            $address = $addressObj->getFirstname().' '.$addressObj->getLastname().'<br/>';
            $address .= $addressObj->getAddress().'<br/>';
            $address .= $addressObj->getPostal().' '.$addressObj->getCity().'<br/>';
            $address .= $addressObj->getCountry().'<br/>';
            $address .= $addressObj->getPhone();

            $order = new Order();
            $order->setUser($this->getUser());
            $order->setCreatedAt(new \DateTime());
            $order->setState(1);
            $order->setStatePay(1);
            $order->setCarrierName($form->get('carriers')->getData()->getName());
            $order->setCarrierPrice($form->get('carriers')->getData()->getPrice());
            $order->setDelivrey($address);
            $order->setPicker('Hammad SHAFIQ');

            foreach ($products as $product) {
                $orderDetail = new OrderDetail();
                $orderDetail->setProdcutName($product->getProduit()->getName());
                $orderDetail->setProductIllustartion($product->getProduit()->getIllustration());

                if ($cart->calculatePriceForCartItem($product)  > 0) {
                    $orderDetail->setProductPrice($cart->calculatePriceForCartItem($product));
                } else {
                    $orderDetail->setProductPrice($product->getProduit()->getPriceU());
                }

                $orderDetail->setProductQuantity($product->getQte());
                $orderDetail->setProductTva($product->getProduit()->getTva());
                $orderDetail->setCodeArticle($product->getProduit()->getArticleCode());
                $orderDetail->setSlugProduct($product->getProduit()->getSlug());
                $category = $product->getProduit()->getCategory();
                if (null === $category) {
                    dd('Category is null');
                } else {
                    // Utiliser la méthode findRootCategoryById pour obtenir la catégorie de niveau 1
                    $rootCategory = $categoryRepository->findRootCategoryById($category->getId());
                    if (null === $rootCategory) {
                        dd('Root category is null');
                    } else {
                        $orderDetail->setCategory($rootCategory->getName());
                    }
                }

                $order->addOrderDetail($orderDetail);
            }
            $entityManager->persist($order);

            // Creation l'addresse de colissimo
            $addressColissimo = new AddressItem();
            $addressColissimo->setReference($order);
            $addressColissimo->setCompanyName($addressObj->getCompany());
            $addressColissimo->setLastName($addressObj->getLastname());
            $addressColissimo->setFirstName($addressObj->getFirstname());
            $addressColissimo->setLine0($addressObj->getAddress());
            $addressColissimo->setLine1($addressObj->getCity());
            $addressColissimo->setCountryCode($addressObj->getCountry());
            $addressColissimo->setCity($addressObj->getCity());
            $addressColissimo->setZipCode($addressObj->getPostal());
            $addressColissimo->setPhoneNumber($addressObj->getPhone());
            $addressColissimo->setEmail($this->getUser()->getEmail());

            $entityManager->persist($addressColissimo);
            $entityManager->flush();
        }
                

        // verfication de plafond 
       $verify = false;
        $commandes = $commandeRepo->findBy([
            'user' => $this->getUser(),
            'state' => [2, 3, 4], // États autorisés
            'statePay' => 1, // Commandes payées
        ]);

        // Initialisation des valeurs pour les plafonds
        $total = 0;
        $user = $this->getUser();

        // Vérification que l'utilisateur a bien un magasin avec un plafond défini
        $plafond = $user->getPlafond();
        // if (!$user || !$plafond) {
        //     throw $this->createNotFoundException('Le plafond du magasin n\'est pas défini pour cet utilisateur.');
        // }


        // Calcul du total des commandes
        foreach ($commandes as $commande) {
            $total += $commande->getTotalWt();
        }
        
        if ($total >= $plafond) {
            $this->addFlash('info', 'Un paiement immédiat est nécessaire pour pouvoir passer une commande.');
            // return $this->redirectToRoute('app_account');
            $verify = true;
        }
        
        if ( $cart->getTotalWt() > ($plafond - $total)) {
            $this->addFlash('danger', 'Merci de procéder au règlement de la commande en cours ou d\'une facture impayée.');
            // return $this->redirectToRoute('app_account');
            $verify = true;
        }

        return $this->render('order/summary.html.twig', [
            'choices' => $form->getData(),
            'cart' => $products,
            'cartData'=>$cart,
            'order' => $order,
            'totalWt' => $cart->calculateTotalPrice(),
            'verify' => $verify,
        ], new Response(null, $form->isSubmitted() ? 422 : 200));
    }
}
