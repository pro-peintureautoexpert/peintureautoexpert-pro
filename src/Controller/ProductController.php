<?php

namespace App\Controller;

use App\Form\ProductFiltreType;
use App\Repository\ProductParentRepository;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    #[Route('/produit/{slug}', name: 'app_product')]
    public function index($slug, ProductRepository $productRepository, Request $request, ProductParentRepository $produitParentRepository): Response
    {
        if(!$this->getUser()){
            $this->addFlash('danger', 'Connectez-vous!!.');
            return $this->redirectToRoute('app_login');
        }

        $product = $productRepository->findOneBy(['slug' => $slug]);
        
        if (!$product) {
            return $this->redirectToRoute('app_home');
        }
        
        $productsitem = $product->getProductParent()->getProducts();
        $attribut = $product->getProductParent()->getAbout();

        if(!$productsitem){
            $this->addFlash('danger', 'Produit introuvable!!');
            return $this->redirectToRoute('app_login');
        }


        $form = $this->createForm(ProductFiltreType::class, null, [
            'category' => $product->getCategory()->getId(),
            'filters' => $product->getCategory()->getFilters(),
            'current_category' => $product->getCategory(),

        ]);

        $form->handleRequest($request);

        // if ($form->isSubmitted() && $form->isValid()) {
        //     $filters = $form->getData();
        //     $productsFilter = $productRepository->findByFilters($categoryParent, $filters);
        // } else {
        //     $productsFilter = $productRepository->findByGroup($categoryParent);
        // }

        $productScrolling = $product->getCategory()->getProducts();

        $detailTechnique = $product->getTechniqueProducts();

        return $this->render('product/index.html.twig', [
            'product' => $product,
            'productScrolling' => $productScrolling,
            'detailTechnique' => $detailTechnique,
            'form' => $form->createView(),
            'productsItem' => $productsitem,
            'attribut' => $attribut,
        ]);
    }
}
