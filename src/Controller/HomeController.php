<?php

namespace App\Controller;

use App\Entity\QtyCarton;
use App\Repository\BrandRepository;
use App\Repository\HeaderRepository;
use App\Repository\ProductRepository;
use App\Repository\QtyCartonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(BrandRepository $brandRepository,QtyCartonRepository $qtyCarton, ProductRepository $productRepository, HeaderRepository $headerRepository): Response
    {
        $brands = $brandRepository->findAll();
        $products = $productRepository->findAll();
        $headers = $headerRepository->findAll();

        $quantite = $qtyCarton->findByOrderQty();
        $QtyProd = $qtyCarton-> findByOrderPalette();
    
        return $this->render('home/index.html.twig', [
            'brands' => $brands,
            'products' => $products,
            'headers' => $headers,
            'quantites' => $quantite,
            'qtyProd' => $QtyProd,
        ]);
    }
}
