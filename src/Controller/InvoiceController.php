<?php

namespace App\Controller;

use App\Repository\OrderRepository;
use Dompdf\Dompdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InvoiceController extends AbstractController
{
    /*
     * IMPRESSION FACTURE PDF pour un utilisateur connecté
     * Vérification de la commande pour un utilisateur donné
     */
    #[Route('/compte/facture/impression/{id_order}', name: 'app_invoice_customer')]
    public function printForCustomer(OrderRepository $orderRepository, $id_order): Response
    {
        // 1. Vérification de l'objet commande - Existe ?
        $order = $orderRepository->findOneBy(['id' => $id_order]);

        if (!$order) {
            return $this->redirectToRoute('app_account');
        }

        // 2. Vérification de l'objet commande - Ok pour l'utilisateur ?
        if ($order->getUser() != $this->getUser()) {
            return $this->redirectToRoute('app_account');
        }
        $orderDetails = $order->getOrderDetails();
        $groupedProducts = [];

        foreach ($orderDetails as $orderDetail) {
            $category = $orderDetail->getCategory();
            if (!isset($groupedProducts[$category])) {
                $groupedProducts[$category] = [];
            }
            $groupedProducts[$category][] = $orderDetail;
        }

        $dompdf = new Dompdf();
        $html = $this->renderView('invoice/index.html.twig', [
            'groupedProducts' => $groupedProducts,
            'order' => $order,
        ]);
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        $canvas = $dompdf->getCanvas();
        $font = $dompdf->getFontMetrics()->get_font('Arial, Helvetica, sans-serif', 'normal');
        $size = 10;
        $pageCount = $dompdf->getCanvas()->get_page_count();

        for ($i = 1; $i <= $pageCount; ++$i) {
            $canvas->page_text(510, 820, 'Page {PAGE_NUM} sur {PAGE_COUNT}', $font, $size, [0, 0, 0]);
        }

        $dompdf->stream('facture.pdf', [
            'Attachment' => false,
        ]);

        exit;
    }

    /*
     * IMPRESSION FACTURE PDF pour un administrateur connecté
     * Vérification de la commande pour un utilisateur donné
     */
    #[Route('/admin/facture/impression/{id_order}', name: 'app_invoice_admin')]
    public function printForAdmin(OrderRepository $orderRepository, $id_order): Response
    {
        // 1. Vérification de l'objet commande - Existe ?
        $order = $orderRepository->findOneBy(['id' => $id_order]);

        if (!$order) {
            return $this->redirectToRoute('admin');
        }

        $orderDetails = $order->getOrderDetails();
        $groupedProducts = [];

        foreach ($orderDetails as $orderDetail) {
            $category = $orderDetail->getCategory();
            if (!isset($groupedProducts[$category])) {
                $groupedProducts[$category] = [];
            }
            $groupedProducts[$category][] = $orderDetail;
        }

        $dompdf = new Dompdf();
        $html = $this->renderView('invoice/index.html.twig', [
            'groupedProducts' => $groupedProducts,
            'order' => $order,
        ]);
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $canvas = $dompdf->getCanvas();
        $font = $dompdf->getFontMetrics()->get_font('Arial, Helvetica, sans-serif', 'normal');
        $size = 10;
        $pageCount = $dompdf->getCanvas()->get_page_count();

        for ($i = 1; $i <= $pageCount; ++$i) {
            $canvas->page_text(510, 820, 'Page {PAGE_NUM} sur {PAGE_COUNT}', $font, $size, [0, 0, 0]);
        }
        $dompdf->stream('facture.pdf', [
            'Attachment' => false,
        ]);

        exit;
    }

    #[Route('/admin/commande/impression/{id_order}', name: 'app_invoice_admin_commande')]
    public function printForAdminCommande(OrderRepository $orderRepository, $id_order): Response
    {
        // 1. Vérification de l'objet commande - Existe ?
        $order = $orderRepository->findOneBy(['id' => $id_order]);

        if (!$order) {
            return $this->redirectToRoute('admin');
        }

        $orderDetails = $order->getOrderDetails();
        $groupedProducts = [];

        foreach ($orderDetails as $orderDetail) {
            $category = $orderDetail->getCategory();
            if (!isset($groupedProducts[$category])) {
                $groupedProducts[$category] = [];
            }
            $groupedProducts[$category][] = $orderDetail;
        }

        $dompdf = new Dompdf();
        $html = $this->renderView('invoice/BonCommande.html.twig', [
            'groupedProducts' => $groupedProducts,
            'order' => $order,
        ]);
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $canvas = $dompdf->getCanvas();
        $font = $dompdf->getFontMetrics()->get_font('Arial, Helvetica, sans-serif', 'normal');
        $size = 10;
        $pageCount = $dompdf->getCanvas()->get_page_count();

        for ($i = 1; $i <= $pageCount; ++$i) {
            $canvas->page_text(510, 820, 'Page {PAGE_NUM} sur {PAGE_COUNT}', $font, $size, [0, 0, 0]);
        }
        $dompdf->stream('facture.pdf', [
            'Attachment' => false,
        ]);

        exit;
    }

    /*
    * IMPRESSION FACTURE PDF pour un administrateur connecté
    * Vérification de la commande pour un utilisateur donné
    */
    #[Route('/compte/commande/impression/{id_order}', name: 'app_invoice_customer_commande')]
    public function printForCommande(OrderRepository $orderRepository, $id_order): Response
    {
        // 1. Vérification de l'objet commande - Existe ?
        $order = $orderRepository->findOneBy(['id' => $id_order]);

        if (!$order) {
            return $this->redirectToRoute('app_account');
        }

        // 2. Vérification de l'objet commande - Ok pour l'utilisateur ?
        if ($order->getUser() != $this->getUser()) {
            return $this->redirectToRoute('app_account');
        }
        $orderDetails = $order->getOrderDetails();
        $groupedProducts = [];

        foreach ($orderDetails as $orderDetail) {
            $category = $orderDetail->getCategory();
            if (!isset($groupedProducts[$category])) {
                $groupedProducts[$category] = [];
            }
            $groupedProducts[$category][] = $orderDetail;
        }

        $dompdf = new Dompdf();
        $html = $this->renderView('invoice/BonCommande.html.twig', [
            'groupedProducts' => $groupedProducts,
            'order' => $order,
        ]);
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $canvas = $dompdf->getCanvas();
        $font = $dompdf->getFontMetrics()->get_font('Arial, Helvetica, sans-serif', 'normal');
        $size = 10;
        $pageCount = $dompdf->getCanvas()->get_page_count();

        for ($i = 1; $i <= $pageCount; ++$i) {
            $canvas->page_text(510, 820, 'Page {PAGE_NUM} sur {PAGE_COUNT}', $font, $size, [0, 0, 0]);
        }
        $dompdf->stream('facture'.$order->getId().'.pdf', [
            'Attachment' => false,
        ]);

        exit;
    }
}
