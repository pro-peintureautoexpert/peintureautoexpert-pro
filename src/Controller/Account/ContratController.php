<?php

namespace App\Controller\Account;

use App\Entity\Contrat;
use App\Entity\User;
use App\Form\ContratType;
use App\Repository\ContratRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ContratController extends AbstractController
{
    #[Route('/compte/contrat', name: 'app_contrat')]
    public function index(Request $request, EntityManagerInterface $entityManager, ContratRepository $contratRepository): Response
    {
        $contrats = $contratRepository->findBy(["user"=>$this->getUser()]);

        $contrat = new Contrat();

        $form = $this->createForm(ContratType::class, $contrat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // récupere les fils.
            /**
             * @var Uploaded $file
             */
            $file = $form->get('name')->getData();
            $file->getClientOriginalName();
            $file->getClientOriginalExtension();
            $fileName = md5(uniqid()).'.'.$file->getClientOriginalExtension();
            $file->move($this->getParameter('kernel.project_dir').'/public/uploads/docUser', $fileName);
            $contrat->setName($fileName);

            $entityManager->persist($contrat);
            $entityManager->flush();

            $this->addFlash(
                'success',
                'le contrat est correctement créé.'
            );
        }


        return $this->render('account/contrat/index.html.twig', [
            'form'=>$form->createView(),
            'contrats'=>$contrats,
        ]);
    }
}
