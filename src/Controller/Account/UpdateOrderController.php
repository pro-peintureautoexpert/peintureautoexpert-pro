<?php

namespace App\Controller\Account;

use App\Entity\Order;
use App\Entity\OrderDetail;
use App\Form\AddproductType;
use App\Form\SearchOrderType;
use App\Repository\CategoryRepository;
use App\Repository\OrderDetailRepository;
use App\Repository\OrderRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class UpdateOrderController extends AbstractController
{
    //Sélectinne la commande par la form symfony  
    #[Route('compte/update/order', name: 'app_update_order')]
    public function index(OrderRepository $orderRepository, Request $request): Response
    {
        if(!$this->getUser()){
            return $this->redirectToRoute('app_login');
        }

        $form = $this->createForm(SearchOrderType::class, null, [
            'action' => $this->generateUrl('app_update_order_summry'),

        ]);

        return $this->renderForm('account/update_order/index.html.twig', [
            'form' => $form,
        ]);
    }
    
// Traite la commande sélectionné
    #[Route('compte/update/order/2', 'app_update_order_summry')]
    public function summry(OrderRepository $orderRepository, Request $request): Response
    {
        if ('POST' != $request->getMethod()) {
            return $this->redirectToRoute('compte/update/order');
        }

        $form = $this->createForm(SearchOrderType::class, null);
       
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
             $selectedOrder = $form->get('order')->getData();
             
             if (!$selectedOrder) {
                 $this->addFlash('danger', 'Aucune commande sélectionnée.');
                 return $this->redirectToRoute('app_update_order');
             }
         
                 //affiche la form de produit dans model bootstarp
                 $addProduct = $this->createForm(AddproductType::class , null,[
                    'action' => $this->generateUrl('app_update_order_product', ['id' =>
                    $selectedOrder->getId()]),
                ]);

             try {
                 $products = $selectedOrder->getOrderDetails();
                 
                
                 if (!$products || $products->isEmpty()) {
                     throw new \Exception('Aucun produit trouvé pour cette commande.');
                 }
                
         
             } catch (\Exception $e) {
                 $this->addFlash('danger', 'Produits non trouvés.');
                 return $this->redirectToRoute('app_update_order');
             }
         }

         return $this->render('account/update_order/summary.html.twig', [
             'form' => $form->createView(),
             'addProduct'=>$addProduct->createView(),
             'products'=>$products,
             'order'=>$selectedOrder,
      
          ], new Response(null, $form->isSubmitted() ? 422 : 200));
    }

    //Remove Produit de commande
    #[Route('/compte/update/order/{id}', 'order_remove_product')]
    public function removeProduct(OrderRepository $order, Request $request, $id , OrderDetailRepository $orderDetailRepository, EntityManagerInterface $entityManager): Response
    {
      
       $productremoved = $orderDetailRepository->findOneBy(['id'=>$id]);

       if (!$productremoved) {
        $this->addFlash('danger', 'Produit non trouvé.');
        return $this->redirectToRoute('app_update_order');
        }

        // 3. Récupérer la commande associée au produit
        $order = $productremoved->getMyOrder();

          // 4. Vérifier si la commande existe
            if (!$order) {
                $this->addFlash('danger', 'Commande non trouvée.');
                return $this->redirectToRoute('app_update_order');
            }

        // 1. Supprimer le produit de la commande
        $order->removeOrderDetail($productremoved);

        // 2. Vérifier si la commande contient encore des détails
        if ($order->getOrderDetails()->isEmpty()) {
            // 3. Supprimer la commande si elle n'a plus de détails
            $entityManager->remove($order);
        }

        $entityManager->flush();
        
        return $this->redirect($request->headers->get('referer'));
    }

    #[Route('/compte/update/orderQte/{id}/{qte}', name: 'order_update_qte')]
    public function updateQte(
        Request $request,
        $id,
        $qte,
        OrderDetailRepository $orderDetailRepository,
        EntityManagerInterface $em
    ): Response
    {
        // 1. Récupérer le produit à mettre à jour
        $product = $orderDetailRepository->findOneBy(['id' => $id]);

        // 2. Vérifier si le produit existe
        if (!$product) {
            $this->addFlash('danger', 'Produit non trouvé.');
            return $this->redirectToRoute('app_update_order');
        }

        // 3. Valider la quantité
        if (!is_numeric($qte) || $qte <= 0) {
            $this->addFlash('danger', 'La quantité doit être un nombre positif.');
            return $this->redirect($request->headers->get('referer'));
        }

        // 4. Mettre à jour la quantité du produit
        $product->setProductQuantity((int) $qte);

        // 5. Enregistrer les modifications en base de données
        try {
            $em->flush();
            $this->addFlash('success', 'La quantité du produit a été mise à jour avec succès.');
        } catch (\Exception $e) {
            $this->addFlash('danger', 'Une erreur est survenue lors de la mise à jour de la quantité.');
        }

        // 6. Rediriger l'utilisateur vers la page précédente
        return $this->redirect($request->headers->get('referer'));
    }
    #[Route('/compte/update/orderProduct/{id}', name: 'app_update_order_product', methods: ['POST'])]
    public function updateOrderProduct(
        Request $request,
        $id,
        OrderDetailRepository $orderDetailRepository,
        OrderRepository $orderRepository,
        EntityManagerInterface $em,
        CategoryRepository $categoryRepository
    ): Response {
        // Vérifier que la méthode HTTP est bien POST
        if (!$request->isMethod('POST')) {
            $this->addFlash('danger', 'Méthode non autorisée.');
            return $this->redirectToRoute('compte_update_order');
        }
    
        // Récupérer la commande correspondante à l'ID
        $order = $orderRepository->find($id);
    
        // Vérifier si la commande existe
        if (!$order) {
            $this->addFlash('danger', 'Commande non trouvée.');
            return $this->redirectToRoute('compte_update_order');
        }
    
        // Créer et gérer le formulaire
        $form = $this->createForm(AddproductType::class, null);
        $form->handleRequest($request);
    
        // Vérifier si le formulaire est soumis et valide
        if ($form->isSubmitted() && $form->isValid()) {
            // Récupérer les données du formulaire
            $product = $form->get('product')->getData();
            $qte = $form->get('qte')->getData();
    
            // Vérifier si le produit existe
            if (!$product) {
                $this->addFlash('danger', 'Produit non trouvé.');
                return $this->redirectToRoute('app_update_order');
            }
    
            // Valider la quantité
            if (!is_numeric($qte) || $qte <= 0) {
                $this->addFlash('danger', 'La quantité doit être un nombre positif.');
                return $this->redirectToRoute('app_update_order');
            }
    
            // Créer un nouveau détail de commande
            $orderDetail = new OrderDetail();
            $orderDetail->setProdcutName($product->getName());
            $orderDetail->setProductIllustartion($product->getIllustration());
            $orderDetail->setProductQuantity($qte);
            $orderDetail->setProductPrice($product->getPriceU());
            $orderDetail->setProductTva($product->getTva());
            $orderDetail->setCodeArticle($product->getArticleCode());
            $orderDetail->setSlugProduct($product->getSlug());
    
            // Gérer la catégorie du produit
            $category = $product->getCategory();
            if (null === $category) {
                $this->addFlash('danger', 'La catégorie du produit est introuvable.');
                return $this->redirectToRoute('app_update_order');
            }
    
            // Récupérer la catégorie racine
            $rootCategory = $categoryRepository->findRootCategoryById($category->getId());
            if (null === $rootCategory) {
                $this->addFlash('danger', 'La catégorie racine est introuvable.');
                return $this->redirectToRoute('app_update_order');
            }
    
            $orderDetail->setCategory($rootCategory->getName());
    
            // Ajouter le détail de commande à la commande
            $order->addOrderDetail($orderDetail);
    
            // Persister les changements dans la base de données
            $em->persist($order);
            $em->flush();
    
            $this->addFlash('success', 'Le produit a été bien ajouté.');
        } else {
            $this->addFlash('danger', 'Le formulaire contient des erreurs.');
        }
    
        // Rediriger vers la page précédente
        return $this->redirect($request->headers->get('referer'));
    }

}