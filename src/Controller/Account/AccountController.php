<?php

namespace App\Controller\Account;

use App\Repository\OrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccountController extends AbstractController
{
    #[Route('/compte', name: 'app_account')]
    public function index(OrderRepository $orderRepository): Response
    {
        // Récupération des commandes de l'utilisateur connecté
        $orders = $orderRepository->findBy([
            'user' => $this->getUser(),
            'state' => [2, 3, 4], // États autorisés
            'statePay' => 1, // Commandes payées
        ]);

        // Initialisation des valeurs pour les plafonds
        $total = 0;
        $user = $this->getUser();

        // Vérification que l'utilisateur a bien un magasin avec un plafond défini
        $plafond = $user->getPlafond();
        if (!$user || !$plafond) {
            throw $this->createNotFoundException('Le plafond du magasin n\'est pas défini pour cet utilisateur.');
        }

        // Calcul du total des commandes
        foreach ($orders as $order) {
            $total += $order->getTotalWt();
        }

        // Préparation des données pour le graphique
        $remaining = max(0, $plafond - $total); // Évite les valeurs négatives
        $data = [
            'labels' => ['Commandes non payée', 'Restant'],
            'datasets' => [
                [
                    'data' => [$total, $remaining],
                    'backgroundColor' => ['#fd0202', '#28a745'], // Couleurs personnalisées
                    'hoverBackgroundColor' => ['#d50707', '#1e7e34'], // Couleurs au survol
                ],
            ],
        ];

        // Rendu de la vue
        return $this->render('account/index.html.twig', [
            'orders' => $orders,
            'total' => $total,
            'plafond' => $plafond,
            'data' => $data,
        ]);
    }
}
