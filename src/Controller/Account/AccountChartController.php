<?php

namespace App\Controller\Account;

use App\Repository\OrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

class AccountChartController extends AbstractController
{
    #[Route('/compte/chart', name: 'app_account_chart')]
    public function index(OrderRepository $orderRepository, ChartBuilderInterface $chartBuilder): Response
    {
        $orders = $orderRepository->findBy(['user'=>$this->getUser()]);
        $chiffreAffaire = $orderRepository->getChiffreAffaireMensuelSurSixMois($this->getUser());

        $chart = $chartBuilder->createChart(Chart::TYPE_LINE);
        $chartGrad = $chartBuilder->createChart(Chart::TYPE_BAR); 

        // Initialisation des labels et des données pour le graphique
        $labels = [];
        $data = [];

        foreach ($chiffreAffaire as $result) {
            // Formater le mois en texte et ajouter l'année
            $labels[] = \DateTime::createFromFormat('!m', $result['mois'])->format('F').' '.$result['annee'];
            // Ajouter la valeur de chiffre d'affaire
            $data[] = $result['chiffreAffaire'];
        }

                // Configuration du graphique avec les données récupérées
                $chart->setData([
                    'labels' => $labels,  // Mois et année en labels
                    'datasets' => [
                        [
                            'label' => 'Chiffre d\'affaire HT',
                            'backgroundColor' => 'rgba(255, 99, 132, 0.4)',
                            'borderColor' => 'rgb(255, 99, 132)',
                            'data' => $data,  // Valeurs de chiffre d'affaire
                            'tension' => 0.4,
                        ],
                    ],
                ]);

            // Configuration des options pour ajuster la taille de police
            // $chart->setOptions([
            //     'plugins' => [
            //         'legend' => [
            //             'labels' => [
            //                 'font' => [
            //                     'size' => 18, 
            //                 ],
            //             ],
            //         ],
            //     ],
            //     'scales' => [
            //         'x' => [
            //             'ticks' => [
            //                 'font' => [
            //                     'size' => 12, 
            //                 ],
            //             ],
            //         ],
            //         'y' => [
            //             'ticks' => [
            //                 'font' => [
            //                     'size' => 12, 
            //                 ],
            //             ],
            //         ],
            //     ],
            // ]);


            //Traitement de statistique by categories 
            $nameCategory = [];
            $chiffre = [];
            
            $orderByCategory = $orderRepository->getOrderSurUnAn($this->getUser()); 


            foreach ($orderByCategory as $result) {
           
                $nameCategory[] = $result['categoryName'];
                $chiffre[] = $result['chiffreAffaire'];
            }

             // Configuration du graphique avec les données récupérées
             $chartGrad->setData([
                'labels' => $nameCategory,  // Mois et année en labels
                'datasets' => [
                    [
                        'label' => 'Chiffre d\'affaire HT par catégories',
                        'backgroundColor' => '#154fbc96',
                        'borderColor' => 'rgb(255, 99, 132)',
                        'data' => $chiffre,  // Valeurs de chiffre d'affaire
                        'tension' => 0.4,
                    ],
                ],
            ]);
            
        return $this->render('account/chart/index.html.twig', [
            'orders' => $orders,
            'chart' => $chart,
            'chartGrad' => $chartGrad,
        ]);
    }
}
