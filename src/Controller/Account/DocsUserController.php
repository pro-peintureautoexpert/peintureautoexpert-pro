<?php

namespace App\Controller\Account;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class DocsUserController extends AbstractController
{
    #[Route('/compte/docs', name: 'app_docs_user')]
    public function index(): Response
    {
        return $this->render('account/docs_user/index.html.twig', [
        ]);
    }
}
