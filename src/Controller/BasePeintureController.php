<?php

namespace App\Controller;

use App\Form\PaintType;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BasePeintureController extends AbstractController
{
    #[Route('/base/peinture', name: 'app_base_peinture')]
    public function index(Request $request, ProductRepository $productRepository): Response
    {
        $products = $productRepository->findBy(['inPaint' => true]);
        $item = $productRepository->findBy(['inLabo' => true]);

        $form = $this->createForm(PaintType::class, null);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            if (!$data) {
                return $this->redirectToRoute('app_base_peinture');
            }

            // $this->addFlash(
            //     'success',
            //     'Votre commande de '.$form->get('qte')->getData().' unités de '.$data['name']->getname().' a été envoyée avec succès.'
            // );
            $paints = $data['name'];

            foreach($paints as $paint){
                  $productIds[] = $paint->getId();
            }

            return $this->redirectToRoute('app_cart_addMulti', [
                'productIds' => implode(',', $productIds)
            ]);
      
        }

        return $this->render('base_peinture/index.html.twig', [
            'products' => $products,
            'item' => $item,
        ]);
    }
}
