<?php

namespace App\Controller\Admin;

use App\Entity\Product;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ProductCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Product::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Produit')
            ->setEntityLabelInPlural('Produits')
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        $required = true;

        if ('edit' == $pageName) {
            $required = false;
        }

        return [
            AssociationField::new('productParent', 'Produit associé'),
            TextField::new('articleCode')->setLabel('Référence')->setHelp('La référence de produit'),
            TextField::new('name')->setLabel('Nom')->setHelp('Nom de votre produit'),
            BooleanField::new('isHomepage')->setLabel('Produit à la une ?')->setHelp("Vous permet d'afficher un produit sur la homepage"),
            BooleanField::new('inHome')->setLabel('Produit à la une "palette" ?')->setHelp("Vous permet d'afficher un produit sur la homepage block palette"),
            SlugField::new('slug')->setTargetFieldName('name')->setLabel('URL')->setHelp('URL de votre catégorie générée automatiquement')->onlyOnForms(),
            BooleanField::new('inPaint')->setLabel('Produit à la une "Peinture" ?')->setHelp("Vous permet d'afficher un produit sur la page commende Base peinture"),
            BooleanField::new('inLabo')->setLabel('Produit à la une "Equipements Labo" ?')->setHelp("Vous permet d'afficher un produit sur la page commande peinture"),

            NumberField::new('weight')->setLabel('Poids')->setHelp('le poids net de produit en gramme')->onlyOnForms(),
            NumberField::new('numbreUnitePalette')->setLabel('Qté par palette')->setHelp('Quantité par palette'),
            TextEditorField::new('description')->setLabel('Description')->setHelp('Description de votre produit'),
            ImageField::new('illustration')
                ->setLabel('Image')
                ->setHelp('Image du produit en 600x600px')
                ->setUploadedFileNamePattern('[year]-[month]-[day]-[contenthash].[extension]')
                ->setBasePath('/uploads')
                ->setUploadDir('/public/uploads')
                ->setRequired($required),
            NumberField::new('priceUnitaire')->setLabel('Prix unitaire H.T')->setHelp('Prix H.T du produit sans le sigle €.'),
            NumberField::new('pricePalette')->setLabel('Prix palette H.T')->setHelp('Prix palette H.T du produit sans le sigle €.')->setRequired($required),
            ChoiceField::new('tva')->setLabel('Taux de TVA')->setChoices([
                '5,5%' => '5.5',
                '10%' => '10',
                '20%' => '20',
            ])->onlyOnForms(),
            NumberField::new('stock'),
            AssociationField::new('Category', 'Catégorie associée'),
            AssociationField::new('grain')->onlyOnForms(),
            AssociationField::new('conMastic')->setLabel('Conditionnement Mastic')->onlyOnForms(),
            AssociationField::new('conAppret')->setLabel('Conditionnement Appret')->onlyOnForms(),
            AssociationField::new('conVernis')->setLabel('Conditionnement Vernis')->onlyOnForms(),
            AssociationField::new('rapidite')->setLabel('type de durcisseur')->onlyOnForms(),
            AssociationField::new('malette')->setLabel('Malette')->onlyOnForms(),
            AssociationField::new('couleurAll')->setLabel('Couleur All Categorie')->onlyOnForms(),
            AssociationField::new('filterDimensionCale')->setLabel('Dimension Cale')->onlyOnForms(),
            AssociationField::new('dimensionDoubleFace')->setLabel('Dimension Double Face')->onlyOnForms(),
            AssociationField::new('buse')->setLabel('Buse pistole')->onlyOnForms(),
            AssociationField::new('conAntigravillon')->setLabel('Conditionnement Antigravillon')->onlyOnForms(),
            AssociationField::new('conDegraissant')->setLabel('Conditionnement Dégraissant')->onlyOnForms(),
            AssociationField::new('capaciteAspirateur')->setLabel('Capacité Aspirateur')->onlyOnForms(),
            AssociationField::new('conPolish')->setLabel('Conditionnement Polish')->onlyOnForms(),
            AssociationField::new('conSavon')->setLabel('Conditionnement Savon')->onlyOnForms(),
            AssociationField::new('conDiluant')->setLabel('Conditionnement Diluant')->onlyOnForms(),
            AssociationField::new('conGobelet')->setLabel('Conditionnement Gobelet')->onlyOnForms(),
            AssociationField::new('conBidonPlastique')->setLabel('Conditionnement Bidon Plastique')->onlyOnForms(),
            AssociationField::new('conDurcisseur')->setLabel('Conditionnement Durcisseur')->onlyOnForms(),
            AssociationField::new('dimansionFiltreCabine')->setLabel('Dimension Filtre Cabine')->onlyOnForms(),
            AssociationField::new('diametreZikzak')->setLabel('Diamètre Zikzak')->onlyOnForms(),
            AssociationField::new('diametreAgrafes')->setLabel('Diamètre Agrafes')->onlyOnForms(),
            AssociationField::new('dimensionChariotKraft')->setLabel('Dimension Chariot Kraft')->onlyOnForms(),
            AssociationField::new('dimensionPapierCale')->setLabel('Dimension Papier Cale')->onlyOnForms(),
            AssociationField::new('filtreCone')->setLabel('Filtre Cone')->onlyOnForms(),
            AssociationField::new('dimensionMicroBruch')->setLabel('Dimension Micro Bruch')->onlyOnForms(),
            AssociationField::new('pinceauDimension')->setLabel('Dimension Pinceau')->onlyOnForms(),
            AssociationField::new('regleDosage')->setLabel('Règle de Dosage')->onlyOnForms(),
            AssociationField::new('dimensionKraft')->setLabel('Dimension Kraft')->onlyOnForms(),
            AssociationField::new('dimensionTireau')->setLabel('Dimension Tireau')->onlyOnForms(),
            AssociationField::new('tailleFineTape')->setLabel('Taille Fine Tape')->onlyOnForms(),
            AssociationField::new('effet')->setLabel('Effet')->onlyOnForms(),
            AssociationField::new('aspirateur')->setLabel('Aspirateur')->onlyOnForms(),
            AssociationField::new('ponceuse')->setLabel('Ponceuse')->onlyOnForms(),
            AssociationField::new('typePistole')->setLabel('Type Pistole')->onlyOnForms(),
            AssociationField::new('solPlafond')->setLabel('Sol Plafond')->onlyOnForms(),
            AssociationField::new('acierAlu')->setLabel('Acier Alu')->onlyOnForms(),
            AssociationField::new('dimensionAll')->setLabel('Dimension All')->onlyOnForms(),
            AssociationField::new('taille')->setLabel('Taille')->onlyOnForms(),
            AssociationField::new('matiere')->setLabel('Matière')->onlyOnForms(),
            AssociationField::new('masquage')->setLabel('Masquage')->onlyOnForms(),
            AssociationField::new('nettoyage')->setLabel('Nettoyage')->onlyOnForms(),
            AssociationField::new('durcisseur')->setLabel('Durcisseur')->onlyOnForms(),
            AssociationField::new('accessoireAspirateur')->setLabel('Accessoire Aspirateur')->onlyOnForms(),
            AssociationField::new('vitrage')->setLabel('Vitrage')->onlyOnForms(),
            AssociationField::new('typeRaccord')->setLabel('Type de Raccord')->onlyOnForms(),
            AssociationField::new('typeMateriel')->setLabel('Type de Matériel')->onlyOnForms(),
            AssociationField::new('materiel')->setLabel('Matériel')->onlyOnForms(),
            AssociationField::new('colleJoint')->setLabel('Colle Joint')->onlyOnForms(),
            AssociationField::new('adhesif')->setLabel('Adhésif')->onlyOnForms(),
            AssociationField::new('typePonceuse')->setLabel('Type de Ponceuse')->onlyOnForms(),
            AssociationField::new('typeMastic')->setLabel('Type de Mastic')->onlyOnForms(),
            AssociationField::new('typeDiluant')->setLabel('Type de Diluant')->onlyOnForms(),
            AssociationField::new('accessoire')->setLabel('Accessoire')->onlyOnForms(),
            AssociationField::new('accessoirePolisseuse')->setLabel('Accessoire Polisseuse')->onlyOnForms(),
            AssociationField::new('debosseleurMarteau')->setLabel('Débosseleur Marteau')->onlyOnForms(),
            AssociationField::new('equerreTirageGriffe')->setLabel('Équerre de Tirage Griffe')->onlyOnForms(),
            AssociationField::new('chaineRaccord')->setLabel('Chaîne de Raccord')->onlyOnForms(),
            AssociationField::new('barreDebosslage')->setLabel('Barre de Débosselage')->onlyOnForms(),
            AssociationField::new('masseMagnetiqueChario')->setLabel('Masse Magnétique Chariot')->onlyOnForms(),
        ];
    }
}
