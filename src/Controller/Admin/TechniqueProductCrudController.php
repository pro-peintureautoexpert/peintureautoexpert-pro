<?php

namespace App\Controller\Admin;

use App\Entity\TechniqueProduct;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;

class TechniqueProductCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return TechniqueProduct::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Caractéristiques technique')
            ->setEntityLabelInPlural('Caractéristiques technique')
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        $required = true;

        if ('edit' == $pageName) {
            $required = false;
        }

        return [
            TextEditorField::new('descriptionTech')->setLabel('Description technique')->setHelp('Description technique de votre produit'),
            // ImageField::new('iconOne')
            // ->setLabel('Icon ')
            // ->setHelp('Image du produit en 20x20px')
            // ->setUploadedFileNamePattern('[year]-[month]-[day]-[contenthash].[extension]')
            // ->setBasePath('/uploads')
            // ->setUploadDir('/public/uploads')
            // ->setRequired($required),
            // ImageField::new('iconTwo')
            // ->setLabel('Icon ')
            // ->setHelp('Image du produit en 20x20px')
            // ->setUploadedFileNamePattern('[year]-[month]-[day]-[contenthash].[extension]')
            // ->setBasePath('/uploads')
            // ->setUploadDir('/public/uploads')
            // ->setRequired($required),
            UrlField::new('ficheTechnique')->setLabel('Fiche Technique *(lien externe)'),
            UrlField::new('ficheSecurite')->setLabel('Fiche Sécurite *(lien externe)'),
            AssociationField::new('product')->setLabel('product'),
            AssociationField::new('image')->setLabel('Icons'),
        ];
    }
}
