<?php

namespace App\Controller\Admin;

use App\Entity\Order;
use App\Entity\OrderDetail;
use App\Entity\TemporaryOrder;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Request;

class TemporaryOrderCrudController extends AbstractCrudController
{
    private $em;

    public function __construct(EntityManagerInterface $entityManagerInterface)
    {
        $this->em = $entityManagerInterface;
    }

    public static function getEntityFqcn(): string
    {
        return TemporaryOrder::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        $show = Action::new('Accepter la commande ?')->linkToCrudAction('show');

        return $actions
        ->remove(Crud::PAGE_INDEX, Action::NEW)
        ->remove(Crud::PAGE_INDEX, Action::DELETE)
        ->add(Crud::PAGE_INDEX, $show);
    }

    /*
     * Fonction permettant le changement de statut de commande
     */
    public function valider($temporaryOrder)
    {
        // 1 Transfere les données au order table aprés la vérfication
        $order = new Order();
        $order->setUser($temporaryOrder->getUser());
        $order->setCreatedAt($temporaryOrder->getDate());
        $order->setState(2);
        $order->setStatePay(1);
        $order->setCarrierName($temporaryOrder->getCarrierName());
        $order->setCarrierPrice($temporaryOrder->getCarrierPrice());
        $order->setDelivrey('$address');
        $order->setPicker('Hammad SHAFIQ');

        $orderDetail = new OrderDetail();
        $orderDetail->setProdcutName($temporaryOrder->getName());
        $orderDetail->setProductIllustartion($temporaryOrder->getIllustration());
        $orderDetail->setProductPrice('52');
        $orderDetail->setProductQuantity($temporaryOrder->getQte());
        $orderDetail->setProductTva('26');
        $orderDetail->setCodeArticle($temporaryOrder->getCodeArticle());
        $orderDetail->setCategory('autre');

        $order->addOrderDetail($orderDetail);

        $this->em->persist($order);
        $this->em->flush();

        // 2 Affichage du Flash Message pour informer l'administrateur
        $this->addFlash('success', 'La commande a été acceptée avec succès.');

        // 3 Informer l'utilisateur par mail de la modification du statut de sa commande
        // $mail = new Mail();
        // $vars = [
        //     'firstname' => $order->getUser()->getFirstname(),
        //     'id_order' => $order->getId()
        // ];
        // $mail->send($order->getUser()->getEmail(), $order->getUser()->getFirstname().' '.$order->getUser()->getLastname(), State::STATE[$state]['email_subject'], State::STATE[$state]['email_template'], $vars);
    }

    public function show(AdminContext $context, AdminUrlGenerator $adminUrlGenerator, Request $request)
    {
        $temporaryOrder = $context->getEntity()->getInstance();

        // Récupérer l'URL de notre action "SHOW"
        $url = $adminUrlGenerator->setController(self::class)->setAction('show')->setEntityId($temporaryOrder->getId())->generateUrl();

        // Traitement des changements de statut
        $this->valider($temporaryOrder);
        $this->em->remove($temporaryOrder);
        $this->em->flush();

        return $this->render('admin/TemporaryOrder.html.twig', [
            'order' => $temporaryOrder,
            'current_url' => $url,
        ]);
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('vérifiéer la commande')
            ->setEntityLabelInPlural('Commande par Catalogue')
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        $required = true;

        if ('edit' == $pageName) {
            $required = false;
        }

        return [
            AssociationField::new('user')->setLabel('Nom du magasin'),
            DateField::new('date')->setLabel('Date'),
            TextField::new('codeArticle')->setLabel('Référence')->setHelp('La référence de produit'),
            TextField::new('name')->setLabel('Nom')->setHelp('Nom de votre produit'),
            ImageField::new('illustration')
            ->setLabel('Image')
            ->setHelp('Image du produit en 600x600px')
            ->setUploadedFileNamePattern('[year]-[month]-[day]-[contenthash].[extension]')
            ->setBasePath('/uploads')
            ->setUploadDir('/public/uploads')
            ->setRequired($required),
            NumberField::new('qte')->setLabel('Quantité'),
        ];
    }
}
