<?php

namespace App\Controller\Admin;

use App\Entity\ProductParent;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ProductParentCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ProductParent::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Produit')
            ->setEntityLabelInPlural('Produits')
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        $required = true;

        if ('edit' == $pageName) {
            $required = false;
        }

        return [
            TextField::new('name')->setLabel('Nom')->setHelp('Nom de votre produit'),
            SlugField::new('slug')->setTargetFieldName('name')->setLabel('URL')->setHelp('URL de votre produit générée automatiquement'),
            TextEditorField::new(propertyName: 'description')->setLabel('Description')->setHelp('Description de votre produit'),
            TextField::new(propertyName: 'about')->setLabel('Filter')->setHelp('les filters internes'),
            AssociationField::new('category', 'Catégorie associée'),
            AssociationField::new('brand', 'marque associée'),
        ];
    }
}
