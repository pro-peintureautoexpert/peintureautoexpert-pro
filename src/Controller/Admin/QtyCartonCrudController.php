<?php

namespace App\Controller\Admin;

use App\Entity\QtyCarton;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;


use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class QtyCartonCrudController extends AbstractCrudController
{
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Quantité')
            ->setEntityLabelInPlural('Quantités')
        ;
    }
    public static function getEntityFqcn(): string
    {
        return QtyCarton::class;
    }
    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('object', 'produit associée'),
            IntegerField::new('qty'),
            NumberField::new('price')->setLabel('Prix unitaire par quantité H.T')->setHelp('Prix H.T du produit sans le sigle €.'),
            BooleanField::new('active')->setLabel("Produit à la une ?"),
            BooleanField::new('inPalette')->setLabel("Produit à la une ? (produit palette)"),
            IntegerField::new('orderQte')->setLabel("Order de inQte"),
            IntegerField::new('orderPalette')->setLabel("Order de inPalette"),
        ];
    }
}
