<?php

namespace App\Controller\Admin;

use App\Entity\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class FiltersCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Filters::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
        ];
    }
}
