<?php

namespace App\Controller\Admin;

use App\Controller\Admin\Trait\DetailTrait;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Request;

class UserCrudController extends AbstractCrudController
{
    // pour rendre  l'entité user non-modefiable
    use DetailTrait;

    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Magasin')
            ->setEntityLabelInPlural('Magasins')
        ;
    }

    // Personalise la page de admin User
    public function show(AdminContext $context, AdminUrlGenerator $adminUrlGenerator, Request $request)
    {
        $user = $context->getEntity()->getInstance();

        // Récupérer l'URL de notre action "SHOW"
        $url = $adminUrlGenerator->setController(self::class)->setAction('show')->setEntityId($user->getId())->generateUrl();

        return $this->render('admin/user.html.twig', [
            'user' => $user,
            'current_url' => $url,
        ]);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('uuid', 'Identifiant Magasin')->onlyOnDetail(),
            TextField::new('password', 'Mot de passe (Première connexion)')->onlyOnDetail(),
            EmailField::new('email', 'Email'),
            TextField::new('magasinName', 'Nom du magasin'),
            TextField::new('firstName', 'Prénom'),
            TextField::new('lastName', 'Nom'),
            IntegerField::new('plafond')->setLabel('Plafond du magasin ')
                ];
    }
}
