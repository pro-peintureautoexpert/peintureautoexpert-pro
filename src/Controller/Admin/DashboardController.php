<?php

namespace App\Controller\Admin;

use App\Entity\Brand;
use App\Entity\Carrier;
use App\Entity\Catalogue;
use App\Entity\Category;
use App\Entity\CodeBarre;
use App\Entity\Contrat;
use App\Entity\Filters;
use App\Entity\Header;
use App\Entity\Image;
use App\Entity\Order;
use App\Entity\Product;
use App\Entity\ProductParent;
use App\Entity\Promotions;
use App\Entity\QtyCarton;
use App\Entity\TechniqueProduct;
use App\Entity\TemporaryOrder;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);

        return $this->redirect($adminUrlGenerator->setController(UserCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Peinture-auto-expert Pro');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToCrud('Magasin', 'fa-solid fa-person-chalkboard', User::class);
        yield MenuItem::linkToCrud('Contrat', 'fa-solid fa-file-contract', Contrat::class);
        yield MenuItem::linkToCrud('Produit Parent', 'fa fa-feather-pointed', ProductParent::class);
        yield MenuItem::linkToCrud('Produit', 'fa-brands fa-product-hunt', Product::class);
        yield MenuItem::linkToCrud('Achat par quantité', 'fa-solid fa-box', QtyCarton::class);
        yield MenuItem::linkToCrud('Code Barre', 'fa fa-barcode', CodeBarre::class);
        yield MenuItem::linkToCrud('Categories', 'fa fa-list', Category::class);
        yield MenuItem::linkToCrud('Illustartion', 'fa fa-picture-o', Image::class);
        yield MenuItem::linkToCrud('Promotions', 'fa-solid fa-percent', Promotions::class);
        yield MenuItem::linkToCrud('Marques', 'fa-regular fa-copyright', Brand::class);
        yield MenuItem::linkToCrud('Transporteurs', 'fa fa-truck', Carrier::class);
        yield MenuItem::linkToCrud('Commande', 'fab fa-amazon-pay', Order::class);
        yield MenuItem::linkToCrud('Technique-Produit', 'fa-solid fa-wrench', TechniqueProduct::class);
        yield MenuItem::linkToCrud('Commande par Catalogue', 'fa-solid fa-hourglass-start', TemporaryOrder::class);
        yield MenuItem::linkToCrud('Filters', 'fa fa-filter', Filters::class);
        yield MenuItem::linkToCrud('Headers', 'fa fa-desktop', Header::class);
        yield MenuItem::linkToCrud('Catalogues', 'fa fa-certificate', Catalogue::class);
    }
}
