<?php

namespace App\Controller\Admin;

use App\Entity\Brand;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;

class BrandCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Brand::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Marque')
            ->setEntityLabelInPlural('Nos marques')
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        $required = true;

        if ('edit' == $pageName) {
            $required = false;
        }

        return [
            TextField::new('name'),
            ImageField::new('illustration')
                ->setLabel('Image')
                ->setHelp('Image du marque en 600x600px')
                ->setUploadedFileNamePattern('[year]-[month]-[day]-[contenthash].[extension]')
                ->setBasePath('/uploads/brands')
                ->setUploadDir('/public/uploads/brands')
                ->setRequired($required),
            UrlField::new('cataloguePdf')->setLabel('Catalogue de marque *(lien externe)'),

            // TextEditorField::new('description'),
        ];
    }
}
