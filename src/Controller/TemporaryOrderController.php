<?php

namespace App\Controller;

use App\Entity\TemporaryOrder;
use App\Form\TemporaryOrderFormType;
use App\Repository\BrandRepository;
use App\Repository\CarrierRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Classe\Mail;


class TemporaryOrderController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('compte/demande-produit', name: 'app_temporary_order')]
    public function index(BrandRepository $brandReposiitory, CarrierRepository $carrierRepository, Request $request): Response
    {
        $brands = $brandReposiitory->findAll();
        $carriers = $carrierRepository->findAll();
        $temporaryOrder = new TemporaryOrder();
        $form = $this->createForm(TemporaryOrderFormType::class, null, [
            'brands' => $brands,
            'carriers' => $carriers,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $temporaryOrder->setUser($this->getUser());
            $temporaryOrder->setCodeArticle($data->getCodeArticle());
            $temporaryOrder->setQte($data->getQte());
            $temporaryOrder->setBrands($data->getBrands());
            $temporaryOrder->setName($data->getName());
            $temporaryOrder->setIllustration('Vide');
            $temporaryOrder->setDate(new \DateTime());
            $temporaryOrder->setCarrierName($data->getCarrierName());
            $temporaryOrder->setCarrierPrice($form->get('carrierName')->getData()->getPrice());
            $this->entityManager->persist($temporaryOrder);
            $this->entityManager->flush();
            $this->addFlash(
                'success',
                'Votre demande a été envoyée avec succès.'
            );

            return $this->redirectToRoute('app_temporary_order');

            if ($this->isGranted('ROLE_USER')) {
                $mail = new Mail();
                $subject = "Nouvelle Commande par catalogue" ;
                $vars = [
                    'Numero_de_commande' => $temporaryOrder->getId(),
                    'nameComplet' => $this->getUser()->getMagasinName(),
                    ];
            $mail->notife($this->getUser()->getFirstName().' '.$this->getUser()->getLastName(), $subject, $vars);
            }
        }

        return $this->render('temporary_order/index.html.twig', [
            'orderBycatalogue' => $form,
        ]);
    }
}
