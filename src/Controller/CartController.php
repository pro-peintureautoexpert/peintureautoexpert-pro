<?php

namespace App\Controller;

use App\Classe\CartData;
use App\Repository\CodeBarreRepository;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    #[Route('/mon-panier/{motif}', name: 'app_cart', defaults: ['motif' => null])]
    public function index(CartData $cart, $motif, OrderRepository $orderRepository): Response
    {
        if ('annulation' === $motif) {
            $this->addFlash(
                'info',
                'Paiement annulé : Vous pouvez mettre à jour votre panier et votre commande.'
            );
        }
        
        return $this->render('cart/index.html.twig', [
            'cart' => $cart->afficher(),
            'cartData'=>$cart,
            'totalWt' => $cart->calculateTotalPrice(),
        ]);
    }

    #[Route('/cart/add/{id}', name: 'app_cart_add')]
    public function add($id, CartData $cartData, ProductRepository $productRepository, Request $request): Response
    {
        $product = $productRepository->findOneBy(['id' => $id]);

        if (!$product) {
            $this->addFlash('danger', 'Produit introuvable.');
            return $this->redirectToRoute('app_cart');
        }
        

        $cartData->ajouter($product);

        $this->addFlash(
            'success',
            'Produit correctement ajouté à votre panier.'
        );

         return $this->redirect($request->headers->get('referer'));
        // return $this->redirectToRoute('app_cart');
     }



     #[Route('/cart/addMulti/{productIds}', name: 'app_cart_addMulti')]
     public function addM($productIds, CartData $cartData, ProductRepository $productRepository, Request $request): Response
     {
         $ids = explode(',', $productIds); // Transformer la chaîne en tableau
 
         $products = $productRepository->findBy(['id' => $ids]);
         
         if (!$products) {
             $this->addFlash('danger', 'Produit introuvable.');
             return $this->redirectToRoute('app_cart');
         }
         
         $cartData->addMulti($products);
 
         $this->addFlash(
             'success',
             'Produits correctement ajoutés à votre panier.'
         );
 
          return $this->redirect($request->headers->get('referer'));
        //  return $this->redirectToRoute('app_cart');
      }
 

    #[Route('/cart/add/{id}/{qte}', name: 'app_cart_addQte')]
    public function addQte($id, $qte, CartData $cartData, ProductRepository $productRepository, Request $request): Response
    {
        $product = $productRepository->findOneBy(['id' => $id]);
        // $cart->addQte($product, $qte);

        $cartData->addQuntite($product, $qte);

        // $this->addFlash(
        //     'success',
        //     'Produit correctement ajouté à votre panier.'
        // );

        return $this->redirectToRoute('app_cart');
    }

    #[Route('/cart/decrease/{id}', name: 'app_cart_decrease')]
    public function decrease($id, CartData $cartData, ProductRepository $productRepository): Response
    {
        $product = $productRepository->findOneBy(['id' => $id]);

        if (!$product) {
            $this->addFlash('danger', 'Produit introuvable.');
            return $this->redirectToRoute('app_cart');
        }
        

        $cartData->supprimer($product);

        $this->addFlash(
            'success',
            'Produit correctement supprimée de votre panier.'
        );

        return $this->redirectToRoute('app_cart');
    }

    #[Route('/cart/vider', name: 'app_cart_vider')]
    public function remove(CartData $cart): Response
    {

        $cart->vider();
        return $this->redirectToRoute('app_home');
        
    }

    #[Route('/cart/addCode/{code}', name: 'app_cart_add_code')]
    public function addCode($code, CartData $cartData, CodeBarreRepository $codeBarreRepository, Request $request): Response
    {
        $codeBarre = $codeBarreRepository->findOneBy(['code' => $code]);



        if (!$codeBarre->getProduit()) {
            $this->addFlash('danger', 'Produit introuvable.');
            return $this->redirectToRoute('app_cart');
        }
        
        $cartData->ajouter($codeBarre->getProduit());

        // $this->addFlash(
        //     'success',
        //     'Produit correctement ajouté à votre panier.'
        // );

        // return $this->redirect($request->headers->get('referer'));
        return $this->redirectToRoute('app_cart');
     }
}
