<?php

namespace App\Controller;

use App\Classe\CartData;
use App\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class PayLinkController extends AbstractController
{
    #[Route('/paylink/{orderId}', name: 'app_pay_link')]
    public function index($orderId, OrderRepository $orderRepository, CartData $cart, EntityManagerInterface $entityManager): Response
    {

        $order = $orderRepository->findOneBy([
            'user' => $this->getUser(),
            'id' => $orderId,
        ]);

        if (!$order) {
            return $this->redirectToRoute('app_home');
        }

        if (1 == $order->getState()) {
            $order->setState(2);
            $cart->vider();
            $entityManager->flush();
        }

        if (1 == $order->getStatePay()) {
            $order->setStatePay(4);
            $entityManager->flush();
        }
        
        return $this->render('pay_link/index.html.twig', [
            'order' => $order,
        ]);
    }
}
