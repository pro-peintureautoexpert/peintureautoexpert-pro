<?php

namespace App\Controller;

use App\Repository\BrandRepository;
use App\Repository\ProductParentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    #[Route('/search', name: 'app_search_ajax', methods: ['GET'])]
    public function index(ProductParentRepository $productParentRepository, BrandRepository $brandRepository, Request $request): JsonResponse
    {
        $string = $request->query->get('query', ''); 
        $result = $productParentRepository->findByString($string);
        
        $results = [];
        foreach ($result as $product) {
            $results[] = [
                'slug' => $product->getSlug(), // Inclure l'ID du produit
                'name' => $product->getName(),
            ];
        }

        return $this->json($results);
    }


    #[Route('/compte/recherche', name: 'app_search', methods: ['GET'])]
    public function view(ProductParentRepository $productParentRepository, BrandRepository $brandRepository, Request $request): Response
    {
        $string = $request->query->get('query',''); // Retrieve the query parameter 'q'
        $result = $productParentRepository->findByString($string);

        return $this->render('account/search/index.html.twig', [
            'result' => $result,
        ]);
    }

}
