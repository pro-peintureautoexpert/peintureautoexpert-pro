<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Classe\CartData;
use App\Classe\Mail;
use App\Classe\TokenService;
use App\Entity\TagsColissimo;
use App\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Stripe\Checkout\Session;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PaymentController extends AbstractController
{
    private $tokenService;
    private $entityManager;

    // Injecter l'EntityManager via le constructeur
    public function __construct(EntityManagerInterface $entityManager, TokenService $tokenService)
    {
        $this->entityManager = $entityManager;
        $this->tokenService = $tokenService;
    }

    #[Route('/commande/paiement/{id_order}', name: 'app_payment')]
    public function index($id_order, OrderRepository $orderRepository, EntityManagerInterface $entityManager): Response
    {
        Stripe::setApiKey($_ENV['STRIPE_SECRET_KEY']);

        $order = $orderRepository->findOneBy([
            'id' => $id_order,
            'user' => $this->getUser(),
        ]);

        if (!$order) {
            return $this->redirectToRoute('app_home');
        }

        $products_for_stripe = [];

        foreach ($order->getOrderDetails() as $product) {
            $products_for_stripe[] = [
                'price_data' => [
                    'currency' => 'eur',
                    'unit_amount' => number_format($product->getProductPriceWt() * 100, 0, '', ''),
                    'product_data' => [
                        'name' => $product->getProdcutName(),
                        'images' => [
                            $_ENV['DOMAIN'].'/uploads/'.$product->getProductIllustartion(),
                        ],
                    ],
                ],
                'quantity' => $product->getProductQuantity(),
            ];
        }

        $products_for_stripe[] = [
            'price_data' => [
                'currency' => 'eur',
                'unit_amount' => number_format($order->getCarrierPrice() * 100 * 1.2, 0, '', ''),
                'product_data' => [
                    'name' => 'Transporteur : '.$order->getCarrierName(),
                ],
            ],
            'quantity' => 1,
        ];

        $checkout_session = Session::create([
            'customer_email' => $this->getUser()->getEmail(),
            'line_items' => [[
                $products_for_stripe,
            ]],
            'mode' => 'payment',
            'success_url' => $_ENV['DOMAIN'].'/commande/merci/{CHECKOUT_SESSION_ID}',
            'cancel_url' => $_ENV['DOMAIN'].'/mon-panier/annulation',
        ]);

        $order->setStripeSessionId($checkout_session->id);
        $entityManager->flush();

        return $this->redirect($checkout_session->url);
    }

    #[Route('/commande/merci/{stripe_session_id}', name: 'app_payment_success')]
    public function success($stripe_session_id, OrderRepository $orderRepository, EntityManagerInterface $entityManager, CartData $cart): Response
    {
        $order = $orderRepository->findOneBy([
            'stripe_session_id' => $stripe_session_id,
            'user' => $this->getUser(),
        ]);

        if (!$order) {
            return $this->redirectToRoute('app_home');
        }

        if (1 == $order->getStatePay()) {
            $order->setStatePay(2);
            $order->setState(2);
            $cart->vider();
            $entityManager->flush();
        }
        // Traitement pour générer l'étiquette de livraison Colissimo
  
        if ('Colissimo Domicile - avec signature' === $order->getCarrierName()) {
            // 1- recupere l'address de livraison
            $addressItem = $order->getAddressItem();
            // 2- appelle la fonction generatLabel
            $result = $this->tokenService->generatelabel($addressItem->getCompanyName(), $addressItem->getCity(), $addressItem->getCountryCode(), $addressItem->getEmail(), $addressItem->getLastName(), $addressItem->getFirstName(), $addressItem->getLine0(), $addressItem->getLine1(), $addressItem->getPhoneNumber(), $addressItem->getZipCode(), 5, 'DOS');
            $returne = $this->tokenService->generatereturn($addressItem->getCompanyName(), $addressItem->getCity(), $addressItem->getCountryCode(), $addressItem->getLastName(), $addressItem->getFirstName(), $addressItem->getLine0(), $addressItem->getLine1(), $addressItem->getZipCode(), 5);
            // 3- sauvegarde le label dans la base de données
            $colissimo = new TagsColissimo();
            $colissimo->setName($result);
            $colissimo->setRetour($returne);
            $colissimo->addTicket($order);
            $entityManager->persist($colissimo);
            $entityManager->flush();
        }


        //envoyer une notification de commande au admin :
            
            // if ($this->isGranted('ROLE_USER')) {
            // $mail = new Mail();

            // $subject = "Nouvelle Commande N° ".$order->getId();
            // $vars = [
            // 'Numero_de_commande' => $order->getId(),
            // 'date' => $order->getCreatedAt(),
            // 'statutPay'=>$order->getStatePay(),
            // 'statutOrder'=> $order->getState(),
            // 'nameComplet' => $this->getUser()->getMagasinName(),
            // 'emailClient' =>$this->getUser()->getEmail(),
            // 'phone' => $addressItem->getPhoneNumber(),
            // 'line0'=> $addressItem->getLine0(),
            // 'postal'=>$addressItem->getZipCode(),
            // 'city' => $addressItem->getCity(),
            // 'pays' =>$addressItem->getCountryCode(),
            // 'stripe_id'=>$order->getStripeSessionId(),
            // 'idOrder'=> $order->getId(),
            // ];
            // $mail->notification($this->getUser()->getFirstName().' '.$this->getUser()->getLastName(), $subject, $vars);
            // }

            if ($this->isGranted('ROLE_USER')) {
                $mail = new Mail();
                $subject = "Nouvelle Commande N° ".$order->getId();
                $vars = [
                    'Numero_de_commande' => "Nouvelle Commande N° ".$order->getId()."du Total ".$order->getTotalWt()."€ TTC",
                    'nameComplet' => $this->getUser()->getMagasinName(),
                    ];
            $mail->notife($this->getUser()->getFirstName().' '.$this->getUser()->getLastName(), $subject, $vars);
            }

        return $this->render('payment/success.html.twig', [
            'order' => $order,
            'etiquettereturn' => $returne,
        ]);
    }
}
