<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Classe\CartData;
use App\Form\ProductFiltreType;
use App\Repository\CategoryRepository;
use App\Repository\FiltersRepository;
use App\Repository\ProductParentRepository;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    #[Route('/Categorie/{slug}', name: 'app_category')]
    public function index($slug, CategoryRepository $categoryRepositor, CartData $cart, ProductRepository $productRepository, Request $request, FiltersRepository $filtersRepository, ProductParentRepository $productParentRepository): Response
    {
        $products = $cart->afficher();
        $categoryParent = $categoryRepositor->findOneBy(['slug' => $slug]);

        if (!$categoryParent) {
          
            return $this->redirectToRoute('app_home');
        }


        $categoryGrandParent = $categoryRepositor->findOneBy(['id' => $categoryParent->getParentId()]);
        $categoryFils = $categoryRepositor->findBy(['parentId' => $categoryParent->getId()]);
        // Form de filter
        $form = $this->createForm(ProductFiltreType::class, null, [
            'category' => $categoryParent->getId(),
            'filters' => $categoryParent->getFilters(),
            'current_category' => $categoryParent,

        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $filters = $form->getData();
            $productsFilter = $productParentRepository->findByFilters($categoryParent, $filters);
        } else {
            $productsFilter = $productParentRepository->findBy(['category' => $categoryParent]);
        }

        return $this->render('category/index.html.twig', [
            'Fils' => $categoryFils,
            'Parent' => $categoryParent,
            'GrandParent' => $categoryGrandParent,
            'products' => $products,
            'productfilter' => $productsFilter,
            'form' => $form->createView(),
        ], new Response(null, $form->isSubmitted() ? 422 : 200));
    }
}
