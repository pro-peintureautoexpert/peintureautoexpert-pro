<?php

namespace App\Controller;

use App\Classe\Mail;
use App\Entity\User;
use App\Form\RegisterUserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class RegisterController extends AbstractController
{
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    #[Route('/inscription', name: 'app_register')]
    public function index(Request $request, EntityManagerInterface $entityManager): Response
    {
        $user = new User();

        $form = $this->createForm(RegisterUserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // récupere les fils.

            /**
             * @var Uploaded $file
             */
            $file = $form->get('kbis')->getData();
            $file->getClientOriginalName();
            $file->getClientOriginalExtension();
            $fileName = md5(uniqid()).'.'.$file->getClientOriginalExtension();
            $file->move($this->getParameter('kernel.project_dir').'/public/uploads/docUser', $fileName);
            $user->setKbis($fileName);

            /**
             * @var Uploaded $file2
             */
            $file2 = $form->get('identite')->getData();
            $file2->getClientOriginalName();
            $file2->getClientOriginalExtension();
            $fileName = md5(uniqid()).'.'.$file2->getClientOriginalExtension();
            $file2->move($this->getParameter('kernel.project_dir').'/public/uploads/docUser', $fileName);
            $user->setIdentite($fileName);

            // Enregistrement de mot de passe non crypté
            $plainPassword = $user->getPassword();
            // Enregistrement de l'utilisateur
            $user->setRoles(['ROLE_USER']);
            $user->setPassword($this->passwordHasher->hashPassword($user, $user->getPassword()));
            $entityManager->persist($user);
            $entityManager->flush();

            // Envoie d'un email de confirmation d'inscription
            $mail = new Mail();
            $vars = [
                'Uuid' => $user->getUuid(),
                'Password' => $plainPassword,
            ];
            $mail->send($user->getEmail(), $user->getFirstName().' '.$user->getLastName(), 'Détails de Connexion à PeintureAutoExpert-Pro', $vars);

            $this->addFlash(
                'success',
                'le magasin est correctement créé.'
            );

            return $this->redirectToRoute('app_register');
        }

        return $this->render('register/index.html.twig', [
            'registerForm' => $form->createView(),
        ]);
    }
}
