<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Classe\CartData;
use App\Classe\Mail;
use App\Classe\TokenService;
use App\Entity\TagsColissimo;
use App\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ConfirmationController extends AbstractController
{
    private $tokenService;
    private $entityManager;

    // Injecter l'EntityManager via le constructeur
    public function __construct(EntityManagerInterface $entityManager, TokenService $tokenService)
    {
        $this->entityManager = $entityManager;
        $this->tokenService = $tokenService;
    }

    #[Route('commande/merci/confirmation/{orderId}', name: 'app_confirmation')]
    public function success(OrderRepository $orderRepository, EntityManagerInterface $entityManager, CartData $cart, $orderId): Response
    {
        $order = $orderRepository->findOneBy([
            'user' => $this->getUser(),
            'id' => $orderId,
        ]);

        if (!$order) {
            return $this->redirectToRoute('app_home');
        }

        if (1 == $order->getState()) {
            $order->setState(2);
            $cart->vider();
            $entityManager->flush();
        }

        // Traitement pour générer l'étiquette de livraison Colissimo
        $returne = null;

        if ('Colissimo Domicile - avec signature' === $order->getCarrierName()) {
            // 1- recupere l'address de livraison
            $addressItem = $order->getAddressItem();
            // 2- appelle la fonction generatLabel
            $result = $this->tokenService->generatelabel($addressItem->getCompanyName(), $addressItem->getCity(), $addressItem->getCountryCode(), $addressItem->getEmail(), $addressItem->getLastName(), $addressItem->getFirstName(), $addressItem->getLine0(), $addressItem->getLine1(), $addressItem->getPhoneNumber(), $addressItem->getZipCode(), 5, 'DOS');
            $returne = $this->tokenService->generatereturn($addressItem->getCompanyName(), $addressItem->getCity(), $addressItem->getCountryCode(), $addressItem->getLastName(), $addressItem->getFirstName(), $addressItem->getLine0(), $addressItem->getLine1(), $addressItem->getZipCode(), 5);
            // 3- sauvegarde le label dans la base de données
            $colissimo = new TagsColissimo();
            $colissimo->setName($result);
            $colissimo->setRetour($returne);
            $colissimo->addTicket($order);
            $entityManager->persist($colissimo);
            $entityManager->flush();
        }

        if ($this->isGranted('ROLE_USER')) {
            $mail = new Mail();
            $subject = "Nouvelle Commande N° ".$order->getId()."du Total ".$order->getTotalWt()."€ TTC";
            $vars = [
                'Numero_de_commande' => $order->getId(),
                'nameComplet' => $this->getUser()->getMagasinName(),
                ];
        $mail->notife($this->getUser()->getFirstName().' '.$this->getUser()->getLastName(), $subject, $vars);
        }

        return $this->render('confirmation/success.html.twig', [
            'order' => $order,
            'etiquettereturn' => $returne,
        ]);
    }
}
