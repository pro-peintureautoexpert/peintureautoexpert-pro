<?php

namespace App\Controller;

use App\Repository\BrandRepository;
use App\Repository\CatalogueRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CatalogueController extends AbstractController
{
    #[Route('/catalogue/{id}', name: 'app_catalogue')]
    public function index($id, CatalogueRepository $catalogueRepository, BrandRepository $brandRepository): Response
    {
        $catalogue = $catalogueRepository->findBy(['brand' => $id]);
        $brand = $brandRepository->findOneBy(['id' => $id]);
        $brands = $brandRepository->findByCatalogue();
    

        return $this->render('catalogue/index.html.twig', [
            'catalogues' => $catalogue,
            'brand' => $brand,
            'brands' => $brands,
        ]);
    }
}
