<?php

namespace App\Twig;

use App\Classe\CartData;
use App\Repository\BrandRepository;
use App\Repository\CategoryRepository;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;
use Twig\TwigFilter;

class AppExtensions extends AbstractExtension implements GlobalsInterface
{
    private CategoryRepository $categoryRepository;
    private CartData $cart;
    private BrandRepository $brandRepository;

    public function __construct(
        CategoryRepository $categoryRepository,
        CartData $cart,
        BrandRepository $brandRepository
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->cart = $cart;
        $this->brandRepository = $brandRepository;
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('price', [$this, 'formatPrice']),
        ];
    }

    public function formatPrice($number): string
    {
        return number_format($number, 2, ',', ' ') . ' €';
    }

    public function getGlobals(): array
    {
        return [
            'brands' => $this->brandRepository->findAll(),
            'parentCategories' => $this->categoryRepository->findBy(['parentId' => null]),
            'allCategory' => $this->categoryRepository->findAll(),
            'fullCartQuantity' => $this->cart->fullQuantity(),
        ];
    }
}
