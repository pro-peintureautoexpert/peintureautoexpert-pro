<?php

namespace App\Classe;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class TokenService
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function getToken(): ?string
    {
        $response = $this->client->request('POST', 'https://ws.colissimo.fr/widget-colissimo/rest/authenticate.rest', [
            'json' => [
                // Ajoutez ici les paramètres nécessaires pour l'authentification
                'login' => '458744',
                'password' => 'Symfony123456@',
            ],
        ]);

        $data = $response->toArray();

        return $data['token'] ?? null;
    }

    // Function pour imprime ou reimprime l'etiquette de livraison :
    public function getlabel($parcelNumber)
    {
        $response = $this->client->request('POST', 'https://ws.colissimo.fr/sls-ws/SlsServiceWSRest/2.0/getLabel', [
            'headers' => [
                // 'Content-Type' => 'application/json',
                // 'Host' => 'ws.colissimo.fr',
                'login' => '458744',
                'password' => 'Symfony123456@',
            ],
            'json' => [
                'parcelNumber' => "$parcelNumber",
                'outputPrintingType' => 'PDF_A4_300dpi',
            ],
        ]);

        $result = $response->getContent();
        $date = new \DateTime();
        $etiquette = 'Colissimo_'.$date->format('Ymd_His').'_'.uniqid().'.pdf';
        $filePath = 'uploads/Colissimo/'.$etiquette;
        file_put_contents($filePath, $result);

        return $etiquette;
    }

    // function pour genere l'etiquette et imprime

    public function generatelabel($nameCompany, $city, $contryCode, $email, $lastName, $firstName, $line0, $line1, $phone, $zipCode, $weight, $sansAvecSignature)
    {
        $date = new \DateTime();

        $response = $this->client->request('POST', 'https://ws.colissimo.fr/sls-ws/SlsServiceWSRest/2.0/generateLabel', [
            'headers' => [
                'login' => '458744',
                'password' => 'Symfony123456@',
            ],
            'json' => [
                'contractNumber' => '458744',
                'password' => 'Symfony123456@',
                'outputFormat' => [
                    'x' => 0,
                    'y' => 0,
                    'outputPrintingType' => 'PDF_A4_300dpi',
                ],
                'letter' => [
                    'service' => [
                        'productCode' => "$sansAvecSignature",
                        'depositDate' => '2099-11-23',
                        'orderNumber' => 123456,
                        'commercialName' => 'PEINTURES AUTO PROS GROS',
                    ],
                    'parcel' => [
                        'weight' => $weight,
                    ],
                    'sender' => [
                        'senderParcelRef' => '',
                        'address' => [
                            'companyName' => 'PEINTURES AUTO PROS GROS (PAPG)',
                            'lastName' => 'Joseph',
                            'firstName' => 'Georges',
                            'line0' => '69 AV DE LA DIVISION LECLERC',
                            'line1' => '',
                            'line2' => 'Villetaneuse',
                            'line3' => '',
                            'countryCode' => 'FR',
                            'city' => 'Villetaneuse',
                            'zipCode' => 93430,
                            'phoneNumber' => '0175342549',
                            'mobileNumber' => '0175342549',
                            'doorCode1' => '',
                            'doorCode2' => '',
                            'email' => 'info@papg-auto.fr',
                            'intercom' => '',
                            'language' => 'FR',
                        ],
                    ],
                    'addressee' => [
                        'codeBarForReference' => false,
                        'serviceInfo' => 'service info',
                        'promotionCode' => 4444,
                        'address' => [
                            'companyName' => "$nameCompany",
                            'lastName' => "$lastName",
                            'firstName' => "$firstName",
                            'line0' => "$line0",
                            'line1' => '',
                            'line2' => "$line1",
                            'line3' => '',
                            'countryCode' => "$contryCode",
                            'city' => "$city",
                            'zipCode' => $zipCode,
                            'phoneNumber' => "$phone",
                            'mobileNumber' => "$phone",
                            'doorCode1' => '',
                            'doorCode2' => '',
                            'email' => "$email",
                            'intercom' => '',
                            'language' => 'FR',
                        ],
                    ],
                ],
            ],
        ]);

        $result = $response->getContent();
        $etiquette = 'Colissimo_'.$date->format('Ymd_His').'_'.uniqid().'.pdf';
        $filePath = 'uploads/Colissimo/'.$etiquette;
        file_put_contents($filePath, $result);

        return $etiquette;
    }

    // genere l'étiquette de retour
    public function generatereturn($nameCompany, $city, $contryCode, $lastName, $firstName, $line0, $line1, $zipCode, $weight)
    {
        $response = $this->client->request('POST', 'https://ws.colissimo.fr/sls-ws/SlsServiceWSRest/2.0/generateLabel', [
            'headers' => [
                'login' => '458744',
                'password' => 'Symfony123456@',
            ],
            'json' => [
                'contractNumber' => '458744',
                'password' => 'Symfony123456@',
                'outputFormat' => [
                    'x' => 0,
                    'y' => 0,
                    'outputPrintingType' => 'PDF_A4_300dpi',
                ],
                'letter' => [
                    'service' => [
                        'productCode' => 'CORE',
                        'depositDate' => '2099-11-23',
                        'orderNumber' => 123456,
                        'commercialName' => 'PEINTURES AUTO PROS GROS',
                    ],
                    'parcel' => [
                        'weight' => $weight,
                    ],
                    'sender' => [
                        'senderParcelRef' => '',
                        'address' => [
                            'companyName' => "$nameCompany",
                            'lastName' => "$lastName",
                            'firstName' => "$firstName",
                            'line0' => "$line0",
                            'line1' => '',
                            'line2' => "$line1",
                            'line3' => '',
                            'countryCode' => "$contryCode",
                            'city' => "$city",
                            'zipCode' => $zipCode,
                        ],
                    ],
                    'addressee' => [
                        'address' => [
                            'companyName' => 'PEINTURES AUTO PROS GROS (PAPG)',
                            'lastName' => 'Joseph',
                            'firstName' => 'Georges',
                            'line0' => '69 AV DE LA DIVISION LECLERC',
                            'line1' => '',
                            'line2' => 'Villetaneuse',
                            'line3' => '',
                            'countryCode' => 'FR',
                            'city' => 'Villetaneuse',
                            'zipCode' => 93430,
                        ],
                    ],
                ],
            ],
        ]);

        $result = $response->getContent();
        $date = new \DateTime();
        $etiquette = 'Colissimo_'.$date->format('Ymd_His').'_'.uniqid().'.pdf';
        $filePath = 'uploads/Colissimo/'.$etiquette;
        file_put_contents($filePath, $result);

        return $etiquette;
    }

    // etiquette point relai
    public function generatepointrelai($city, $contryCode, $email, $lastName, $firstName, $line0, $line1, $phone, $zipCode, $weight, $pickUpId)
    {
        $response = $this->client->request('POST', 'https://ws.colissimo.fr/sls-ws/SlsServiceWSRest/2.0/generateLabel', [
            'headers' => [
                'login' => '458744',
                'password' => 'Symfony123456@',
            ],
            'json' => [
                'contractNumber' => '458744',
                'password' => 'Symfony123456@',
                'outputFormat' => [
                    'x' => 0,
                    'y' => 0,
                    'outputPrintingType' => 'PDF_A4_300dpi',
                ],
                'letter' => [
                    'service' => [
                        'productCode' => 'HD',
                        'depositDate' => '2099-11-23',
                        'orderNumber' => 123456,
                        'commercialName' => 'PEINTURES AUTO PROS GROS',
                    ],
                    'parcel' => [
                        'weight' => $weight,
                        'pickupLocationId' => "$pickUpId",
                    ],
                    'sender' => [
                        'senderParcelRef' => '',
                        'address' => [
                            'companyName' => 'PEINTURES AUTO PROS GROS (PAPG)',
                            'line0' => '69 AV DE LA DIVISION LECLERC',
                            'line1' => '',
                            'line2' => 'Villetaneuse',
                            'line3' => '',
                            'countryCode' => 'FR',
                            'city' => 'Villetaneuse',
                            'zipCode' => 93430,
                        ],
                    ],
                    'addressee' => [
                        'address' => [
                            'lastName' => "$lastName",
                            'firstName' => "$firstName",
                            'line0' => "$line0",
                            'line1' => '',
                            'line2' => "$line1",
                            'line3' => '',
                            'countryCode' => "$contryCode",
                            'city' => "$city",
                            'zipCode' => $zipCode,
                            'mobileNumber' => "$phone",
                            'email' => "$email",
                        ],
                    ],
                ],
            ],
        ]);

        $result = $response->getContent();
        $date = new \DateTime();
        $etiquette = 'Colissimo_'.$date->format('Ymd_His').'_'.uniqid().'.pdf';
        $filePath = 'uploads/Colissimo/'.$etiquette;
        file_put_contents($filePath, $result);

        return $etiquette;
    }
}
