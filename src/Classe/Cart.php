<?php

namespace App\Classe;

use Symfony\Component\HttpFoundation\RequestStack;

class Cart
{
    // public function __construct(private RequestStack $requestStack)
    // {
    // }

    /*
     * add()
     * Fonction permettant l'ajout d'un produit au panier
     */
    // public function add($product)
    // {
    //     // Appeler la session CART de symfony
    //     $cart = $this->getCart();

    //     // Ajouter une qtity +1 à mon produit
    //     if (isset($cart[$product->getId()])) {
    //         $cart[$product->getId()] = [
    //             'object' => $product,
    //             'qty' => $cart[$product->getId()]['qty'] + 1,
    //         ];
    //     } else {
    //         $cart[$product->getId()] = [
    //             'object' => $product,
    //             'qty' => 1,
    //         ];
    //     }

    //     // Créer ma session Cart
    //     $this->requestStack->getSession()->set('cart', $cart);
    // }

    // fonction in parametres qte and product
    // public function addQte($product, $qte)
    // {
    //     // Appeler la session CART de symfony
    //     $cart = $this->getCart();

    //     // Ajouter une qtity +1 à mon produit
    //     if (isset($cart[$product->getId()])) {
    //         $cart[$product->getId()] = [
    //             'object' => $product,
    //             'qty' => $qte,
    //         ];
    //     } else {
    //         $cart[$product->getId()] = [
    //             'object' => $product,
    //             'qty' => $qte,
    //         ];
    //     }

    //     // Créer ma session Cart
    //     $this->requestStack->getSession()->set('cart', $cart);
    // }

    /*
     * decrease()
     * Fonction permettant la suppression d'une quantity d'un produit au panier
     */
    // public function decrease($id)
    // {
    //     $cart = $this->getCart();

    //     if ($cart[$id]['qty'] > 1) {
    //         $cart[$id]['qty'] = $cart[$id]['qty'] - 1;
    //     } else {
    //         unset($cart[$id]);
    //     }

    //     $this->requestStack->getSession()->set('cart', $cart);
    // }

    /*
     * fullQuantity()
     * Fonction retournant le nombre total de produit au panier
    //  */
    // public function fullQuantity()
    // {
    //     $cart = $this->getCart();
    //     $quantity = 0;

    //     if (!isset($cart)) {
    //         return $quantity;
    //     }

    //     foreach ($cart as $product) {
    //         $quantity = $quantity + $product['qty'];
    //     }

    //     return $quantity;
    // }

    /*
     * getTotalWt()
     * Fonction retournant le prix total des produits au panier
     */
    // public function getTotalWt(): float
    // {
    //     $cart = $this->getCart();
    //     $totalPrice = 0.0;

    //     if (empty($cart)) {
    //         return $totalPrice;
    //     }

    //     foreach ($cart as $product) {
    //         $object = $product['object'] ?? null;

    //         // if (!$object instanceof Product) {
    //         //     // Ignorer les éléments invalides ou mal formés
    //         //     continue;
    //         // }

    //         $qty = $product['qty'] ?? 0;

    //         // Calcul du prix selon la logique "par palette" ou "unitaire"
    //         if (null !== $object->getNumbreUnitePalette() && $qty >= $object->getNumbreUnitePalette()) {
    //             $totalPrice += $object->getPriceP() * $qty;
    //         } else {
    //             $totalPrice += $object->getPriceU() * $qty;
    //         }
    //     }

    //     return $totalPrice;
    // }

    /*
     * remove()
     * Fonction permettant de supprimer totalement le panier
     */
    // public function remove()
    // {
    //     return $this->requestStack->getSession()->remove('cart');
    // }

    /*
     * getCart()
     * Fonction retournant le panier
     */
    // public function getCart()
    // {
    //     return $this->requestStack->getSession()->get('cart');
    // }

}
