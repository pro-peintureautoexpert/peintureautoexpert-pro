<?php

namespace App\Classe;

use App\Entity\Panier;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class CartData
{
    private EntityManagerInterface $em;
    private $user;

    public function __construct(EntityManagerInterface $em, Security $security)
    {
        $this->em = $em;
        $this->user = $security->getUser();
    }

    public function ajouter(Product $product): void
    {
        
        if (!$this->user) {
            return; // Aucun utilisateur connecté
        }
        
        // Vérifier si le produit est déjà dans le panier
        $panier = $this->em->getRepository(Panier::class)->findOneBy([
            'user' => $this->user,
            'produit' => $product,
        ]);
        

        if ($panier) {
            $panier->setQte($panier->getQte() + 1 ); // Incrémenter la quantité
        } else {
            $panier = new Panier();
            $panier->setUser($this->user);
            $panier->setProduit($product);
            $panier->setQte(1);
            $this->em->persist($panier);
        }

        $this->em->flush();
    }


    public function addMulti($products): void
    {
        // Vérifie si l'utilisateur est connecté
        if (!$this->user) {
            return; // Aucun utilisateur connecté
        }
    
        // Parcours les produits à ajouter au panier
        foreach ($products as $product) {
            // Vérifier si le produit est déjà dans le panier
            $panier = $this->em->getRepository(Panier::class)->findOneBy([
                'user' => $this->user,
                'produit' => $product,
            ]);
    
            // Si le produit est déjà dans le panier, on incrémente la quantité
            if ($panier) {
                $panier->setQte($panier->getQte() + 1);
            } else {
                // Sinon, on crée un nouveau panier pour ce produit
                $panier = new Panier();
                $panier->setUser($this->user);
                $panier->setProduit($product);
                $panier->setQte(1);
                $this->em->persist($panier);
            }
        }
    
        // Enregistrer les modifications dans la base de données
        $this->em->flush();
    }
    
    public function addQuntite(Product $product, int $qte): void
    {
        if (!$this->user) {
            return; // Aucun utilisateur connecté
        }
        if ($qte <= 0) {
            throw new \InvalidArgumentException('La quantité doit être positive.');
        }

        // Vérifier si le produit est déjà dans le panier
        $panier = $this->em->getRepository(Panier::class)->findOneBy([
            'user' => $this->user,
            'produit' => $product,
        ]);

        if ($panier) {
            $panier->setQte($qte); // Incrémenter la quantité
        } else {
            $panier = new Panier();
            $panier->setUser($this->user);
            $panier->setProduit($product);
            $panier->setQte($qte);
            $this->em->persist($panier);
        }

        $this->em->flush();
    }


    /**
     * Supprimer un produit du panier.
     *
     * @param Product $product
     */
    public function supprimer(Product $product): void
    {
        if (!$this->user) {
            return;
        }

        $panier = $this->em->getRepository(Panier::class)->findOneBy([
            'user' => $this->user,
            'produit' => $product,
        ]);

        if ($panier) {
            if($panier->getQte() > 1){
            $panier->setQte($panier->getQte() -1);
            }else{
                $this->em->remove($panier);
            }
            $this->em->flush();
        }
    }

    /**
     * Vider complètement le panier.
     */
    public function vider(): void
    {
        if (!$this->user) {
            return;
        }

        $paniers = $this->em->getRepository(Panier::class)->findBy([
            'user' => $this->user,
        ]);

       

        foreach ($paniers as $panier) {
            $this->em->remove($panier);
        }

        $this->em->flush();
    }
    
    public function afficher(): array
    {
        if (!$this->user) {
            return [];
        }

        return $this->em->getRepository(Panier::class)->findBy([
            'user' => $this->user,
        ]);
    }

    public function fullQuantity(): int
    {
        if (!$this->user) {
            return 0; // Retourne 0 au lieu d'un tableau vide si l'utilisateur n'est pas connecté
        }
    
        $paniers = $this->em->getRepository(Panier::class)->findBy([
            'user' => $this->user,
        ]);
    
        $qte = 0;
    
        foreach ($paniers as $panier) {
            $qte += $panier->getQte(); // Utilisation de l'opérateur += pour plus de lisibilité
        }
    
        return $qte;
    }

    public function getTotalWt(): float
    {
        if (!$this->user) {
            return 0.0; // Retourne 0 si l'utilisateur n'est pas connecté
        }

        // Récupération des paniers de l'utilisateur
        $paniers = $this->em->getRepository(Panier::class)->findBy([
            'user' => $this->user,
        ]);

        $totalPrice = 0.0;

        foreach ($paniers as $panier) {
            $product = $panier->getProduit();

            if (!$product) {
                continue; // Ignorer les produits invalides
            }

            $qty = $panier->getQte();

            // Calcul du prix selon la logique "par palette" ou "unitaire"
            if (null !== $product->getNumbreUnitePalette() && $qty >= $product->getNumbreUnitePalette()) {
                $totalPrice += $product->getPriceP() * $qty;
            } else {
                $totalPrice += $product->getPriceU() * $qty;
            }
        }

        return $totalPrice;
    }



    public function calculatePriceForCartItem(Panier $cartItem): float
    {
        $product = $cartItem->getProduit();
        $quantity = $cartItem->getQte();

        $qtyCartons = $product->getQtyCartons()->toArray();

        // Trier les options par quantité croissante
        usort($qtyCartons, fn($a, $b) => $a->getQty() <=> $b->getQty());

        $price = 0;
        foreach ($qtyCartons as $qtyCarton) {
            if ($quantity >= $qtyCarton->getQty()) {
                $price = $qtyCarton->getPriceQte();
            } else {
                break;
            }
        }

        return $price ;
    }

    public function calculateTotalPrice(): float
    {
        if (!$this->user) {
            return 0.0; // Retourne 0 si l'utilisateur n'est pas connecté
        }

        // Récupération des paniers de l'utilisateur
        $cartItems = $this->em->getRepository(Panier::class)->findBy([
            'user' => $this->user,
        ]);

        $total = 0;
        foreach ($cartItems as $cartItem) {
            $qty = $cartItem->getQte();
            if( $this->calculatePriceForCartItem($cartItem) > 0){

                $total += $this->calculatePriceForCartItem($cartItem)  * $qty;
            }else{
                $total += $cartItem->getProduit()->getPriceU() * $qty;
            }
        }
        return $total;
    }
}
