<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\OrderRepository;

class Plafond
{
    private OrderRepository $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * Vérifie si l'utilisateur a un plafond défini.
     */
    public function getPlafondForUser(User $user): ?float
    {
        $plafondEntity = $user->getMagasinPlafond();

        if (!$plafondEntity || !$plafondEntity->getValue()) {
            return null; // Aucun plafond défini
        }

        return $plafondEntity->getValue();
    }

    /**
     * Calcule le total des commandes non payées pour un utilisateur.
     */
    public function getTotalOrdersForUser(User $user): float
    {
        $orders = $this->orderRepository->findBy([
            'user' => $user,
            'state' => [2, 3, 4], // États autorisés
            'statePay' => 1, // Non payé
        ]);

        $total = 0;
        foreach ($orders as $order) {
            $total += $order->getTotalWt();
        }

        return $total;
    }

    /**
     * Vérifie si l'utilisateur peut passer une commande avec le montant donné.
     */
    public function canPlaceOrder(User $user, float $montantCommande): bool
    {
        $plafond = $this->getPlafondForUser($user);

        if ($plafond === null) {
            throw new \RuntimeException('Le plafond n\'est pas défini pour cet utilisateur.');
        }

        $totalOrders = $this->getTotalOrdersForUser($user);
        $montantDisponible = $plafond - $totalOrders;

        return $montantCommande <= $montantDisponible;
    }

    /**
     * Retourne le montant restant disponible dans le plafond d'un utilisateur.
     */
    public function getMontantDisponible(User $user): float
    {
        $plafond = $this->getPlafondForUser($user);

        if ($plafond === null) {
            throw new \RuntimeException('Le plafond n\'est pas défini pour cet utilisateur.');
        }

        $totalOrders = $this->getTotalOrdersForUser($user);

        return $plafond - $totalOrders;
    }
}
