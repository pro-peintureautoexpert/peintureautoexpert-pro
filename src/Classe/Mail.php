<?php

namespace App\Classe;

use Mailjet\Client;
use Mailjet\Resources;

class Mail
{
    public function send($to_email, $to_name, $subject, $vars = null)
    {
        // Récupération du template
        // $content = file_get_contents(dirname(__DIR__).'/Mail/'.$template);

        // Récupère les variables facultatives
        // if ($vars) {
        //     foreach($vars as $key=>$var) {
        //         $content = str_replace('{'.$key.'}', $var, $content);
        //     }
        // }

        $mj = new Client($_ENV['MJ_APIKEY_PUBLIC'], $_ENV['MJ_APIKEY_PRIVATE'], true, ['version' => 'v3.1']);
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => 'developpeur@peintureautoexpert.com',
                        'Name' => 'Peintureautoexpert',
                    ],
                    'To' => [
                        [
                            'Email' => $to_email,
                            'Name' => $to_name,
                        ],
                    ],
                    'TemplateID' => 5926509,
                    'TemplateLanguage' => true,
                    'Subject' => $subject,
                    'Variables' => [
                        'Uuid' => $vars['Uuid'], // Remplace {Uuid} dans le template
                        'Password' => $vars['Password'], // Remplace {Password} dans le template
                        'name' => $to_name, // Remplace {name} dans le template
                    ],
                ],
            ],
        ];

        $mj->post(Resources::$Email, ['body' => $body]);
    }


    public function notification($to_name, $subject, $vars = null)
    {
        if (!is_array($vars) || !isset($vars['Numero_de_commande'], $vars['date'], $vars['statutPay'], $vars['statutOrder'], 
            $vars['nameComplet'], $vars['emailClient'], $vars['phone'], $vars['line0'], $vars['postal'], $vars['city'], 
            $vars['pays'], $vars['stripe_id'], $vars['idOrder'])) {
            throw new \InvalidArgumentException("Toutes les variables nécessaires ne sont pas fournies dans le tableau \$vars.");
        }
    
        if (empty($_ENV['MJ_APIKEY_PUBLIC']) || empty($_ENV['MJ_APIKEY_PRIVATE'])) {
            throw new \RuntimeException("Les clés API Mailjet ne sont pas définies dans les variables d'environnement.");
        }
    
        $mj = new Client($_ENV['MJ_APIKEY_PUBLIC'], $_ENV['MJ_APIKEY_PRIVATE'], true, ['version' => 'v3.1']);
    
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => 'developpeur@peintureautoexpert.com',
                        'Name' => 'X-PRO',
                    ],
                    'To' => [
                        [
                            'Email' => 'walidfr.digital@gmail.com', // Adresse e-mail rendue dynamique
                            'Name' => $to_name,
                        ],
                    ],
                    'TemplateID' =>6549188,
                    'TemplateLanguage' => true,
                    'Subject' => $subject,
                    'Variables' => [
                        'Numero_de_commande' => $vars['Numero_de_commande'],
                        'date' => $vars['date'],
                        'statutPay' => $vars['statutPay'],
                        'statutOrder' => $vars['statutOrder'],
                        'nameComplet' => $vars['nameComplet'],
                        'emailClient' => $vars['emailClient'],
                        'phone' => $vars['phone'],
                        'line0' => $vars['line0'],
                        'postal' => $vars['postal'],
                        'city' => $vars['city'],
                        'pays' => $vars['pays'],
                        'mode_pay' => 'carte',
                        'stripe_id' => $vars['stripe_id'],
                        'idOrder' => $vars['idOrder'],
                    ],
                ],
            ],
        ];
    
        $response = $mj->post(Resources::$Email, ['body' => $body]);
        if ($response->getStatus() === 200) {
            return $response->getBody(); // Succès
        } else {
            throw new \Exception("Erreur d'envoi d'email : " . $response->getReasonPhrase());
        }
    }


    public function notife($to_name, $subject, $vars = null)
    {

        $mj = new Client($_ENV['MJ_APIKEY_PUBLIC'], $_ENV['MJ_APIKEY_PRIVATE'], true, ['version' => 'v3.1']);
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => 'developpeur@peintureautoexpert.com',
                        'Name' => 'X-PRO',
                    ],
                    'To' => [
                        [
                            'Email' => 'papg.order@gmail.com',
                            'Name' => $to_name,
                        ],
                    ],
                    'TemplateID' => 6556456,
                    'TemplateLanguage' => true,
                    'Subject' => $subject,
                    'Variables' => [
                        'confirmation_link' => $vars['Numero_de_commande'],
                        'magasin' => $vars['nameComplet'],
                    ],
                ],
            ],
        ];

        $mj->post(Resources::$Email, ['body' => $body]);
    }


    public function resetPassword($to_email, $to_name, $subject, $vars = null)
    {
       
        $mj = new Client($_ENV['MJ_APIKEY_PUBLIC'], $_ENV['MJ_APIKEY_PRIVATE'], true, ['version' => 'v3.1']);
       $body = [
           'Messages' => [
               [
                   'From' => [
                       'Email' => "developpeur@peintureautoexpert.com",
                       'Name' => "X-PRO"
                   ],
                   'To' => [
                       [
                           'Email' => $to_email,
                           'Name' => $to_name,
                       ]
                   ],
                   'TemplateID' => 6615700,
                   'TemplateLanguage' => true,
                   'Subject' => $subject,
                   'Variables' => [
                    'line0' => $vars['lien'],
                   ]
               ]
           ]
       ];
         $mj->post(Resources::$Email, ['body' => $body]);
}


}
