<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ORM\Table(name: '`order`')]
class Order
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $createdAt = null;

    #[ORM\Column]
    private ?int $state = null;

    #[ORM\Column(length: 255)]
    private ?string $carrierName = null;

    #[ORM\Column]
    private ?float $carrierPrice = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $delivrey = null;

    #[ORM\OneToMany(targetEntity: OrderDetail::class, mappedBy: 'myOrder', cascade: ['persist'])]
    private Collection $orderDetails;

    #[ORM\ManyToOne(inversedBy: 'orders')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\Column]
    private ?int $statePay = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $stripe_session_id = null;

    #[ORM\Column(length: 255)]
    private ?string $picker = null;

    #[ORM\OneToOne(mappedBy: 'reference', cascade: ['persist', 'remove'])]
    private ?AddressItem $addressItem = null;

    #[ORM\ManyToOne(inversedBy: 'ticket')]
    private ?TagsColissimo $tagsColissimo = null;

    public function __construct()
    {
        $this->orderDetails = new ArrayCollection();
    }


    public function __toString(){
        return  "Réf " . $this->getId(). " - " . $this->getUser()->getMagasinName() ;
        }
    // public function __toString()
    // {
    //     return $this->getId();
    // }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getState(): ?int
    {
        return $this->state;
    }

    public function setState(int $state): static
    {
        $this->state = $state;

        return $this;
    }

    public function getCarrierName(): ?string
    {
        return $this->carrierName;
    }

    public function setCarrierName(string $carrierName): static
    {
        $this->carrierName = $carrierName;

        return $this;
    }

    public function getCarrierPrice(): ?float
    {
        return $this->carrierPrice;
    }

    public function setCarrierPrice(float $carrierPrice): static
    {
        $this->carrierPrice = $carrierPrice;

        return $this;
    }

    public function getDelivrey(): ?string
    {
        return $this->delivrey;
    }

    public function setDelivrey(string $delivrey): static
    {
        $this->delivrey = $delivrey;

        return $this;
    }

    public function getTotalWt()
    {
        $totalTTC = 0;
        $products = $this->getOrderDetails();

        foreach ($products as $product) {
            $coeff = 1 + ($product->getProductTva() / 100);
            $totalTTC += ($product->getProductPrice() * $coeff) * $product->getProductQuantity();
        }

        return $totalTTC + ($this->getCarrierPrice() * 1.20);
    }

    public function getTotalTva()
    {
        $totalTva = 0;
        $products = $this->getOrderDetails();

        foreach ($products as $product) {
            $coeff = $product->getProductTva() / 100;
            $totalTva += $product->getProductPrice() * $coeff;
        }

        $totalTva += ($this->getCarrierPrice() * 0.20);

        return $totalTva;
    }

    /**
     * @return Collection<int, OrderDetail>
     */
    public function getOrderDetails(): Collection
    {
        return $this->orderDetails;
    }

    public function addOrderDetail(OrderDetail $orderDetail): static
    {
        if (!$this->orderDetails->contains($orderDetail)) {
            $this->orderDetails->add($orderDetail);
            $orderDetail->setMyOrder($this);
        }

        return $this;
    }

    public function removeOrderDetail(OrderDetail $orderDetail): static
    {
        if ($this->orderDetails->removeElement($orderDetail)) {
            // set the owning side to null (unless already changed)
            if ($orderDetail->getMyOrder() === $this) {
                $orderDetail->setMyOrder(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getStatePay(): ?int
    {
        return $this->statePay;
    }

    public function setStatePay(int $statePay): static
    {
        $this->statePay = $statePay;

        return $this;
    }

    public function getStripeSessionId(): ?string
    {
        return $this->stripe_session_id;
    }

    public function setStripeSessionId(?string $stripe_session_id): static
    {
        $this->stripe_session_id = $stripe_session_id;

        return $this;
    }

    public function getPicker(): ?string
    {
        return $this->picker;
    }

    public function setPicker(string $picker): static
    {
        $this->picker = $picker;

        return $this;
    }

    public function getAddressItem(): ?AddressItem
    {
        return $this->addressItem;
    }

    public function setAddressItem(?AddressItem $addressItem): static
    {
        // unset the owning side of the relation if necessary
        if (null === $addressItem && null !== $this->addressItem) {
            $this->addressItem->setReference(null);
        }

        // set the owning side of the relation if necessary
        if (null !== $addressItem && $addressItem->getReference() !== $this) {
            $addressItem->setReference($this);
        }

        $this->addressItem = $addressItem;

        return $this;
    }

    public function getTagsColissimo(): ?TagsColissimo
    {
        return $this->tagsColissimo;
    }

    public function setTagsColissimo(?TagsColissimo $tagsColissimo): static
    {
        $this->tagsColissimo = $tagsColissimo;

        return $this;
    }
}
