<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\UX\Turbo\Attribute\Broadcast;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
#[Broadcast]
class Product
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $articleCode = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $slug = null;

    #[ORM\Column(length: 255)]
    private ?string $description = null;

    #[ORM\Column]
    private ?float $weight = null;

    #[ORM\Column(length: 255)]
    private ?string $illustration = null;

    #[ORM\Column]
    private ?float $priceUnitaire = null;

    #[ORM\Column]
    private ?float $pricePalette = null;

    #[ORM\Column]
    private ?int $stock = null;

    #[ORM\ManyToOne(inversedBy: 'products')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Category $Category = null;

    #[ORM\ManyToMany(targetEntity: Promotions::class, mappedBy: 'produit')]
    private Collection $promotions;

    #[ORM\ManyToMany(targetEntity: Image::class, inversedBy: 'products')]
    private Collection $images;

    #[ORM\Column]
    private ?bool $isHomepage = null;

    #[ORM\Column]
    private ?float $tva = null;

    #[ORM\ManyToMany(targetEntity: TechniqueProduct::class, mappedBy: 'product')]
    private Collection $techniqueProducts;

    #[ORM\ManyToOne(inversedBy: 'products')]
    private ?Grain $grain = null;

    #[ORM\ManyToOne(inversedBy: 'products')]
    private ?ConMastic $conMastic = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?ConAppret $conAppret = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?ConVernis $conVernis = null;

    #[ORM\Column]
    private ?bool $inHome = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?Rapidite $rapidite = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?Malette $malette = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?CouleurAll $couleurAll = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?FilterDimensionCale $filterDimensionCale = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?DimensionDoubleFace $dimensionDoubleFace = null;

    #[ORM\Column(nullable: true)]
    private ?int $numbreUnitePalette = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?Buse $buse = null;

    #[ORM\Column]
    private ?bool $inPaint = null;

    #[ORM\ManyToOne(inversedBy: 'products')]
    private ?ProductParent $productParent = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?ConAntigravillon $conAntigravillon = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?ConDegraissant $conDegraissant = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?CapaciteAspirateur $capaciteAspirateur = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?ConPolish $conPolish = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?ConSavon $conSavon = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?ConDiluant $conDiluant = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?ConGobelet $conGobelet = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?ConBidonPlastique $conBidonPlastique = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?ConDurcisseur $conDurcisseur = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?DimansionFiltreCabine $dimansionFiltreCabine = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?DiametreZikzak $diametreZikzak = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?DiametreAgrafes $diametreAgrafes = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?DimensionChariotKraft $dimensionChariotKraft = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?DimensionPapierCale $dimensionPapierCale = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?FiltreCone $filtreCone = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?DimensionMicroBruch $dimensionMicroBruch = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?PinceauDimension $pinceauDimension = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?RegleDosage $regleDosage = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?DimensionKraft $dimensionKraft = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?DimensionTireau $dimensionTireau = null;

    #[ORM\ManyToOne(inversedBy: 'product')]
    private ?TailleFineTape $tailleFineTape = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?Effet $effet = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?Aspirateur $aspirateur = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?Ponceuse $ponceuse = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?TypePistole $typePistole = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?SolPlafond $solPlafond = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?AcierAlu $acierAlu = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?Taille $taille = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?DimensionAll $dimensionAll = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?Matiere $matiere = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?Masquage $masquage = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?Nettoyage $nettoyage = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?Durcisseur $durcisseur = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?AccessoireAspirateur $accessoireAspirateur = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?Vitrage $vitrage = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?TypeRaccord $typeRaccord = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?TypeMateriel $typeMateriel = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?Materiel $materiel = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?ColleJoint $colleJoint = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?Adhesif $adhesif = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?TypePonceuse $typePonceuse = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?TypeMastic $typeMastic = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?TypeDiluant $typeDiluant = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?Accessoire $accessoire = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?AccessoirePolisseuse $accessoirePolisseuse = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?DebosseleurMarteau $debosseleurMarteau = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?EquerreTirageGriffe $equerreTirageGriffe = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?ChaineRaccord $chaineRaccord = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?BarreDebosslage $barreDebosslage = null;

    #[ORM\ManyToOne(inversedBy: 'produit')]
    private ?MasseMagnetiqueChario $masseMagnetiqueChario = null;

    #[ORM\Column]
    private ?bool $inLabo = null;

    /**
     * @var Collection<int, Panier>
     */
    #[ORM\OneToMany(targetEntity: Panier::class, mappedBy: 'produit')]
    private Collection $paniers;

    /**
     * @var Collection<int, QtyCarton>
     */
    #[ORM\OneToMany(targetEntity: QtyCarton::class, mappedBy: 'object')]
    private Collection $qtyCartons;

    #[ORM\OneToOne(mappedBy: 'produit', cascade: ['persist', 'remove'])]
    private ?CodeBarre $codeBarre = null;

    #[ORM\Column(nullable: true)]
    private ?float $pvc = null;

    public function __construct()
    {
        $this->promotions = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->techniqueProducts = new ArrayCollection();
        $this->paniers = new ArrayCollection();
        $this->qtyCartons = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName().' '.$this->getArticleCode();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArticleCode(): ?string
    {
        return $this->articleCode;
    }

    public function setArticleCode(string $articleCode): static
    {
        $this->articleCode = $articleCode;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function setWeight(float $weight): static
    {
        $this->weight = $weight;

        return $this;
    }

    public function getIllustration(): ?string
    {
        return $this->illustration;
    }

    public function setIllustration(string $illustration): static
    {
        $this->illustration = $illustration;

        return $this;
    }

    public function getPriceUnitaire(): ?float
    {
        return $this->priceUnitaire;
    }

    public function setPriceUnitaire(float $priceUnitaire): static
    {
        $this->priceUnitaire = $priceUnitaire;

        return $this;
    }

    public function getPricePalette(): ?float
    {
        return $this->pricePalette;
    }

    public function getPriceU()
    {
        return $this->priceUnitaire / 100;
    }

    public function getPriceP()
    {
        return $this->pricePalette / 100;
    }

    public function setPricePalette(float $pricePalette): static
    {
        $this->pricePalette = $pricePalette;

        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(int $stock): static
    {
        $this->stock = $stock;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->Category;
    }

    public function setCategory(?Category $Category): static
    {
        $this->Category = $Category;

        return $this;
    }

    /**
     * @return Collection<int, Promotions>
     */
    public function getPromotions(): Collection
    {
        return $this->promotions;
    }

    public function addPromotion(Promotions $promotion): static
    {
        if (!$this->promotions->contains($promotion)) {
            $this->promotions->add($promotion);
            $promotion->addProduit($this);
        }

        return $this;
    }

    public function removePromotion(Promotions $promotion): static
    {
        if ($this->promotions->removeElement($promotion)) {
            $promotion->removeProduit($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Image>
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): static
    {
        if (!$this->images->contains($image)) {
            $this->images->add($image);
        }

        return $this;
    }

    public function removeImage(Image $image): static
    {
        $this->images->removeElement($image);

        return $this;
    }

    public function isIsHomepage(): ?bool
    {
        return $this->isHomepage;
    }

    public function setIsHomepage(bool $isHomepage): static
    {
        $this->isHomepage = $isHomepage;

        return $this;
    }

    public function getTva(): ?float
    {
        return $this->tva;
    }

    public function getPriceWt()
    {
        $coeff = 1 + ($this->tva / 100);

        return $coeff * $this->priceUnitaire / 100;
    }

    public function setTva(float $tva): static
    {
        $this->tva = $tva;

        return $this;
    }

    /**
     * @return Collection<int, TechniqueProduct>
     */
    public function getTechniqueProducts(): Collection
    {
        return $this->techniqueProducts;
    }

    public function addTechniqueProduct(TechniqueProduct $techniqueProduct): static
    {
        if (!$this->techniqueProducts->contains($techniqueProduct)) {
            $this->techniqueProducts->add($techniqueProduct);
            $techniqueProduct->addProduct($this);
        }

        return $this;
    }

    public function removeTechniqueProduct(TechniqueProduct $techniqueProduct): static
    {
        if ($this->techniqueProducts->removeElement($techniqueProduct)) {
            $techniqueProduct->removeProduct($this);
        }

        return $this;
    }

    public function getGrain(): ?Grain
    {
        return $this->grain;
    }

    public function setGrain(?Grain $grain): static
    {
        $this->grain = $grain;

        return $this;
    }

    public function getConMastic(): ?ConMastic
    {
        return $this->conMastic;
    }

    public function setConMastic(?ConMastic $conMastic): static
    {
        $this->conMastic = $conMastic;

        return $this;
    }

    public function getConAppret(): ?ConAppret
    {
        return $this->conAppret;
    }

    public function setConAppret(?ConAppret $conAppret): static
    {
        $this->conAppret = $conAppret;

        return $this;
    }

    public function getConVernis(): ?ConVernis
    {
        return $this->conVernis;
    }

    public function setConVernis(?ConVernis $conVernis): static
    {
        $this->conVernis = $conVernis;

        return $this;
    }

    public function isInHome(): ?bool
    {
        return $this->inHome;
    }

    public function setInHome(bool $inHome): static
    {
        $this->inHome = $inHome;

        return $this;
    }

    public function getRapidite(): ?Rapidite
    {
        return $this->rapidite;
    }

    public function setRapidite(?Rapidite $rapidite): static
    {
        $this->rapidite = $rapidite;

        return $this;
    }

    public function getMalette(): ?Malette
    {
        return $this->malette;
    }

    public function setMalette(?Malette $malette): static
    {
        $this->malette = $malette;

        return $this;
    }

    public function getCouleurAll(): ?CouleurAll
    {
        return $this->couleurAll;
    }

    public function setCouleurAll(?CouleurAll $couleurAll): static
    {
        $this->couleurAll = $couleurAll;

        return $this;
    }

    public function getFilterDimensionCale(): ?FilterDimensionCale
    {
        return $this->filterDimensionCale;
    }

    public function setFilterDimensionCale(?FilterDimensionCale $filterDimensionCale): static
    {
        $this->filterDimensionCale = $filterDimensionCale;

        return $this;
    }

    public function getDimensionDoubleFace(): ?DimensionDoubleFace
    {
        return $this->dimensionDoubleFace;
    }

    public function setDimensionDoubleFace(?DimensionDoubleFace $dimensionDoubleFace): static
    {
        $this->dimensionDoubleFace = $dimensionDoubleFace;

        return $this;
    }

    public function getNumbreUnitePalette(): ?int
    {
        return $this->numbreUnitePalette;
    }

    public function setNumbreUnitePalette(?int $numbreUnitePalette): static
    {
        $this->numbreUnitePalette = $numbreUnitePalette;

        return $this;
    }

    public function getBuse(): ?Buse
    {
        return $this->buse;
    }

    public function setBuse(?Buse $buse): static
    {
        $this->buse = $buse;

        return $this;
    }

    public function isInPaint(): ?bool
    {
        return $this->inPaint;
    }

    public function setInPaint(bool $inPaint): static
    {
        $this->inPaint = $inPaint;

        return $this;
    }

    public function getProductParent(): ?ProductParent
    {
        return $this->productParent;
    }

    public function setProductParent(?ProductParent $productParent): static
    {
        $this->productParent = $productParent;

        return $this;
    }

    public function getConAntigravillon(): ?ConAntigravillon
    {
        return $this->conAntigravillon;
    }

    public function setConAntigravillon(?ConAntigravillon $conAntigravillon): static
    {
        $this->conAntigravillon = $conAntigravillon;

        return $this;
    }

    public function getConDegraissant(): ?ConDegraissant
    {
        return $this->conDegraissant;
    }

    public function setConDegraissant(?ConDegraissant $conDegraissant): static
    {
        $this->conDegraissant = $conDegraissant;

        return $this;
    }

    public function getCapaciteAspirateur(): ?CapaciteAspirateur
    {
        return $this->capaciteAspirateur;
    }

    public function setCapaciteAspirateur(?CapaciteAspirateur $capaciteAspirateur): static
    {
        $this->capaciteAspirateur = $capaciteAspirateur;

        return $this;
    }

    public function getConPolish(): ?ConPolish
    {
        return $this->conPolish;
    }

    public function setConPolish(?ConPolish $conPolish): static
    {
        $this->conPolish = $conPolish;

        return $this;
    }

    public function getConSavon(): ?ConSavon
    {
        return $this->conSavon;
    }

    public function setConSavon(?ConSavon $conSavon): static
    {
        $this->conSavon = $conSavon;

        return $this;
    }

    public function getConDiluant(): ?ConDiluant
    {
        return $this->conDiluant;
    }

    public function setConDiluant(?ConDiluant $conDiluant): static
    {
        $this->conDiluant = $conDiluant;

        return $this;
    }

    public function getConGobelet(): ?ConGobelet
    {
        return $this->conGobelet;
    }

    public function setConGobelet(?ConGobelet $conGobelet): static
    {
        $this->conGobelet = $conGobelet;

        return $this;
    }

    public function getConBidonPlastique(): ?ConBidonPlastique
    {
        return $this->conBidonPlastique;
    }

    public function setConBidonPlastique(?ConBidonPlastique $conBidonPlastique): static
    {
        $this->conBidonPlastique = $conBidonPlastique;

        return $this;
    }

    public function getConDurcisseur(): ?ConDurcisseur
    {
        return $this->conDurcisseur;
    }

    public function setConDurcisseur(?ConDurcisseur $conDurcisseur): static
    {
        $this->conDurcisseur = $conDurcisseur;

        return $this;
    }

    public function getDimansionFiltreCabine(): ?DimansionFiltreCabine
    {
        return $this->dimansionFiltreCabine;
    }

    public function setDimansionFiltreCabine(?DimansionFiltreCabine $dimansionFiltreCabine): static
    {
        $this->dimansionFiltreCabine = $dimansionFiltreCabine;

        return $this;
    }

    public function getDiametreZikzak(): ?DiametreZikzak
    {
        return $this->diametreZikzak;
    }

    public function setDiametreZikzak(?DiametreZikzak $diametreZikzak): static
    {
        $this->diametreZikzak = $diametreZikzak;

        return $this;
    }

    public function getDiametreAgrafes(): ?DiametreAgrafes
    {
        return $this->diametreAgrafes;
    }

    public function setDiametreAgrafes(?DiametreAgrafes $diametreAgrafes): static
    {
        $this->diametreAgrafes = $diametreAgrafes;

        return $this;
    }

    public function getDimensionChariotKraft(): ?DimensionChariotKraft
    {
        return $this->dimensionChariotKraft;
    }

    public function setDimensionChariotKraft(?DimensionChariotKraft $dimensionChariotKraft): static
    {
        $this->dimensionChariotKraft = $dimensionChariotKraft;

        return $this;
    }

    public function getDimensionPapierCale(): ?DimensionPapierCale
    {
        return $this->dimensionPapierCale;
    }

    public function setDimensionPapierCale(?DimensionPapierCale $dimensionPapierCale): static
    {
        $this->dimensionPapierCale = $dimensionPapierCale;

        return $this;
    }

    public function getFiltreCone(): ?FiltreCone
    {
        return $this->filtreCone;
    }

    public function setFiltreCone(?FiltreCone $filtreCone): static
    {
        $this->filtreCone = $filtreCone;

        return $this;
    }

    public function getDimensionMicroBruch(): ?DimensionMicroBruch
    {
        return $this->dimensionMicroBruch;
    }

    public function setDimensionMicroBruch(?DimensionMicroBruch $dimensionMicroBruch): static
    {
        $this->dimensionMicroBruch = $dimensionMicroBruch;

        return $this;
    }

    public function getPinceauDimension(): ?PinceauDimension
    {
        return $this->pinceauDimension;
    }

    public function setPinceauDimension(?PinceauDimension $pinceauDimension): static
    {
        $this->pinceauDimension = $pinceauDimension;

        return $this;
    }

    public function getRegleDosage(): ?RegleDosage
    {
        return $this->regleDosage;
    }

    public function setRegleDosage(?RegleDosage $regleDosage): static
    {
        $this->regleDosage = $regleDosage;

        return $this;
    }

    public function getDimensionKraft(): ?DimensionKraft
    {
        return $this->dimensionKraft;
    }

    public function setDimensionKraft(?DimensionKraft $dimensionKraft): static
    {
        $this->dimensionKraft = $dimensionKraft;

        return $this;
    }

    public function getDimensionTireau(): ?DimensionTireau
    {
        return $this->dimensionTireau;
    }

    public function setDimensionTireau(?DimensionTireau $dimensionTireau): static
    {
        $this->dimensionTireau = $dimensionTireau;

        return $this;
    }

    public function getTailleFineTape(): ?TailleFineTape
    {
        return $this->tailleFineTape;
    }

    public function setTailleFineTape(?TailleFineTape $tailleFineTape): static
    {
        $this->tailleFineTape = $tailleFineTape;

        return $this;
    }

    public function getEffet(): ?Effet
    {
        return $this->effet;
    }

    public function setEffet(?Effet $effet): static
    {
        $this->effet = $effet;

        return $this;
    }

    public function getAspirateur(): ?Aspirateur
    {
        return $this->aspirateur;
    }

    public function setAspirateur(?Aspirateur $aspirateur): static
    {
        $this->aspirateur = $aspirateur;

        return $this;
    }

    public function getPonceuse(): ?Ponceuse
    {
        return $this->ponceuse;
    }

    public function setPonceuse(?Ponceuse $ponceuse): static
    {
        $this->ponceuse = $ponceuse;

        return $this;
    }

    public function getTypePistole(): ?TypePistole
    {
        return $this->typePistole;
    }

    public function setTypePistole(?TypePistole $typePistole): static
    {
        $this->typePistole = $typePistole;

        return $this;
    }

    public function getSolPlafond(): ?SolPlafond
    {
        return $this->solPlafond;
    }

    public function setSolPlafond(?SolPlafond $solPlafond): static
    {
        $this->solPlafond = $solPlafond;

        return $this;
    }

    public function getAcierAlu(): ?AcierAlu
    {
        return $this->acierAlu;
    }

    public function setAcierAlu(?AcierAlu $acierAlu): static
    {
        $this->acierAlu = $acierAlu;

        return $this;
    }

    public function getTaille(): ?Taille
    {
        return $this->taille;
    }

    public function setTaille(?Taille $taille): static
    {
        $this->taille = $taille;

        return $this;
    }

    public function getDimensionAll(): ?DimensionAll
    {
        return $this->dimensionAll;
    }

    public function setDimensionAll(?DimensionAll $dimensionAll): static
    {
        $this->dimensionAll = $dimensionAll;

        return $this;
    }

    public function getMatiere(): ?Matiere
    {
        return $this->matiere;
    }

    public function setMatiere(?Matiere $matiere): static
    {
        $this->matiere = $matiere;

        return $this;
    }

    public function getMasquage(): ?Masquage
    {
        return $this->masquage;
    }

    public function setMasquage(?Masquage $masquage): static
    {
        $this->masquage = $masquage;

        return $this;
    }

    public function getNettoyage(): ?Nettoyage
    {
        return $this->nettoyage;
    }

    public function setNettoyage(?Nettoyage $nettoyage): static
    {
        $this->nettoyage = $nettoyage;

        return $this;
    }

    public function getDurcisseur(): ?Durcisseur
    {
        return $this->durcisseur;
    }

    public function setDurcisseur(?Durcisseur $durcisseur): static
    {
        $this->durcisseur = $durcisseur;

        return $this;
    }

    public function getAccessoireAspirateur(): ?AccessoireAspirateur
    {
        return $this->accessoireAspirateur;
    }

    public function setAccessoireAspirateur(?AccessoireAspirateur $accessoireAspirateur): static
    {
        $this->accessoireAspirateur = $accessoireAspirateur;

        return $this;
    }

    public function getVitrage(): ?Vitrage
    {
        return $this->vitrage;
    }

    public function setVitrage(?Vitrage $vitrage): static
    {
        $this->vitrage = $vitrage;

        return $this;
    }

    public function getTypeRaccord(): ?TypeRaccord
    {
        return $this->typeRaccord;
    }

    public function setTypeRaccord(?TypeRaccord $typeRaccord): static
    {
        $this->typeRaccord = $typeRaccord;

        return $this;
    }

    public function getTypeMateriel(): ?TypeMateriel
    {
        return $this->typeMateriel;
    }

    public function setTypeMateriel(?TypeMateriel $typeMateriel): static
    {
        $this->typeMateriel = $typeMateriel;

        return $this;
    }

    public function getMateriel(): ?Materiel
    {
        return $this->materiel;
    }

    public function setMateriel(?Materiel $materiel): static
    {
        $this->materiel = $materiel;

        return $this;
    }

    public function getColleJoint(): ?ColleJoint
    {
        return $this->colleJoint;
    }

    public function setColleJoint(?ColleJoint $colleJoint): static
    {
        $this->colleJoint = $colleJoint;

        return $this;
    }

    public function getAdhesif(): ?Adhesif
    {
        return $this->adhesif;
    }

    public function setAdhesif(?Adhesif $adhesif): static
    {
        $this->adhesif = $adhesif;

        return $this;
    }

    public function getTypePonceuse(): ?TypePonceuse
    {
        return $this->typePonceuse;
    }

    public function setTypePonceuse(?TypePonceuse $typePonceuse): static
    {
        $this->typePonceuse = $typePonceuse;

        return $this;
    }

    public function getTypeMastic(): ?TypeMastic
    {
        return $this->typeMastic;
    }

    public function setTypeMastic(?TypeMastic $typeMastic): static
    {
        $this->typeMastic = $typeMastic;

        return $this;
    }

    public function getTypeDiluant(): ?TypeDiluant
    {
        return $this->typeDiluant;
    }

    public function setTypeDiluant(?TypeDiluant $typeDiluant): static
    {
        $this->typeDiluant = $typeDiluant;

        return $this;
    }

    public function getAccessoire(): ?Accessoire
    {
        return $this->accessoire;
    }

    public function setAccessoire(?Accessoire $accessoire): static
    {
        $this->accessoire = $accessoire;

        return $this;
    }

    public function getAccessoirePolisseuse(): ?AccessoirePolisseuse
    {
        return $this->accessoirePolisseuse;
    }

    public function setAccessoirePolisseuse(?AccessoirePolisseuse $accessoirePolisseuse): static
    {
        $this->accessoirePolisseuse = $accessoirePolisseuse;

        return $this;
    }

    public function getDebosseleurMarteau(): ?DebosseleurMarteau
    {
        return $this->debosseleurMarteau;
    }

    public function setDebosseleurMarteau(?DebosseleurMarteau $debosseleurMarteau): static
    {
        $this->debosseleurMarteau = $debosseleurMarteau;

        return $this;
    }

    public function getVerinPompeHydrau(): ?VerinPompeHydrau
    {
        return $this->verinPompeHydrau;
    }

    public function setVerinPompeHydrau(?VerinPompeHydrau $verinPompeHydrau): static
    {
        $this->verinPompeHydrau = $verinPompeHydrau;

        return $this;
    }

    public function getEquerreTirageGriffe(): ?EquerreTirageGriffe
    {
        return $this->equerreTirageGriffe;
    }

    public function setEquerreTirageGriffe(?EquerreTirageGriffe $equerreTirageGriffe): static
    {
        $this->equerreTirageGriffe = $equerreTirageGriffe;

        return $this;
    }

    public function getChaineRaccord(): ?ChaineRaccord
    {
        return $this->chaineRaccord;
    }

    public function setChaineRaccord(?ChaineRaccord $chaineRaccord): static
    {
        $this->chaineRaccord = $chaineRaccord;

        return $this;
    }

    public function getDoumdoum(): ?Doumdoum
    {
        return $this->doumdoum;
    }

    public function setDoumdoum(?Doumdoum $doumdoum): static
    {
        $this->doumdoum = $doumdoum;

        return $this;
    }

    public function getBarreDebosslage(): ?BarreDebosslage
    {
        return $this->barreDebosslage;
    }

    public function setBarreDebosslage(?BarreDebosslage $barreDebosslage): static
    {
        $this->barreDebosslage = $barreDebosslage;

        return $this;
    }

    public function getPince(): ?Pince
    {
        return $this->pince;
    }

    public function setPince(?Pince $pince): static
    {
        $this->pince = $pince;

        return $this;
    }

    public function getCuilliere(): ?Cuilliere
    {
        return $this->cuilliere;
    }

    public function setCuilliere(?Cuilliere $cuilliere): static
    {
        $this->cuilliere = $cuilliere;

        return $this;
    }

    public function getMarteau(): ?Marteau
    {
        return $this->marteau;
    }

    public function setMarteau(?Marteau $marteau): static
    {
        $this->marteau = $marteau;

        return $this;
    }

    public function getTas(): ?Tas
    {
        return $this->tas;
    }

    public function setTas(?Tas $tas): static
    {
        $this->tas = $tas;

        return $this;
    }

    public function getMasseMagnetiqueChario(): ?MasseMagnetiqueChario
    {
        return $this->masseMagnetiqueChario;
    }

    public function setMasseMagnetiqueChario(?MasseMagnetiqueChario $masseMagnetiqueChario): static
    {
        $this->masseMagnetiqueChario = $masseMagnetiqueChario;

        return $this;
    }

    public function getConsomable(): ?Consomable
    {
        return $this->consomable;
    }

    public function setConsomable(?Consomable $consomable): static
    {
        $this->consomable = $consomable;

        return $this;
    }

    public function isInLabo(): ?bool
    {
        return $this->inLabo;
    }

    public function setInLabo(bool $inLabo): static
    {
        $this->inLabo = $inLabo;

        return $this;
    }

    /**
     * @return Collection<int, Panier>
     */
    public function getPaniers(): Collection
    {
        return $this->paniers;
    }

    public function addPanier(Panier $panier): static
    {
        if (!$this->paniers->contains($panier)) {
            $this->paniers->add($panier);
            $panier->setProduit($this);
        }

        return $this;
    }

    public function removePanier(Panier $panier): static
    {
        if ($this->paniers->removeElement($panier)) {
            // set the owning side to null (unless already changed)
            if ($panier->getProduit() === $this) {
                $panier->setProduit(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, QtyCarton>
     */
    public function getQtyCartons(): Collection
    {
        return $this->qtyCartons;
    }

    public function addQtyCarton(QtyCarton $qtyCarton): static
    {
        if (!$this->qtyCartons->contains($qtyCarton)) {
            $this->qtyCartons->add($qtyCarton);
            $qtyCarton->setObject($this);
        }

        return $this;
    }

    public function removeQtyCarton(QtyCarton $qtyCarton): static
    {
        if ($this->qtyCartons->removeElement($qtyCarton)) {
            // set the owning side to null (unless already changed)
            if ($qtyCarton->getObject() === $this) {
                $qtyCarton->setObject(null);
            }
        }

        return $this;
    }

    public function getCodeBarre(): ?CodeBarre
    {
        return $this->codeBarre;
    }

    public function setCodeBarre(?CodeBarre $codeBarre): static
    {
        // unset the owning side of the relation if necessary
        if ($codeBarre === null && $this->codeBarre !== null) {
            $this->codeBarre->setProduit(null);
        }

        // set the owning side of the relation if necessary
        if ($codeBarre !== null && $codeBarre->getProduit() !== $this) {
            $codeBarre->setProduit($this);
        }

        $this->codeBarre = $codeBarre;

        return $this;
    }

    public function getPvc(): ?float
    {
        return $this->pvc;
    }

    public function setPvc(?float $pvc): static
    {
        $this->pvc = $pvc;

        return $this;
    }

}
