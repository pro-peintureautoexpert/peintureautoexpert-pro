<?php

namespace App\Entity;

use App\Repository\QtyCartonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
#[ORM\Entity(repositoryClass: QtyCartonRepository::class)]
class QtyCarton
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $qty = null;

    #[ORM\Column]
    private ?bool $active = null;

    #[ORM\Column]
    private ?float $price = null;

    #[ORM\ManyToOne(inversedBy: 'qtyCartons')]
    private ?Product $object = null;

    #[ORM\Column]
    private ?bool $inPalette = null;

    #[ORM\Column(nullable: true)]
    private ?int $OrderPalette = null;

    #[ORM\Column(nullable: true)]
    private ?int $OrderQte = null;

    public function __construct()
    {
        $this->produit = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQty(): ?int
    {
        return $this->qty;
    }

    public function setQty(int $qty): static
    {
        $this->qty = $qty;

        return $this;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): static
    {
        $this->active = $active;

        return $this;
    }
    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): static
    {
        $this->price = $price;

        return $this;
    }

    public function getObject(): ?Product
    {
        return $this->object;
    }

    public function setObject(?Product $object): static
    {
        $this->object = $object;

        return $this;
    }

    public function isInPalette(): ?bool
    {
        return $this->inPalette;
    }

    public function setInPalette(bool $inPalette): static
    {
        $this->inPalette = $inPalette;

        return $this;
    }

    public function getPriceQte()
    {
        return $this->price / 100;
    }

    public function getOrderPalette(): ?int
    {
        return $this->OrderPalette;
    }

    public function setOrderPalette(?int $OrderPalette): static
    {
        $this->OrderPalette = $OrderPalette;

        return $this;
    }

    public function getOrderQte(): ?int
    {
        return $this->OrderQte;
    }

    public function setOrderQte(?int $OrderQte): static
    {
        $this->OrderQte = $OrderQte;

        return $this;
    }
}
