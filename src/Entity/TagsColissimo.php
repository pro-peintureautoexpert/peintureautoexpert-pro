<?php

namespace App\Entity;

use App\Repository\TagsColissimoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TagsColissimoRepository::class)]
class TagsColissimo
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    /**
     * @var Collection<int, Order>
     */
    #[ORM\OneToMany(targetEntity: Order::class, mappedBy: 'tagsColissimo')]
    private Collection $ticket;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $retour = null;

    public function __construct()
    {
        $this->ticket = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Order>
     */
    public function getTicket(): Collection
    {
        return $this->ticket;
    }

    public function addTicket(Order $ticket): static
    {
        if (!$this->ticket->contains($ticket)) {
            $this->ticket->add($ticket);
            $ticket->setTagsColissimo($this);
        }

        return $this;
    }

    public function removeTicket(Order $ticket): static
    {
        if ($this->ticket->removeElement($ticket)) {
            // set the owning side to null (unless already changed)
            if ($ticket->getTagsColissimo() === $this) {
                $ticket->setTagsColissimo(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getRetour(): ?string
    {
        return $this->retour;
    }

    public function setRetour(string $retour): static
    {
        $this->retour = $retour;

        return $this;
    }
}
