<?php

namespace App\Entity;

use App\Repository\TechniqueProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TechniqueProductRepository::class)]
class TechniqueProduct
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToMany(targetEntity: Product::class, inversedBy: 'techniqueProducts')]
    private Collection $product;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $ficheTechnique = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $ficheSecurite = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $iconOne = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $iconTwo = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $descriptionTech = null;

    #[ORM\ManyToMany(targetEntity: Image::class)]
    private Collection $image;

    public function __construct()
    {
        $this->product = new ArrayCollection();
        $this->image = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Product>
     */
    public function getProduct(): Collection
    {
        return $this->product;
    }

    public function addProduct(Product $product): static
    {
        if (!$this->product->contains($product)) {
            $this->product->add($product);
        }

        return $this;
    }

    public function removeProduct(Product $product): static
    {
        $this->product->removeElement($product);

        return $this;
    }

    public function getFicheTechnique(): ?string
    {
        return $this->ficheTechnique;
    }

    public function setFicheTechnique(?string $ficheTechnique): static
    {
        $this->ficheTechnique = $ficheTechnique;

        return $this;
    }

    public function getFicheSecurite(): ?string
    {
        return $this->ficheSecurite;
    }

    public function setFicheSecurite(?string $ficheSecurite): static
    {
        $this->ficheSecurite = $ficheSecurite;

        return $this;
    }

    public function getIconOne(): ?string
    {
        return $this->iconOne;
    }

    public function setIconOne(?string $iconOne): static
    {
        $this->iconOne = $iconOne;

        return $this;
    }

    public function getIconTwo(): ?string
    {
        return $this->iconTwo;
    }

    public function setIconTwo(?string $iconTwo): static
    {
        $this->iconTwo = $iconTwo;

        return $this;
    }

    public function getDescriptionTech(): ?string
    {
        return $this->descriptionTech;
    }

    public function setDescriptionTech(?string $descriptionTech): static
    {
        $this->descriptionTech = $descriptionTech;

        return $this;
    }

    /**
     * @return Collection<int, Image>
     */
    public function getImage(): Collection
    {
        return $this->image;
    }

    public function addImage(Image $image): static
    {
        if (!$this->image->contains($image)) {
            $this->image->add($image);
        }

        return $this;
    }

    public function removeImage(Image $image): static
    {
        $this->image->removeElement($image);

        return $this;
    }
}
