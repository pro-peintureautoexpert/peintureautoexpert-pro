<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CategoryRepository::class)]
class Category
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\ManyToOne]
    private ?Category $parentId = null;

    #[ORM\OneToMany(targetEntity: Product::class, mappedBy: 'Category')]
    private Collection $products;

    #[ORM\Column(length: 255)]
    private ?string $illustration = null;

    #[ORM\Column(length: 255)]
    private ?string $slug = null;

    #[ORM\Column(length: 255)]
    private ?string $icon = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $illustrationTwo = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $description = null;

    #[ORM\ManyToMany(targetEntity: Filters::class, inversedBy: 'categories')]
    private Collection $filters;

    #[ORM\OneToMany(targetEntity: ProductParent::class, mappedBy: 'category')]
    private Collection $productParents;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->filters = new ArrayCollection();
        $this->productParents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getParentId(): ?Category
    {
        return $this->parentId;
    }

    public function setParentId(?Category $parentId): static
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * @return Collection<int, Product>
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): static
    {
        if (!$this->products->contains($product)) {
            $this->products->add($product);
            $product->setCategory($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): static
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getCategory() === $this) {
                $product->setCategory(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function getIllustration(): ?string
    {
        return $this->illustration;
    }

    public function setIllustration(string $illustration): static
    {
        $this->illustration = $illustration;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(string $icon): static
    {
        $this->icon = $icon;

        return $this;
    }

    public function getIllustrationTwo(): ?string
    {
        return $this->illustrationTwo;
    }

    public function setIllustrationTwo(?string $illustrationTwo): static
    {
        $this->illustrationTwo = $illustrationTwo;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, Filters>
     */
    public function getFilters(): Collection
    {
        return $this->filters;
    }

    public function addFilter(Filters $filter): static
    {
        if (!$this->filters->contains($filter)) {
            $this->filters->add($filter);
        }

        return $this;
    }

    public function removeFilter(Filters $filter): static
    {
        $this->filters->removeElement($filter);

        return $this;
    }

    /**
     * @return Collection<int, ProductParent>
     */
    public function getProductParents(): Collection
    {
        return $this->productParents;
    }

    public function addProductParent(ProductParent $productParent): static
    {
        if (!$this->productParents->contains($productParent)) {
            $this->productParents->add($productParent);
            $productParent->setCategory($this);
        }

        return $this;
    }

    public function removeProductParent(ProductParent $productParent): static
    {
        if ($this->productParents->removeElement($productParent)) {
            // set the owning side to null (unless already changed)
            if ($productParent->getCategory() === $this) {
                $productParent->setCategory(null);
            }
        }

        return $this;
    }
}
