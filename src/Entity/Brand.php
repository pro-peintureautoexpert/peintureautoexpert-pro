<?php

namespace App\Entity;

use App\Repository\BrandRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BrandRepository::class)]
class Brand
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $illustration = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $cataloguePdf = null;

    #[ORM\OneToMany(targetEntity: Catalogue::class, mappedBy: 'brand')]
    private Collection $catalogue;

    #[ORM\OneToMany(targetEntity: ProductParent::class, mappedBy: 'brand')]
    private Collection $productParents;

    public function __construct()
    {
        $this->catalogue = new ArrayCollection();
        $this->productParents = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getIllustration(): ?string
    {
        return $this->illustration;
    }

    public function setIllustration(string $illustration): static
    {
        $this->illustration = $illustration;

        return $this;
    }

    public function getCataloguePdf(): ?string
    {
        return $this->cataloguePdf;
    }

    public function setCataloguePdf(?string $cataloguePdf): static
    {
        $this->cataloguePdf = $cataloguePdf;

        return $this;
    }

    /**
     * @return Collection<int, Catalogue>
     */
    public function getCatalogue(): Collection
    {
        return $this->catalogue;
    }

    public function addCatalogue(Catalogue $catalogue): static
    {
        if (!$this->catalogue->contains($catalogue)) {
            $this->catalogue->add($catalogue);
            $catalogue->setBrand($this);
        }

        return $this;
    }

    public function removeCatalogue(Catalogue $catalogue): static
    {
        if ($this->catalogue->removeElement($catalogue)) {
            // set the owning side to null (unless already changed)
            if ($catalogue->getBrand() === $this) {
                $catalogue->setBrand(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ProductParent>
     */
    public function getProductParents(): Collection
    {
        return $this->productParents;
    }

    public function addProductParent(ProductParent $productParent): static
    {
        if (!$this->productParents->contains($productParent)) {
            $this->productParents->add($productParent);
            $productParent->setBrand($this);
        }

        return $this;
    }

    public function removeProductParent(ProductParent $productParent): static
    {
        if ($this->productParents->removeElement($productParent)) {
            // set the owning side to null (unless already changed)
            if ($productParent->getBrand() === $this) {
                $productParent->setBrand(null);
            }
        }

        return $this;
    }
}
