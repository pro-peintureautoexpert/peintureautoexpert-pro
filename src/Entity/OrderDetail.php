<?php

namespace App\Entity;

use App\Repository\OrderDetailRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OrderDetailRepository::class)]
class OrderDetail
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'orderDetails')]
    private ?Order $myOrder = null;

    #[ORM\Column(length: 255)]
    private ?string $prodcutName = null;

    #[ORM\Column(length: 255)]
    private ?string $productIllustartion = null;

    #[ORM\Column]
    private ?int $ProductQuantity = null;

    #[ORM\Column]
    private ?float $productPrice = null;

    #[ORM\Column]
    private ?float $productTva = null;

    #[ORM\Column(length: 255)]
    private ?string $codeArticle = null;

    #[ORM\Column(length: 255)]
    private ?string $category = null;

    #[ORM\Column(length: 255)]
    private ?string $slugProduct = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProductPriceWt()
    {
        $coeff = 1 + ($this->productTva / 100);

        return $coeff * $this->productPrice;
    }

    public function getMyOrder(): ?Order
    {
        return $this->myOrder;
    }

    public function setMyOrder(?Order $myOrder): static
    {
        $this->myOrder = $myOrder;

        return $this;
    }

    public function getProdcutName(): ?string
    {
        return $this->prodcutName;
    }

    public function setProdcutName(string $prodcutName): static
    {
        $this->prodcutName = $prodcutName;

        return $this;
    }

    public function getProductIllustartion(): ?string
    {
        return $this->productIllustartion;
    }

    public function setProductIllustartion(string $productIllustartion): static
    {
        $this->productIllustartion = $productIllustartion;

        return $this;
    }

    public function getProductQuantity(): ?int
    {
        return $this->ProductQuantity;
    }

    public function setProductQuantity(int $ProductQuantity): static
    {
        $this->ProductQuantity = $ProductQuantity;

        return $this;
    }

    public function getProductPrice(): ?float
    {
        return $this->productPrice;
    }

    public function setProductPrice(float $productPrice): static
    {
        $this->productPrice = $productPrice;

        return $this;
    }

    public function getProductTva(): ?float
    {
        return $this->productTva;
    }

    public function setProductTva(float $productTva): static
    {
        $this->productTva = $productTva;

        return $this;
    }

    public function getCodeArticle(): ?string
    {
        return $this->codeArticle;
    }

    public function setCodeArticle(string $codeArticle): static
    {
        $this->codeArticle = $codeArticle;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): static
    {
        $this->category = $category;

        return $this;
    }

    public function getSlugProduct(): ?string
    {
        return $this->slugProduct;
    }

    public function setSlugProduct(string $slugProduct): static
    {
        $this->slugProduct = $slugProduct;

        return $this;
    }
}
