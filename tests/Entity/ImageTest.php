<?php

namespace App\Tests\Entity;

use App\Entity\Image;
use App\Entity\Product;
use PHPUnit\Framework\TestCase;

class ImageTest extends TestCase
{
    public function testId()
    {
        $image = new Image();
        $this->assertNull($image->getId());

        $id = 1;
        $reflection = new \ReflectionClass($image);
        $idProperty = $reflection->getProperty('id');
        $idProperty->setAccessible(true);
        $idProperty->setValue($image, $id);

        $this->assertEquals($id, $image->getId());
    }

    public function testUrlImage()
    {
        $image = new Image();
        $url = 'https://example.com/image.jpg';
        $image->setUrlImage($url);

        $this->assertEquals($url, $image->getUrlImage());
    }

    public function testProducts()
    {
        $image = new Image();
        $product1 = new Product();
        $product2 = new Product();

        $this->assertCount(0, $image->getProducts());

        $image->addProduct($product1);
        $this->assertCount(1, $image->getProducts());
        $this->assertTrue($image->getProducts()->contains($product1));

        $image->addProduct($product2);
        $this->assertCount(2, $image->getProducts());
        $this->assertTrue($image->getProducts()->contains($product2));

        $image->removeProduct($product1);
        $this->assertCount(1, $image->getProducts());
        $this->assertFalse($image->getProducts()->contains($product1));

        $image->removeProduct($product2);
        $this->assertCount(0, $image->getProducts());
        $this->assertFalse($image->getProducts()->contains($product2));
    }
}
