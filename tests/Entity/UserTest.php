<?php

namespace App\Tests\Entity;

use App\Entity\Address;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testId()
    {
        $user = new User();
        $this->assertNull($user->getId());

        $id = 1;
        $reflection = new \ReflectionClass($user);
        $idProperty = $reflection->getProperty('id');
        $idProperty->setAccessible(true);
        $idProperty->setValue($user, $id);

        $this->assertEquals($id, $user->getId());
    }

    public function testUuid()
    {
        $user = new User();
        $uuid = '123e4567-e89b-12d3-a456-426614174000';

        $user->setUuid($uuid);
        $this->assertEquals($uuid, $user->getUuid());
    }

    public function testRoles()
    {
        $user = new User();
        $roles = ['ROLE_ADMIN', 'ROLE_USER'];

        $user->setRoles($roles);
        $this->assertEquals($roles, $user->getRoles());
    }

    public function testPassword()
    {
        $user = new User();
        $password = 'test_password';

        $user->setPassword($password);
        $this->assertEquals($password, $user->getPassword());
    }

    public function testEmail()
    {
        $user = new User();
        $email = 'test@example.com';

        $user->setEmail($email);
        $this->assertEquals($email, $user->getEmail());
    }

    public function testFirstName()
    {
        $user = new User();
        $firstName = 'John';

        $user->setFirstName($firstName);
        $this->assertEquals($firstName, $user->getFirstName());
    }

    public function testLastName()
    {
        $user = new User();
        $lastName = 'Doe';

        $user->setLastName($lastName);
        $this->assertEquals($lastName, $user->getLastName());
    }

    public function testAddresses()
    {
        $user = new User();
        $address1 = new Address();
        $address2 = new Address();

        $this->assertCount(0, $user->getAddresses());

        $user->addAddress($address1);
        $this->assertCount(1, $user->getAddresses());
        $this->assertTrue($user->getAddresses()->contains($address1));

        $user->addAddress($address2);
        $this->assertCount(2, $user->getAddresses());
        $this->assertTrue($user->getAddresses()->contains($address2));

        $user->removeAddress($address1);
        $this->assertCount(1, $user->getAddresses());
        $this->assertFalse($user->getAddresses()->contains($address1));

        $user->removeAddress($address2);
        $this->assertCount(0, $user->getAddresses());
        $this->assertFalse($user->getAddresses()->contains($address2));
    }
}
