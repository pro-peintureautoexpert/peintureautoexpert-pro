<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Entity\Image;
use App\Entity\Product;
use App\Entity\Promotions;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    public function testId()
    {
        $product = new Product();
        $this->assertNull($product->getId());

        $id = 1;
        $reflection = new \ReflectionClass($product);
        $idProperty = $reflection->getProperty('id');
        $idProperty->setAccessible(true);
        $idProperty->setValue($product, $id);

        $this->assertEquals($id, $product->getId());
    }

    public function testFields()
    {
        $product = new Product();
        $articleCode = 'P12345';
        $name = 'Sample Product';
        $slug = 'sample-product';
        $description = 'Lorem ipsum dolor sit amet';
        $weight = 1.5;
        $illustration = 'sample.jpg';
        $priceUnitaire = 10.99;
        $pricePalette = 99.99;
        $stock = 100;
        $category = new Category();

        $product->setArticleCode($articleCode)
            ->setName($name)
            ->setSlug($slug)
       
            ->setDescription($description)
            ->setWeight($weight)
            ->setIllustration($illustration)
            ->setPriceUnitaire($priceUnitaire)
            ->setPricePalette($pricePalette)
            ->setStock($stock)
            ->setCategory($category);

        $this->assertEquals($articleCode, $product->getArticleCode());
        $this->assertEquals($name, $product->getName());
        $this->assertEquals($slug, $product->getSlug());
        $this->assertEquals($description, $product->getDescription());
        $this->assertEquals($weight, $product->getWeight());
        $this->assertEquals($illustration, $product->getIllustration());
        $this->assertEquals($priceUnitaire, $product->getPriceUnitaire());
        $this->assertEquals($pricePalette, $product->getPricePalette());
        $this->assertEquals($stock, $product->getStock());
        $this->assertInstanceOf(Category::class, $product->getCategory());
        $this->assertEquals($category, $product->getCategory());
    }

    public function testPromotions()
    {
        $product = new Product();
        $promotion1 = new Promotions();
        $promotion2 = new Promotions();

        $this->assertCount(0, $product->getPromotions());

        $product->addPromotion($promotion1);
        $this->assertCount(1, $product->getPromotions());
        $this->assertTrue($product->getPromotions()->contains($promotion1));

        $product->addPromotion($promotion2);
        $this->assertCount(2, $product->getPromotions());
        $this->assertTrue($product->getPromotions()->contains($promotion2));

        $product->removePromotion($promotion1);
        $this->assertCount(1, $product->getPromotions());
        $this->assertFalse($product->getPromotions()->contains($promotion1));

        $product->removePromotion($promotion2);
        $this->assertCount(0, $product->getPromotions());
        $this->assertFalse($product->getPromotions()->contains($promotion2));
    }

    public function testImages()
    {
        $product = new Product();
        $image1 = new Image();
        $image2 = new Image();

        $this->assertCount(0, $product->getImages());

        $product->addImage($image1);
        $this->assertCount(1, $product->getImages());
        $this->assertTrue($product->getImages()->contains($image1));

        $product->addImage($image2);
        $this->assertCount(2, $product->getImages());
        $this->assertTrue($product->getImages()->contains($image2));

        $product->removeImage($image1);
        $this->assertCount(1, $product->getImages());
        $this->assertFalse($product->getImages()->contains($image1));

        $product->removeImage($image2);
        $this->assertCount(0, $product->getImages());
        $this->assertFalse($product->getImages()->contains($image2));
    }
}
