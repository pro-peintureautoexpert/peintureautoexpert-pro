<?php

namespace App\Tests\Entity;

use App\Entity\Panier;
use App\Entity\Product;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class PanierTest extends TestCase
{
    public function testPanierProperties(): void
    {
        // Création des entités fictives User et Product
        $user = new User();
        $product = new Product();

        // Création d'une instance de Panier
        $panier = new Panier();

        // Test des propriétés
        $panier->setUser($user);
        $panier->setProduit($product);
        $panier->setQte(5);

        // Assertions
        $this->assertSame($user, $panier->getUser(), 'L\'utilisateur du panier doit correspondre');
        $this->assertSame($product, $panier->getProduit(), 'Le produit du panier doit correspondre');
        $this->assertSame(5, $panier->getQte(), 'La quantité du panier doit être correcte');
    }

    public function testDefaultValues(): void
    {
        // Création d'une instance de Panier
        $panier = new Panier();

        // Test des valeurs par défaut
        $this->assertNull($panier->getId(), 'L\'ID du panier doit être null par défaut');
        $this->assertNull($panier->getUser(), 'L\'utilisateur du panier doit être null par défaut');
        $this->assertNull($panier->getProduit(), 'Le produit du panier doit être null par défaut');
        $this->assertNull($panier->getQte(), 'La quantité du panier doit être null par défaut');
    }

    public function testChainingMethods(): void
    {
        // Création des entités fictives User et Product
        $user = new User();
        $product = new Product();

        // Création d'une instance de Panier et test du chaînage
        $panier = (new Panier())
            ->setUser($user)
            ->setProduit($product)
            ->setQte(3);

        // Assertions
        $this->assertSame($user, $panier->getUser(), 'L\'utilisateur du panier doit correspondre après un chaînage');
        $this->assertSame($product, $panier->getProduit(), 'Le produit du panier doit correspondre après un chaînage');
        $this->assertSame(3, $panier->getQte(), 'La quantité doit correspondre après un chaînage');
    }
}
