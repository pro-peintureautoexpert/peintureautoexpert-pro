<?php

namespace App\Tests\Entity;

use App\Entity\Address;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class AddressTest extends TestCase
{
    public function testId()
    {
        $address = new Address();
        $this->assertNull($address->getId());

        $id = 1;
        $reflection = new \ReflectionClass($address);
        $idProperty = $reflection->getProperty('id');
        $idProperty->setAccessible(true);
        $idProperty->setValue($address, $id);

        $this->assertEquals($id, $address->getId());
    }

    public function testUser()
    {
        $address = new Address();
        $user = new User();

        $this->assertNull($address->getUser());
        
        $address->setUser($user);
        $this->assertInstanceOf(User::class, $address->getUser());
        $this->assertEquals($user, $address->getUser());
    }

    public function testFields()
    {
        $address = new Address();
        $firstName = 'John';
        $lastName = 'Doe';
        $company = 'Company Inc.';
        $addressValue = '123 Main St';
        $postal = '12345';
        $city = 'Anytown';
        $country = 'USA';
        $phone = '123-456-7890';

        $address->setFirstName($firstName)
            ->setLastName($lastName)
            ->setCompany($company)
            ->setAddress($addressValue)
            ->setPostal($postal)
            ->setCity($city)
            ->setCountry($country)
            ->setPhone($phone);

        $this->assertEquals($firstName, $address->getFirstName());
        $this->assertEquals($lastName, $address->getLastName());
        $this->assertEquals($company, $address->getCompany());
        $this->assertEquals($addressValue, $address->getAddress());
        $this->assertEquals($postal, $address->getPostal());
        $this->assertEquals($city, $address->getCity());
        $this->assertEquals($country, $address->getCountry());
        $this->assertEquals($phone, $address->getPhone());
    }
}
