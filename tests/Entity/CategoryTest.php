<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Entity\Product;
use PHPUnit\Framework\TestCase;

class CategoryTest extends TestCase
{
    public function testId()
    {
        $category = new Category();
        $this->assertNull($category->getId());

        $id = 1;
        $reflection = new \ReflectionClass($category);
        $idProperty = $reflection->getProperty('id');
        $idProperty->setAccessible(true);
        $idProperty->setValue($category, $id);

        $this->assertEquals($id, $category->getId());
    }

    public function testName()
    {
        $category = new Category();
        $name = 'Test Category';

        $category->setName($name);
        $this->assertEquals($name, $category->getName());
    }

    public function testParentId()
    {
        $category = new Category();
        $parentCategory = new Category();

        $this->assertNull($category->getParentId());

        $category->setParentId($parentCategory);
        $this->assertInstanceOf(Category::class, $category->getParentId());
        $this->assertEquals($parentCategory, $category->getParentId());
    }

    public function testProducts()
    {
        $category = new Category();
        $product1 = new Product();
        $product2 = new Product();

        $this->assertCount(0, $category->getProducts());

        $category->addProduct($product1);
        $this->assertCount(1, $category->getProducts());
        $this->assertTrue($category->getProducts()->contains($product1));

        $category->addProduct($product2);
        $this->assertCount(2, $category->getProducts());
        $this->assertTrue($category->getProducts()->contains($product2));

        $category->removeProduct($product1);
        $this->assertCount(1, $category->getProducts());
        $this->assertFalse($category->getProducts()->contains($product1));

        $category->removeProduct($product2);
        $this->assertCount(0, $category->getProducts());
        $this->assertFalse($category->getProducts()->contains($product2));
    }
}
