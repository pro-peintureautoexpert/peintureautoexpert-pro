<?php

namespace App\Tests\Entity;

use App\Entity\Product;
use App\Entity\Promotions;
use PHPUnit\Framework\TestCase;

class PromotionsTest extends TestCase
{
    public function testId()
    {
        $promotion = new Promotions();
        $this->assertNull($promotion->getId());

        $id = 1;
        $reflection = new \ReflectionClass($promotion);
        $idProperty = $reflection->getProperty('id');
        $idProperty->setAccessible(true);
        $idProperty->setValue($promotion, $id);

        $this->assertEquals($id, $promotion->getId());
    }

    public function testReferencePromo()
    {
        $promotion = new Promotions();
        $reference = 'PROMO123';

        $promotion->setReferencePromo($reference);
        $this->assertEquals($reference, $promotion->getReferencePromo());
    }

    public function testDescription()
    {
        $promotion = new Promotions();
        $description = 'Sample promotion description';

        $promotion->setDescription($description);
        $this->assertEquals($description, $promotion->getDescription());
    }

    public function testIllustration()
    {
        $promotion = new Promotions();
        $illustration = 'promo_image.jpg';

        $promotion->setIllustration($illustration);
        $this->assertEquals($illustration, $promotion->getIllustration());
    }

    public function testDateStart()
    {
        $promotion = new Promotions();
        $dateStart = new \DateTime('2024-04-25');

        $promotion->setDateStart($dateStart);
        $this->assertEquals($dateStart, $promotion->getDateStart());
    }

    public function testDateEnd()
    {
        $promotion = new Promotions();
        $dateEnd = new \DateTime('2024-05-25');

        $promotion->setDateEnd($dateEnd);
        $this->assertEquals($dateEnd, $promotion->getDateEnd());
    }

    public function testProduit()
    {
        $promotion = new Promotions();
        $product1 = new Product();
        $product2 = new Product();

        $this->assertCount(0, $promotion->getProduit());

        $promotion->addProduit($product1);
        $this->assertCount(1, $promotion->getProduit());
        $this->assertTrue($promotion->getProduit()->contains($product1));

        $promotion->addProduit($product2);
        $this->assertCount(2, $promotion->getProduit());
        $this->assertTrue($promotion->getProduit()->contains($product2));

        $promotion->removeProduit($product1);
        $this->assertCount(1, $promotion->getProduit());
        $this->assertFalse($promotion->getProduit()->contains($product1));

        $promotion->removeProduit($product2);
        $this->assertCount(0, $promotion->getProduit());
        $this->assertFalse($promotion->getProduit()->contains($product2));
    }
}
