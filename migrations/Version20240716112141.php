<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240716112141 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE technique_product (id INT AUTO_INCREMENT NOT NULL, fiche_technique VARCHAR(255) DEFAULT NULL, fiche_securite VARCHAR(255) DEFAULT NULL, icon_one VARCHAR(255) DEFAULT NULL, icon_two VARCHAR(255) DEFAULT NULL, description_tech LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE technique_product_product (technique_product_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_A4F578E43C9B91A (technique_product_id), INDEX IDX_A4F578E4584665A (product_id), PRIMARY KEY(technique_product_id, product_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE technique_product_product ADD CONSTRAINT FK_A4F578E43C9B91A FOREIGN KEY (technique_product_id) REFERENCES technique_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE technique_product_product ADD CONSTRAINT FK_A4F578E4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE technique_product_product DROP FOREIGN KEY FK_A4F578E43C9B91A');
        $this->addSql('ALTER TABLE technique_product_product DROP FOREIGN KEY FK_A4F578E4584665A');
        $this->addSql('DROP TABLE technique_product');
        $this->addSql('DROP TABLE technique_product_product');
    }
}
