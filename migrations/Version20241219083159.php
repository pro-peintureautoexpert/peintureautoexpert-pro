<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241219083159 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE qty_carton (id INT AUTO_INCREMENT NOT NULL, qty INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product ADD qty_carton_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD495A07A4 FOREIGN KEY (qty_carton_id) REFERENCES qty_carton (id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD495A07A4 ON product (qty_carton_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD495A07A4');
        $this->addSql('DROP TABLE qty_carton');
        $this->addSql('DROP INDEX IDX_D34A04AD495A07A4 ON product');
        $this->addSql('ALTER TABLE product DROP qty_carton_id');
    }
}
