<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241220080800 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE qty_carton DROP FOREIGN KEY FK_C15FB7491C83F75');
        $this->addSql('DROP INDEX UNIQ_C15FB7491C83F75 ON qty_carton');
        $this->addSql('ALTER TABLE qty_carton CHANGE prod_id object_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE qty_carton ADD CONSTRAINT FK_C15FB749232D562B FOREIGN KEY (object_id) REFERENCES product (id)');
        $this->addSql('CREATE INDEX IDX_C15FB749232D562B ON qty_carton (object_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE qty_carton DROP FOREIGN KEY FK_C15FB749232D562B');
        $this->addSql('DROP INDEX IDX_C15FB749232D562B ON qty_carton');
        $this->addSql('ALTER TABLE qty_carton CHANGE object_id prod_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE qty_carton ADD CONSTRAINT FK_C15FB7491C83F75 FOREIGN KEY (prod_id) REFERENCES product (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C15FB7491C83F75 ON qty_carton (prod_id)');
    }
}
