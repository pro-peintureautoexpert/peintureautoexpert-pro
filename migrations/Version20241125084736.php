<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241125084736 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE accessoire (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE accessoire_aspirateur (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adhesif (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE colle_joint (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE durcisseur (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE masquage (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE materiel (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE nettoyage (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_diluant (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_mastic (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_materiel (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_ponceuse (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_raccord (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vitrage (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product ADD masquage_id INT DEFAULT NULL, ADD nettoyage_id INT DEFAULT NULL, ADD durcisseur_id INT DEFAULT NULL, ADD accessoire_aspirateur_id INT DEFAULT NULL, ADD vitrage_id INT DEFAULT NULL, ADD type_raccord_id INT DEFAULT NULL, ADD type_materiel_id INT DEFAULT NULL, ADD materiel_id INT DEFAULT NULL, ADD colle_joint_id INT DEFAULT NULL, ADD adhesif_id INT DEFAULT NULL, ADD type_ponceuse_id INT DEFAULT NULL, ADD type_mastic_id INT DEFAULT NULL, ADD type_diluant_id INT DEFAULT NULL, ADD accessoire_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADA424F8BE FOREIGN KEY (masquage_id) REFERENCES masquage (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADC4A1BF66 FOREIGN KEY (nettoyage_id) REFERENCES nettoyage (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADDFEBBAB6 FOREIGN KEY (durcisseur_id) REFERENCES durcisseur (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD461CFF51 FOREIGN KEY (accessoire_aspirateur_id) REFERENCES accessoire_aspirateur (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADC6B2988F FOREIGN KEY (vitrage_id) REFERENCES vitrage (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADF4E3FD67 FOREIGN KEY (type_raccord_id) REFERENCES type_raccord (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD5D91DD3E FOREIGN KEY (type_materiel_id) REFERENCES type_materiel (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD16880AAF FOREIGN KEY (materiel_id) REFERENCES materiel (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADAD468493 FOREIGN KEY (colle_joint_id) REFERENCES colle_joint (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD9879D172 FOREIGN KEY (adhesif_id) REFERENCES adhesif (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD9D7EE821 FOREIGN KEY (type_ponceuse_id) REFERENCES type_ponceuse (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD8A2047FA FOREIGN KEY (type_mastic_id) REFERENCES type_mastic (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD77A659E4 FOREIGN KEY (type_diluant_id) REFERENCES type_diluant (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADD23B67ED FOREIGN KEY (accessoire_id) REFERENCES accessoire (id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADA424F8BE ON product (masquage_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADC4A1BF66 ON product (nettoyage_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADDFEBBAB6 ON product (durcisseur_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD461CFF51 ON product (accessoire_aspirateur_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADC6B2988F ON product (vitrage_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADF4E3FD67 ON product (type_raccord_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD5D91DD3E ON product (type_materiel_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD16880AAF ON product (materiel_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADAD468493 ON product (colle_joint_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD9879D172 ON product (adhesif_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD9D7EE821 ON product (type_ponceuse_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD8A2047FA ON product (type_mastic_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD77A659E4 ON product (type_diluant_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADD23B67ED ON product (accessoire_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADD23B67ED');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD461CFF51');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD9879D172');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADAD468493');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADDFEBBAB6');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADA424F8BE');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD16880AAF');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADC4A1BF66');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD77A659E4');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD8A2047FA');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD5D91DD3E');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD9D7EE821');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADF4E3FD67');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADC6B2988F');
        $this->addSql('DROP TABLE accessoire');
        $this->addSql('DROP TABLE accessoire_aspirateur');
        $this->addSql('DROP TABLE adhesif');
        $this->addSql('DROP TABLE colle_joint');
        $this->addSql('DROP TABLE durcisseur');
        $this->addSql('DROP TABLE masquage');
        $this->addSql('DROP TABLE materiel');
        $this->addSql('DROP TABLE nettoyage');
        $this->addSql('DROP TABLE type_diluant');
        $this->addSql('DROP TABLE type_mastic');
        $this->addSql('DROP TABLE type_materiel');
        $this->addSql('DROP TABLE type_ponceuse');
        $this->addSql('DROP TABLE type_raccord');
        $this->addSql('DROP TABLE vitrage');
        $this->addSql('DROP INDEX IDX_D34A04ADA424F8BE ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADC4A1BF66 ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADDFEBBAB6 ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD461CFF51 ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADC6B2988F ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADF4E3FD67 ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD5D91DD3E ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD16880AAF ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADAD468493 ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD9879D172 ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD9D7EE821 ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD8A2047FA ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD77A659E4 ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADD23B67ED ON product');
        $this->addSql('ALTER TABLE product DROP masquage_id, DROP nettoyage_id, DROP durcisseur_id, DROP accessoire_aspirateur_id, DROP vitrage_id, DROP type_raccord_id, DROP type_materiel_id, DROP materiel_id, DROP colle_joint_id, DROP adhesif_id, DROP type_ponceuse_id, DROP type_mastic_id, DROP type_diluant_id, DROP accessoire_id');
    }
}
