<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241203140830 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649C7D54999 FOREIGN KEY (magasin_plafond_id) REFERENCES magasin_plafond (id)');
        $this->addSql('CREATE INDEX IDX_8D93D649C7D54999 ON user (magasin_plafond_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649C7D54999');
        $this->addSql('DROP INDEX IDX_8D93D649C7D54999 ON user');
    }
}
