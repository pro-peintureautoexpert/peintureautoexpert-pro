<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241109093942 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADFB24F947');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD91D428E2');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADFDBD8345');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD74DCE87A');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADBC72EFC1');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD477C69');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADEEC9591B');
        $this->addSql('DROP INDEX IDX_D34A04ADFDBD8345 ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD91D428E2 ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD74DCE87A ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADBC72EFC1 ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD477C69 ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADEEC9591B ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADFB24F947 ON product');
        $this->addSql('ALTER TABLE product DROP composants_appret_id, DROP diametre_abrasif_id, DROP composants_vernis_id, DROP finition_vernis_id, DROP type_vernis_id, DROP ral_acrylique_id, DROP orbital_ou_aleatoire_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product ADD composants_appret_id INT DEFAULT NULL, ADD diametre_abrasif_id INT DEFAULT NULL, ADD composants_vernis_id INT DEFAULT NULL, ADD finition_vernis_id INT DEFAULT NULL, ADD type_vernis_id INT DEFAULT NULL, ADD ral_acrylique_id INT DEFAULT NULL, ADD orbital_ou_aleatoire_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADFB24F947 FOREIGN KEY (composants_appret_id) REFERENCES composants_appret (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD91D428E2 FOREIGN KEY (diametre_abrasif_id) REFERENCES diametre_abrasif (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADFDBD8345 FOREIGN KEY (finition_vernis_id) REFERENCES finition_vernis (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD74DCE87A FOREIGN KEY (type_vernis_id) REFERENCES type_vernis (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADBC72EFC1 FOREIGN KEY (composants_vernis_id) REFERENCES composants_vernis (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD477C69 FOREIGN KEY (orbital_ou_aleatoire_id) REFERENCES orbital_ou_aleatoire (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADEEC9591B FOREIGN KEY (ral_acrylique_id) REFERENCES ral_acrylique (id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADFDBD8345 ON product (finition_vernis_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD91D428E2 ON product (diametre_abrasif_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD74DCE87A ON product (type_vernis_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADBC72EFC1 ON product (composants_vernis_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD477C69 ON product (orbital_ou_aleatoire_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADEEC9591B ON product (ral_acrylique_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADFB24F947 ON product (composants_appret_id)');
    }
}
