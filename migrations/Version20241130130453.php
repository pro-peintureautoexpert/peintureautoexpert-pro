<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241130130453 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE accessoire_polisseuse (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE barre_debosslage (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE chaine_raccord (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE debosseleur_marteau (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE equerre_tirage_griffe (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE masse_magnetique_chario (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product ADD accessoire_polisseuse_id INT DEFAULT NULL, ADD debosseleur_marteau_id INT DEFAULT NULL, ADD equerre_tirage_griffe_id INT DEFAULT NULL, ADD chaine_raccord_id INT DEFAULT NULL, ADD barre_debosslage_id INT DEFAULT NULL, ADD masse_magnetique_chario_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD2B675B0D FOREIGN KEY (accessoire_polisseuse_id) REFERENCES accessoire_polisseuse (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADE9D27AF0 FOREIGN KEY (debosseleur_marteau_id) REFERENCES debosseleur_marteau (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD81E6FEA0 FOREIGN KEY (equerre_tirage_griffe_id) REFERENCES equerre_tirage_griffe (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADDF44A6BD FOREIGN KEY (chaine_raccord_id) REFERENCES chaine_raccord (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADA98B9F49 FOREIGN KEY (barre_debosslage_id) REFERENCES barre_debosslage (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD8A537620 FOREIGN KEY (masse_magnetique_chario_id) REFERENCES masse_magnetique_chario (id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD2B675B0D ON product (accessoire_polisseuse_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADE9D27AF0 ON product (debosseleur_marteau_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD81E6FEA0 ON product (equerre_tirage_griffe_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADDF44A6BD ON product (chaine_raccord_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADA98B9F49 ON product (barre_debosslage_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD8A537620 ON product (masse_magnetique_chario_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD2B675B0D');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADA98B9F49');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADDF44A6BD');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADE9D27AF0');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD81E6FEA0');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD8A537620');
        $this->addSql('DROP TABLE accessoire_polisseuse');
        $this->addSql('DROP TABLE barre_debosslage');
        $this->addSql('DROP TABLE chaine_raccord');
        $this->addSql('DROP TABLE debosseleur_marteau');
        $this->addSql('DROP TABLE equerre_tirage_griffe');
        $this->addSql('DROP TABLE masse_magnetique_chario');
        $this->addSql('DROP INDEX IDX_D34A04AD2B675B0D ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADE9D27AF0 ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD81E6FEA0 ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADDF44A6BD ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADA98B9F49 ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD8A537620 ON product');
        $this->addSql('ALTER TABLE product DROP accessoire_polisseuse_id, DROP debosseleur_marteau_id, DROP equerre_tirage_griffe_id, DROP chaine_raccord_id, DROP barre_debosslage_id, DROP masse_magnetique_chario_id');
    }
}
