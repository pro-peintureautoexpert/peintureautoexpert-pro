<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241113085308 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE acier_alu (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE aspirateur (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE capacite_aspirateur (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE con_antigravillon (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE con_bidon_plastique (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE con_degraissant (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE con_diluant (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE con_durcisseur (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE con_gobelet (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE con_polish (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE con_savon (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE diametre_agrafes (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE diametre_zikzak (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dimansion_filtre_cabine (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dimension_chariot_kraft (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dimension_kraft (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dimension_micro_bruch (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dimension_papier_cale (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dimension_tireau (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE effet (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE filtre_cone (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pinceau_dimension (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ponceuse (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE regle_dosage (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sol_plafond (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE taille_fine_tape (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_pistole (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product ADD con_antigravillon_id INT DEFAULT NULL, ADD con_degraissant_id INT DEFAULT NULL, ADD capacite_aspirateur_id INT DEFAULT NULL, ADD con_polish_id INT DEFAULT NULL, ADD con_savon_id INT DEFAULT NULL, ADD con_diluant_id INT DEFAULT NULL, ADD con_gobelet_id INT DEFAULT NULL, ADD con_bidon_plastique_id INT DEFAULT NULL, ADD con_durcisseur_id INT DEFAULT NULL, ADD dimansion_filtre_cabine_id INT DEFAULT NULL, ADD diametre_zikzak_id INT DEFAULT NULL, ADD diametre_agrafes_id INT DEFAULT NULL, ADD dimension_chariot_kraft_id INT DEFAULT NULL, ADD dimension_papier_cale_id INT DEFAULT NULL, ADD filtre_cone_id INT DEFAULT NULL, ADD dimension_micro_bruch_id INT DEFAULT NULL, ADD pinceau_dimension_id INT DEFAULT NULL, ADD regle_dosage_id INT DEFAULT NULL, ADD dimension_kraft_id INT DEFAULT NULL, ADD dimension_tireau_id INT DEFAULT NULL, ADD taille_fine_tape_id INT DEFAULT NULL, ADD effet_id INT DEFAULT NULL, ADD aspirateur_id INT DEFAULT NULL, ADD ponceuse_id INT DEFAULT NULL, ADD type_pistole_id INT DEFAULT NULL, ADD sol_plafond_id INT DEFAULT NULL, ADD acier_alu_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD75DE074F FOREIGN KEY (con_antigravillon_id) REFERENCES con_antigravillon (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADC987DFA3 FOREIGN KEY (con_degraissant_id) REFERENCES con_degraissant (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADF0DFAD88 FOREIGN KEY (capacite_aspirateur_id) REFERENCES capacite_aspirateur (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADEF533380 FOREIGN KEY (con_polish_id) REFERENCES con_polish (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADE0EE37D FOREIGN KEY (con_savon_id) REFERENCES con_savon (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADAB8EAD1D FOREIGN KEY (con_diluant_id) REFERENCES con_diluant (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD8AB9C3A7 FOREIGN KEY (con_gobelet_id) REFERENCES con_gobelet (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD6EAC9667 FOREIGN KEY (con_bidon_plastique_id) REFERENCES con_bidon_plastique (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD5076C943 FOREIGN KEY (con_durcisseur_id) REFERENCES con_durcisseur (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADBF77AEA3 FOREIGN KEY (dimansion_filtre_cabine_id) REFERENCES dimansion_filtre_cabine (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD3BB8D10D FOREIGN KEY (diametre_zikzak_id) REFERENCES diametre_zikzak (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD2A6FB429 FOREIGN KEY (diametre_agrafes_id) REFERENCES diametre_agrafes (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD4993058F FOREIGN KEY (dimension_chariot_kraft_id) REFERENCES dimension_chariot_kraft (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADB7A84020 FOREIGN KEY (dimension_papier_cale_id) REFERENCES dimension_papier_cale (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADD72924FA FOREIGN KEY (filtre_cone_id) REFERENCES filtre_cone (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD99328893 FOREIGN KEY (dimension_micro_bruch_id) REFERENCES dimension_micro_bruch (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADF0B0E33 FOREIGN KEY (pinceau_dimension_id) REFERENCES pinceau_dimension (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADEEB55716 FOREIGN KEY (regle_dosage_id) REFERENCES regle_dosage (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADD2027B8A FOREIGN KEY (dimension_kraft_id) REFERENCES dimension_kraft (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADE5AFC66E FOREIGN KEY (dimension_tireau_id) REFERENCES dimension_tireau (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADE0C7DA9D FOREIGN KEY (taille_fine_tape_id) REFERENCES taille_fine_tape (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADE76D37FA FOREIGN KEY (effet_id) REFERENCES effet (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADB71B5139 FOREIGN KEY (aspirateur_id) REFERENCES aspirateur (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADD6673FB0 FOREIGN KEY (ponceuse_id) REFERENCES ponceuse (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADF26052AF FOREIGN KEY (type_pistole_id) REFERENCES type_pistole (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADE2F5CE5E FOREIGN KEY (sol_plafond_id) REFERENCES sol_plafond (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD5B71F8EC FOREIGN KEY (acier_alu_id) REFERENCES acier_alu (id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD75DE074F ON product (con_antigravillon_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADC987DFA3 ON product (con_degraissant_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADF0DFAD88 ON product (capacite_aspirateur_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADEF533380 ON product (con_polish_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADE0EE37D ON product (con_savon_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADAB8EAD1D ON product (con_diluant_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD8AB9C3A7 ON product (con_gobelet_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD6EAC9667 ON product (con_bidon_plastique_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD5076C943 ON product (con_durcisseur_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADBF77AEA3 ON product (dimansion_filtre_cabine_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD3BB8D10D ON product (diametre_zikzak_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD2A6FB429 ON product (diametre_agrafes_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD4993058F ON product (dimension_chariot_kraft_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADB7A84020 ON product (dimension_papier_cale_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADD72924FA ON product (filtre_cone_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD99328893 ON product (dimension_micro_bruch_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADF0B0E33 ON product (pinceau_dimension_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADEEB55716 ON product (regle_dosage_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADD2027B8A ON product (dimension_kraft_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADE5AFC66E ON product (dimension_tireau_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADE0C7DA9D ON product (taille_fine_tape_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADE76D37FA ON product (effet_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADB71B5139 ON product (aspirateur_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADD6673FB0 ON product (ponceuse_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADF26052AF ON product (type_pistole_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADE2F5CE5E ON product (sol_plafond_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD5B71F8EC ON product (acier_alu_id)');
        $this->addSql('ALTER TABLE product_parent CHANGE slug slug VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD5B71F8EC');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADB71B5139');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADF0DFAD88');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD75DE074F');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD6EAC9667');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADC987DFA3');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADAB8EAD1D');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD5076C943');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD8AB9C3A7');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADEF533380');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADE0EE37D');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD2A6FB429');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD3BB8D10D');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADBF77AEA3');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD4993058F');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADD2027B8A');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD99328893');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADB7A84020');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADE5AFC66E');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADE76D37FA');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADD72924FA');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADF0B0E33');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADD6673FB0');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADEEB55716');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADE2F5CE5E');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADE0C7DA9D');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADF26052AF');
        $this->addSql('DROP TABLE acier_alu');
        $this->addSql('DROP TABLE aspirateur');
        $this->addSql('DROP TABLE capacite_aspirateur');
        $this->addSql('DROP TABLE con_antigravillon');
        $this->addSql('DROP TABLE con_bidon_plastique');
        $this->addSql('DROP TABLE con_degraissant');
        $this->addSql('DROP TABLE con_diluant');
        $this->addSql('DROP TABLE con_durcisseur');
        $this->addSql('DROP TABLE con_gobelet');
        $this->addSql('DROP TABLE con_polish');
        $this->addSql('DROP TABLE con_savon');
        $this->addSql('DROP TABLE diametre_agrafes');
        $this->addSql('DROP TABLE diametre_zikzak');
        $this->addSql('DROP TABLE dimansion_filtre_cabine');
        $this->addSql('DROP TABLE dimension_chariot_kraft');
        $this->addSql('DROP TABLE dimension_kraft');
        $this->addSql('DROP TABLE dimension_micro_bruch');
        $this->addSql('DROP TABLE dimension_papier_cale');
        $this->addSql('DROP TABLE dimension_tireau');
        $this->addSql('DROP TABLE effet');
        $this->addSql('DROP TABLE filtre_cone');
        $this->addSql('DROP TABLE pinceau_dimension');
        $this->addSql('DROP TABLE ponceuse');
        $this->addSql('DROP TABLE regle_dosage');
        $this->addSql('DROP TABLE sol_plafond');
        $this->addSql('DROP TABLE taille_fine_tape');
        $this->addSql('DROP TABLE type_pistole');
        $this->addSql('DROP INDEX IDX_D34A04AD75DE074F ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADC987DFA3 ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADF0DFAD88 ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADEF533380 ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADE0EE37D ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADAB8EAD1D ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD8AB9C3A7 ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD6EAC9667 ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD5076C943 ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADBF77AEA3 ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD3BB8D10D ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD2A6FB429 ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD4993058F ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADB7A84020 ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADD72924FA ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD99328893 ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADF0B0E33 ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADEEB55716 ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADD2027B8A ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADE5AFC66E ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADE0C7DA9D ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADE76D37FA ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADB71B5139 ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADD6673FB0 ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADF26052AF ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADE2F5CE5E ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD5B71F8EC ON product');
        $this->addSql('ALTER TABLE product DROP con_antigravillon_id, DROP con_degraissant_id, DROP capacite_aspirateur_id, DROP con_polish_id, DROP con_savon_id, DROP con_diluant_id, DROP con_gobelet_id, DROP con_bidon_plastique_id, DROP con_durcisseur_id, DROP dimansion_filtre_cabine_id, DROP diametre_zikzak_id, DROP diametre_agrafes_id, DROP dimension_chariot_kraft_id, DROP dimension_papier_cale_id, DROP filtre_cone_id, DROP dimension_micro_bruch_id, DROP pinceau_dimension_id, DROP regle_dosage_id, DROP dimension_kraft_id, DROP dimension_tireau_id, DROP taille_fine_tape_id, DROP effet_id, DROP aspirateur_id, DROP ponceuse_id, DROP type_pistole_id, DROP sol_plafond_id, DROP acier_alu_id');
        $this->addSql('ALTER TABLE product_parent CHANGE slug slug VARCHAR(255) DEFAULT NULL');
    }
}
