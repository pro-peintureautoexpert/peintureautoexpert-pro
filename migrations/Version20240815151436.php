<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240815151436 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE composants_appret (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE composants_vernis (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE con_vernis (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE diametre_abrasif (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE finition_vernis (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product ADD composants_appret_id INT DEFAULT NULL, ADD diametre_abrasif_id INT DEFAULT NULL, ADD composants_vernis_id INT DEFAULT NULL, ADD con_vernis_id INT DEFAULT NULL, ADD finition_vernis_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADFB24F947 FOREIGN KEY (composants_appret_id) REFERENCES composants_appret (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD91D428E2 FOREIGN KEY (diametre_abrasif_id) REFERENCES diametre_abrasif (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADBC72EFC1 FOREIGN KEY (composants_vernis_id) REFERENCES composants_vernis (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD8A25DE54 FOREIGN KEY (con_vernis_id) REFERENCES con_vernis (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADFDBD8345 FOREIGN KEY (finition_vernis_id) REFERENCES finition_vernis (id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADFB24F947 ON product (composants_appret_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD91D428E2 ON product (diametre_abrasif_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADBC72EFC1 ON product (composants_vernis_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD8A25DE54 ON product (con_vernis_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADFDBD8345 ON product (finition_vernis_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADFB24F947');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADBC72EFC1');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD8A25DE54');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD91D428E2');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADFDBD8345');
        $this->addSql('DROP TABLE composants_appret');
        $this->addSql('DROP TABLE composants_vernis');
        $this->addSql('DROP TABLE con_vernis');
        $this->addSql('DROP TABLE diametre_abrasif');
        $this->addSql('DROP TABLE finition_vernis');
        $this->addSql('DROP INDEX IDX_D34A04ADFB24F947 ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD91D428E2 ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADBC72EFC1 ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD8A25DE54 ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADFDBD8345 ON product');
        $this->addSql('ALTER TABLE product DROP composants_appret_id, DROP diametre_abrasif_id, DROP composants_vernis_id, DROP con_vernis_id, DROP finition_vernis_id');
    }
}
