<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240815091211 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE couleur_appret (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product ADD couleur_appret_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADEDE96FB5 FOREIGN KEY (couleur_appret_id) REFERENCES couleur_appret (id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADEDE96FB5 ON product (couleur_appret_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADEDE96FB5');
        $this->addSql('DROP TABLE couleur_appret');
        $this->addSql('DROP INDEX IDX_D34A04ADEDE96FB5 ON product');
        $this->addSql('ALTER TABLE product DROP couleur_appret_id');
    }
}
