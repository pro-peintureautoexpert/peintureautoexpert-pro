<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240802143757 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE category_filters (category_id INT NOT NULL, filters_id INT NOT NULL, INDEX IDX_BFAF27F312469DE2 (category_id), INDEX IDX_BFAF27F36B715464 (filters_id), PRIMARY KEY(category_id, filters_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE category_filters ADD CONSTRAINT FK_BFAF27F312469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category_filters ADD CONSTRAINT FK_BFAF27F36B715464 FOREIGN KEY (filters_id) REFERENCES filters (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE category_filters DROP FOREIGN KEY FK_BFAF27F312469DE2');
        $this->addSql('ALTER TABLE category_filters DROP FOREIGN KEY FK_BFAF27F36B715464');
        $this->addSql('DROP TABLE category_filters');
    }
}
