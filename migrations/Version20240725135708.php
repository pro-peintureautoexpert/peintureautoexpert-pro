<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240725135708 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE temporary_order DROP FOREIGN KEY FK_1DC619B644F5D008');
        $this->addSql('DROP INDEX IDX_1DC619B644F5D008 ON temporary_order');
        $this->addSql('ALTER TABLE temporary_order DROP brand_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE temporary_order ADD brand_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE temporary_order ADD CONSTRAINT FK_1DC619B644F5D008 FOREIGN KEY (brand_id) REFERENCES brand (id)');
        $this->addSql('CREATE INDEX IDX_1DC619B644F5D008 ON temporary_order (brand_id)');
    }
}
