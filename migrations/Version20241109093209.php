<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241109093209 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADEDE96FB5');
        $this->addSql('DROP INDEX IDX_D34A04ADEDE96FB5 ON product');
        $this->addSql('ALTER TABLE product DROP couleur_appret_id, CHANGE weight weight DOUBLE PRECISION NOT NULL, CHANGE price_palette price_palette DOUBLE PRECISION NOT NULL, CHANGE is_homepage is_homepage TINYINT(1) NOT NULL, CHANGE in_home in_home TINYINT(1) NOT NULL, CHANGE in_paint in_paint TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product ADD couleur_appret_id INT DEFAULT NULL, CHANGE weight weight DOUBLE PRECISION DEFAULT NULL, CHANGE price_palette price_palette DOUBLE PRECISION DEFAULT NULL, CHANGE is_homepage is_homepage TINYINT(1) DEFAULT NULL, CHANGE in_home in_home TINYINT(1) DEFAULT NULL, CHANGE in_paint in_paint TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADEDE96FB5 FOREIGN KEY (couleur_appret_id) REFERENCES couleur_appret (id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADEDE96FB5 ON product (couleur_appret_id)');
    }
}
