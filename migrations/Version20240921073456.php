<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240921073456 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE couleur_all (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dimension_double_face (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE filter_dimension_cale (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE malette (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orbital_ou_aleatoire (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ral_acrylique (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rapidite (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_vernis (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product ADD rapidite_id INT DEFAULT NULL, ADD malette_id INT DEFAULT NULL, ADD couleur_all_id INT DEFAULT NULL, ADD type_vernis_id INT DEFAULT NULL, ADD ral_acrylique_id INT DEFAULT NULL, ADD filter_dimension_cale_id INT DEFAULT NULL, ADD dimension_double_face_id INT DEFAULT NULL, ADD orbital_ou_aleatoire_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD54326AD FOREIGN KEY (rapidite_id) REFERENCES rapidite (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADDB58B62C FOREIGN KEY (malette_id) REFERENCES malette (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD2F2B7A72 FOREIGN KEY (couleur_all_id) REFERENCES couleur_all (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD74DCE87A FOREIGN KEY (type_vernis_id) REFERENCES type_vernis (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADEEC9591B FOREIGN KEY (ral_acrylique_id) REFERENCES ral_acrylique (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADF6F7B257 FOREIGN KEY (filter_dimension_cale_id) REFERENCES filter_dimension_cale (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD6D0EB3B7 FOREIGN KEY (dimension_double_face_id) REFERENCES dimension_double_face (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD477C69 FOREIGN KEY (orbital_ou_aleatoire_id) REFERENCES orbital_ou_aleatoire (id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD54326AD ON product (rapidite_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADDB58B62C ON product (malette_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD2F2B7A72 ON product (couleur_all_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD74DCE87A ON product (type_vernis_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADEEC9591B ON product (ral_acrylique_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADF6F7B257 ON product (filter_dimension_cale_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD6D0EB3B7 ON product (dimension_double_face_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD477C69 ON product (orbital_ou_aleatoire_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD2F2B7A72');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD6D0EB3B7');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADF6F7B257');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADDB58B62C');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD477C69');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADEEC9591B');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD54326AD');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD74DCE87A');
        $this->addSql('DROP TABLE couleur_all');
        $this->addSql('DROP TABLE dimension_double_face');
        $this->addSql('DROP TABLE filter_dimension_cale');
        $this->addSql('DROP TABLE malette');
        $this->addSql('DROP TABLE orbital_ou_aleatoire');
        $this->addSql('DROP TABLE ral_acrylique');
        $this->addSql('DROP TABLE rapidite');
        $this->addSql('DROP TABLE type_vernis');
        $this->addSql('DROP INDEX IDX_D34A04AD54326AD ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADDB58B62C ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD2F2B7A72 ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD74DCE87A ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADEEC9591B ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADF6F7B257 ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD6D0EB3B7 ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD477C69 ON product');
        $this->addSql('ALTER TABLE product DROP rapidite_id, DROP malette_id, DROP couleur_all_id, DROP type_vernis_id, DROP ral_acrylique_id, DROP filter_dimension_cale_id, DROP dimension_double_face_id, DROP orbital_ou_aleatoire_id');
    }
}
