<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240716124306 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE technique_product_image (technique_product_id INT NOT NULL, image_id INT NOT NULL, INDEX IDX_1A36163843C9B91A (technique_product_id), INDEX IDX_1A3616383DA5256D (image_id), PRIMARY KEY(technique_product_id, image_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE technique_product_image ADD CONSTRAINT FK_1A36163843C9B91A FOREIGN KEY (technique_product_id) REFERENCES technique_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE technique_product_image ADD CONSTRAINT FK_1A3616383DA5256D FOREIGN KEY (image_id) REFERENCES image (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE technique_product_image DROP FOREIGN KEY FK_1A36163843C9B91A');
        $this->addSql('ALTER TABLE technique_product_image DROP FOREIGN KEY FK_1A3616383DA5256D');
        $this->addSql('DROP TABLE technique_product_image');
    }
}
