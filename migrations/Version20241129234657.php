<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241129234657 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE tags_colissimo (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, retour VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `order` ADD tags_colissimo_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F5299398C4097FEF FOREIGN KEY (tags_colissimo_id) REFERENCES tags_colissimo (id)');
        $this->addSql('CREATE INDEX IDX_F5299398C4097FEF ON `order` (tags_colissimo_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_F5299398C4097FEF');
        $this->addSql('DROP TABLE tags_colissimo');
        $this->addSql('DROP INDEX IDX_F5299398C4097FEF ON `order`');
        $this->addSql('ALTER TABLE `order` DROP tags_colissimo_id');
    }
}
