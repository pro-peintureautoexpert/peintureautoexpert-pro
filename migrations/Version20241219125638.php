<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241219125638 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD495A07A4');
        $this->addSql('DROP INDEX IDX_D34A04AD495A07A4 ON product');
        $this->addSql('ALTER TABLE product DROP qty_carton_id');
        $this->addSql('ALTER TABLE qty_carton ADD prod_id INT DEFAULT NULL, ADD price DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE qty_carton ADD CONSTRAINT FK_C15FB7491C83F75 FOREIGN KEY (prod_id) REFERENCES product (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C15FB7491C83F75 ON qty_carton (prod_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product ADD qty_carton_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD495A07A4 FOREIGN KEY (qty_carton_id) REFERENCES qty_carton (id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD495A07A4 ON product (qty_carton_id)');
        $this->addSql('ALTER TABLE qty_carton DROP FOREIGN KEY FK_C15FB7491C83F75');
        $this->addSql('DROP INDEX UNIQ_C15FB7491C83F75 ON qty_carton');
        $this->addSql('ALTER TABLE qty_carton DROP prod_id, DROP price');
    }
}
