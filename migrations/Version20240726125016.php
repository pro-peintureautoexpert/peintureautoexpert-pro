<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240726125016 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE temporary_order ADD user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE temporary_order ADD CONSTRAINT FK_1DC619B6A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_1DC619B6A76ED395 ON temporary_order (user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE temporary_order DROP FOREIGN KEY FK_1DC619B6A76ED395');
        $this->addSql('DROP INDEX IDX_1DC619B6A76ED395 ON temporary_order');
        $this->addSql('ALTER TABLE temporary_order DROP user_id');
    }
}
