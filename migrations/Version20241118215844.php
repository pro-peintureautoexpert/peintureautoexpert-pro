<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241118215844 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE dimension_all (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE matiere (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE taille (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product ADD taille_id INT DEFAULT NULL, ADD dimension_all_id INT DEFAULT NULL, ADD matiere_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADFF25611A FOREIGN KEY (taille_id) REFERENCES taille (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD72919D30 FOREIGN KEY (dimension_all_id) REFERENCES dimension_all (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADF46CD258 FOREIGN KEY (matiere_id) REFERENCES matiere (id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADFF25611A ON product (taille_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD72919D30 ON product (dimension_all_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADF46CD258 ON product (matiere_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD72919D30');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADF46CD258');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADFF25611A');
        $this->addSql('DROP TABLE dimension_all');
        $this->addSql('DROP TABLE matiere');
        $this->addSql('DROP TABLE taille');
        $this->addSql('DROP INDEX IDX_D34A04ADFF25611A ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD72919D30 ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADF46CD258 ON product');
        $this->addSql('ALTER TABLE product DROP taille_id, DROP dimension_all_id, DROP matiere_id');
    }
}
