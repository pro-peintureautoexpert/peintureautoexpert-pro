<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241029155006 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE product_parent (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, brand_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, about VARCHAR(255) DEFAULT NULL, INDEX IDX_5FF221EB12469DE2 (category_id), INDEX IDX_5FF221EB44F5D008 (brand_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_parent ADD CONSTRAINT FK_5FF221EB12469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE product_parent ADD CONSTRAINT FK_5FF221EB44F5D008 FOREIGN KEY (brand_id) REFERENCES brand (id)');
        $this->addSql('ALTER TABLE product ADD product_parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD1CDA7CFD FOREIGN KEY (product_parent_id) REFERENCES product_parent (id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD1CDA7CFD ON product (product_parent_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD1CDA7CFD');
        $this->addSql('ALTER TABLE product_parent DROP FOREIGN KEY FK_5FF221EB12469DE2');
        $this->addSql('ALTER TABLE product_parent DROP FOREIGN KEY FK_5FF221EB44F5D008');
        $this->addSql('DROP TABLE product_parent');
        $this->addSql('DROP INDEX IDX_D34A04AD1CDA7CFD ON product');
        $this->addSql('ALTER TABLE product DROP product_parent_id');
    }
}
